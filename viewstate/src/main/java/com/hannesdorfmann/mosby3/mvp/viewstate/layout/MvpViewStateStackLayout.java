/**
 * Copyright 2015 Hanes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.mvp.viewstate.layout;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.delegate.ComponentContainerMvpDelegate;
import com.hannesdorfmann.mosby3.mvp.delegate.ComponentContainerMvpViewStateDelegateImpl;
import com.hannesdorfmann.mosby3.mvp.delegate.ComponentContainerMvpViewStateDelegateCallback;
import com.hannesdorfmann.mosby3.mvp.layout.MvpStackLayout;
import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;
import ohos.agp.components.AttrSet;
import ohos.app.Context;

/**
 *
 * A {@link MvpStackLayout} with ViewState support.
 *
 * @author Hannes Dorfmann
 */
public abstract class MvpViewStateStackLayout<V extends MvpView, P extends MvpPresenter<V>, VS extends ViewState<V>>
    extends MvpStackLayout<V, P> implements ComponentContainerMvpViewStateDelegateCallback<V, P, VS> {

  private boolean restoringViewState = false;
  protected VS viewState;

  public MvpViewStateStackLayout(Context context) {
    super(context);
  }

  public MvpViewStateStackLayout(Context context, AttrSet attrs) {
    super(context, attrs);
  }

  public MvpViewStateStackLayout(Context context, AttrSet attrs, String styleName) {
    super(context, attrs, styleName);
  }

  @Override
  protected ComponentContainerMvpDelegate<V, P> getMvpDelegate() {
    if (mvpDelegate == null) {
      mvpDelegate = new ComponentContainerMvpViewStateDelegateImpl<>(this, this, true);
    }

    return mvpDelegate;
  }

  @Override public VS getViewState() {
    return viewState;
  }

  @Override public void setViewState(VS viewState) {
    this.viewState =  viewState;
  }

  @Override public void setRestoringViewState(boolean retstoringViewState) {
    this.restoringViewState = retstoringViewState;
  }

  @Override public boolean isRestoringViewState() {
    return restoringViewState;
  }

  @Override public void onViewStateInstanceRestored(boolean instanceStateRetained) {
    // can be overridden in subclass
  }

}
