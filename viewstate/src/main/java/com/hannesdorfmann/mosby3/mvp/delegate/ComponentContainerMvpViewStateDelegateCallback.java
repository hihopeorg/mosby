package com.hannesdorfmann.mosby3.mvp.delegate;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

/**
 * * An enhanced version of {@link MvpDelegateCallback} that adds {@link ViewState} support.
 * This interface must be implemented by all (subclasses of) ohos.agp.components.Component like FrameLayout
 * that want to support {@link ViewState} and mvp.
 * @author Hannes Dorfmann
 */
public interface ComponentContainerMvpViewStateDelegateCallback<V extends MvpView, P extends MvpPresenter<V>, VS extends ViewState<V>>
    extends MvpViewStateDelegateCallback<V, P, VS>, ComponentContainerDelegateCallback<V, P> {
}
