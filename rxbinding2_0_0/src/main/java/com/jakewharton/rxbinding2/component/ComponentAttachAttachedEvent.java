package com.jakewharton.rxbinding2.component;


import ohos.agp.components.Component;
import com.google.auto.value.AutoValue;

/**
 * A view attached event on a view.
 * <p>
 * <strong>Warning:</strong> Instances keep a strong reference to the view. Operators that
 * cache instances have the potential to leak the associated {@link Context}.
 */
@AutoValue
public abstract class ComponentAttachAttachedEvent extends ComponentAttachEvent {

  public static ComponentAttachAttachedEvent create(Component view) {
    return new AutoValue_ComponentAttachAttachedEvent(view);
  }

  ComponentAttachAttachedEvent() {
  }
}
