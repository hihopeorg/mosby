package com.jakewharton.rxbinding2.component;

import com.jakewharton.rxbinding2.internal.Notification;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.ohos.MainThreadDisposable;
import ohos.agp.components.Component;

import static com.jakewharton.rxbinding2.internal.Preconditions.checkMainThread;

final class ComponentAttachesObservable extends Observable<Object> {
  private final boolean callOnAttach;
  private final Component view;

  ComponentAttachesObservable(Component view, boolean callOnAttach) {
    this.view = view;
    this.callOnAttach = callOnAttach;
  }

  @Override protected void subscribeActual(final Observer<? super Object> observer) {
    if (!checkMainThread(observer)) {
      return;
    }
    Listener listener = new Listener(view, callOnAttach, observer);
    observer.onSubscribe(listener);
    view.setBindStateChangedListener(listener);
  }

  static final class Listener extends MainThreadDisposable implements Component.BindStateChangedListener/*OnAttachStateChangeListener*/ {
    private final Component view;
    private final boolean callOnAttach;
    private final Observer<? super Object> observer;

    Listener(Component view, boolean callOnAttach, Observer<? super Object> observer) {
      this.view = view;
      this.callOnAttach = callOnAttach;
      this.observer = observer;
    }


    @Override protected void onDispose() {
      view.removeBindStateChangedListener(this);
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
      if (callOnAttach && !isDisposed()) {
        observer.onNext(Notification.INSTANCE);
      }
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
      if (!callOnAttach && !isDisposed()) {
        observer.onNext(Notification.INSTANCE);
      }
    }
  }
}
