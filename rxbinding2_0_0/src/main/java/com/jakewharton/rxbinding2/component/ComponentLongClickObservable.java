package com.jakewharton.rxbinding2.component;

import com.jakewharton.rxbinding2.internal.Notification;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.ohos.MainThreadDisposable;
import ohos.agp.components.Component;

import java.util.concurrent.Callable;

import static com.jakewharton.rxbinding2.internal.Preconditions.checkMainThread;

final class ComponentLongClickObservable extends Observable<Object> {
  private final Component view;
  private final Callable<Boolean> handled;

  ComponentLongClickObservable(Component view, Callable<Boolean> handled) {
    this.view = view;
    this.handled = handled;
  }

  @Override protected void subscribeActual(Observer<? super Object> observer) {
    if (!checkMainThread(observer)) {
      return;
    }
    Listener listener = new Listener(view, handled, observer);
    observer.onSubscribe(listener);
    view.setLongClickedListener(listener);
  }

  static final class Listener extends MainThreadDisposable implements Component.LongClickedListener {
    private final Component view;
    private final Observer<? super Object> observer;
    private final Callable<Boolean> handled;

    Listener(Component view, Callable<Boolean> handled, Observer<? super Object> observer) {
      this.view = view;
      this.observer = observer;
      this.handled = handled;
    }

    @Override protected void onDispose() {
      view.setLongClickedListener(null);
    }

    @Override
    public void onLongClicked(Component component) {
      if (!isDisposed()) {
        try {
          if (handled.call()) {
            observer.onNext(Notification.INSTANCE);
          }
        } catch (Exception e) {
          observer.onError(e);
          dispose();
        }
      }
    }
  }
}
