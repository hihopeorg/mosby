package com.jakewharton.rxbinding2.component;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.ohos.MainThreadDisposable;
import ohos.agp.components.ComponentContainer;

import static com.jakewharton.rxbinding2.internal.Preconditions.checkMainThread;

//TODO 未匹配
final class ComponentContainerHierarchyChangeEventObservable
    /*extends Observable<ComponentContainerHierarchyChangeEvent>*/ {
//  private final ComponentContainer viewGroup;
//
//  ComponentContainerHierarchyChangeEventObservable(ComponentContainer viewGroup) {
//    this.viewGroup = viewGroup;
//  }
//
//  @Override
//  protected void subscribeActual(Observer<? super ComponentContainerHierarchyChangeEvent> observer) {
//    if (!checkMainThread(observer)) {
//      return;
//    }
//    Listener listener = new Listener(viewGroup, observer);
//    observer.onSubscribe(listener);
//    viewGroup.setOnHierarchyChangeListener(listener);
//  }
//
//  static final class Listener extends MainThreadDisposable implements OnHierarchyChangeListener {
//    private final ComponentContainer viewGroup;
//    private final Observer<? super ComponentContainerHierarchyChangeEvent> observer;
//
//    Listener(ComponentContainer viewGroup, Observer<? super ComponentContainerHierarchyChangeEvent> observer) {
//      this.viewGroup = viewGroup;
//      this.observer = observer;
//    }
//
//    @Override public void onChildViewAdded(View parent, View child) {
//      if (!isDisposed()) {
//        observer.onNext(ViewGroupHierarchyChildViewAddEvent.create(viewGroup, child));
//      }
//    }
//
//    @Override public void onChildViewRemoved(View parent, View child) {
//      if (!isDisposed()) {
//        observer.onNext(ViewGroupHierarchyChildViewRemoveEvent.create(viewGroup, child));
//      }
//    }
//
//    @Override protected void onDispose() {
//      viewGroup.setOnHierarchyChangeListener(null);
//    }
//  }
}
