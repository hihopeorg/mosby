package com.jakewharton.rxbinding2.component;

import com.jakewharton.rxbinding2.internal.Notification;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.ohos.MainThreadDisposable;
import ohos.agp.components.Component;

import static com.jakewharton.rxbinding2.internal.Preconditions.checkMainThread;

final class ComponentClickObservable extends Observable<Object> {
  private final Component view;

  ComponentClickObservable(Component view) {
    this.view = view;
  }

  @Override protected void subscribeActual(Observer<? super Object> observer) {
    if (!checkMainThread(observer)) {
      return;
    }
    Listener listener = new Listener(view, observer);
    observer.onSubscribe(listener);
    view.setClickedListener(listener);
  }

  static final class Listener extends MainThreadDisposable implements Component.ClickedListener {
    private final Component view;
    private final Observer<? super Object> observer;

    Listener(Component view, Observer<? super Object> observer) {
      this.view = view;
      this.observer = observer;
    }

    @Override public void onClick(Component v) {
      if (!isDisposed()) {
        observer.onNext(Notification.INSTANCE);
      }
    }

    @Override protected void onDispose() {
      view.setClickedListener(null);
    }
  }
}
