package com.jakewharton.rxbinding2.component;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.ohos.MainThreadDisposable;
import io.reactivex.functions.Predicate;
import ohos.agp.components.Component;
import ohos.agp.components.DragEvent;
import ohos.agp.components.DragInfo;

import static com.jakewharton.rxbinding2.internal.Preconditions.checkMainThread;

final class ComponentDragObservable extends Observable<DragInfo> {
  private final Component view;
  private final Predicate<? super DragInfo> handled;

  ComponentDragObservable(Component view, Predicate<? super DragInfo> handled) {
    this.view = view;
    this.handled = handled;
  }

  @Override protected void subscribeActual(Observer<? super DragInfo> observer) {
    if (!checkMainThread(observer)) {
      return;
    }
    Listener listener = new Listener(view, handled, observer);
    observer.onSubscribe(listener);
    view.setDraggedListener(DragEvent.DRAG_BEGIN, listener);
  }

  static final class Listener extends MainThreadDisposable implements Component.DraggedListener {
    private final Component view;
    private final Predicate<? super DragInfo> handled;
    private final Observer<? super DragInfo> observer;

    Listener(Component view, Predicate<? super DragInfo> handled,
        Observer<? super DragInfo> observer) {
      this.view = view;
      this.handled = handled;
      this.observer = observer;
    }

    public boolean onDrag(Component v, DragInfo event) {
      if (!isDisposed()) {
        try {
          if (handled.test(event)) {
            observer.onNext(event);
            return true;
          }
        } catch (Exception e) {
          observer.onError(e);
          dispose();
        }
      }
      return false;
    }

    @Override protected void onDispose() {
      view.setDraggedListener(DragEvent.DRAG_FINISH, null);
    }

    @Override
    public void onDragDown(Component component, DragInfo dragInfo) {
      onDrag(component, dragInfo);

    }

    @Override
    public void onDragStart(Component component, DragInfo dragInfo) {
      onDrag(component, dragInfo);
    }

    @Override
    public void onDragUpdate(Component component, DragInfo dragInfo) {
      onDrag(component, dragInfo);
    }

    @Override
    public void onDragEnd(Component component, DragInfo dragInfo) {
      onDrag(component, dragInfo);
    }

    @Override
    public void onDragCancel(Component component, DragInfo dragInfo) {
      onDrag(component, dragInfo);
    }
  }
}
