package com.jakewharton.rxbinding2.component;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.ohos.MainThreadDisposable;
import io.reactivex.functions.Predicate;
import ohos.agp.components.Component;
import ohos.multimodalinput.event.KeyEvent;

import static com.jakewharton.rxbinding2.internal.Preconditions.checkMainThread;

final class ComponentKeyObservable extends Observable<KeyEvent> {
  private final Component view;
  private final Predicate<? super KeyEvent> handled;

  ComponentKeyObservable(Component view, Predicate<? super KeyEvent> handled) {
    this.view = view;
    this.handled = handled;
  }

  @Override protected void subscribeActual(Observer<? super KeyEvent> observer) {
    if (!checkMainThread(observer)) {
      return;
    }
    Listener listener = new Listener(view, handled, observer);
    observer.onSubscribe(listener);
    view.setKeyEventListener(listener);
  }

  static final class Listener extends MainThreadDisposable implements Component.KeyEventListener {
    private final Component view;
    private final Predicate<? super KeyEvent> handled;
    private final Observer<? super KeyEvent> observer;

    Listener(Component view, Predicate<? super KeyEvent> handled,
        Observer<? super KeyEvent> observer) {
      this.view = view;
      this.handled = handled;
      this.observer = observer;
    }

    @Override protected void onDispose() {
      view.setKeyEventListener(null);
    }

    @Override
    public boolean onKeyEvent(Component component, KeyEvent keyEvent) {
      if (!isDisposed()) {
        try {
          if (handled.test(keyEvent)) {
            observer.onNext(keyEvent);
            return true;
          }
        } catch (Exception e) {
          observer.onError(e);
          dispose();
        }
      }
      return false;
    }
  }
}
