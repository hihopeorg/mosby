package com.jakewharton.rxbinding2.component;

import com.jakewharton.rxbinding2.internal.Notification;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.ohos.MainThreadDisposable;
import ohos.agp.components.Component;

import java.util.concurrent.Callable;

import static com.jakewharton.rxbinding2.internal.Preconditions.checkMainThread;

//TODO 不匹配
final class ComponentTreeObserverPreDrawObservable /*extends Observable<Object>*/ {
//  private final Component view;
//  private final Callable<Boolean> proceedDrawingPass;
//
//  ComponentTreeObserverPreDrawObservable(Component view, Callable<Boolean> proceedDrawingPass) {
//    this.view = view;
//    this.proceedDrawingPass = proceedDrawingPass;
//  }
//
//  @Override protected void subscribeActual(Observer<? super Object> observer) {
//    if (!checkMainThread(observer)) {
//      return;
//    }
//    Listener listener = new Listener(view, proceedDrawingPass, observer);
//    observer.onSubscribe(listener);
//    view.getComponentTreeObserver()
//        .addOnPreDrawListener(listener);
//  }
//
//  static final class Listener extends MainThreadDisposable implements OnPreDrawListener {
//    private final Component view;
//    private final Callable<Boolean> proceedDrawingPass;
//    private final Observer<? super Object> observer;
//
//    Listener(Component view, Callable<Boolean> proceedDrawingPass, Observer<? super Object> observer) {
//      this.view = view;
//      this.proceedDrawingPass = proceedDrawingPass;
//      this.observer = observer;
//    }
//
//    @Override public boolean onPreDraw() {
//      if (!isDisposed()) {
//        observer.onNext(Notification.INSTANCE);
//        try {
//          return proceedDrawingPass.call();
//        } catch (Exception e) {
//          observer.onError(e);
//          dispose();
//        }
//      }
//      return true;
//    }
//
//    @Override protected void onDispose() {
//      view.getViewTreeObserver().removeOnPreDrawListener(this);
//    }
//  }
}
