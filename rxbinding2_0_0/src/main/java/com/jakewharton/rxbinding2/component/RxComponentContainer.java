package com.jakewharton.rxbinding2.component;

import io.reactivex.Observable;
import ohos.agp.components.ComponentContainer;

import static com.jakewharton.rxbinding2.internal.Preconditions.checkNotNull;

/**
 * Static factory methods for creating {@linkplain Observable observables} for {@link ComponentContainer}.
 */
public final class RxComponentContainer {


  private RxComponentContainer() {
    throw new AssertionError("No instances.");
  }
}
