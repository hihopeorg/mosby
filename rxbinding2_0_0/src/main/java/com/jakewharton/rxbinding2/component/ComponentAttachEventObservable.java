package com.jakewharton.rxbinding2.component;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.ohos.MainThreadDisposable;
import ohos.agp.components.Component;

import static com.jakewharton.rxbinding2.internal.Preconditions.checkMainThread;

final class ComponentAttachEventObservable extends Observable<ComponentAttachEvent> {
  private final Component view;

  ComponentAttachEventObservable(Component view) {
    this.view = view;
  }

  @Override protected void subscribeActual(Observer<? super ComponentAttachEvent> observer) {
    if (!checkMainThread(observer)) {
      return;
    }
    Listener listener = new Listener(view, observer);
    observer.onSubscribe(listener);

    view.setBindStateChangedListener(listener);
//    view.addOnAttachStateChangeListener(listener);
  }

  static final class Listener extends MainThreadDisposable implements  Component.BindStateChangedListener/*implements OnAttachStateChangeListener*/ {
    private final Component view;
    private final Observer<? super ComponentAttachEvent> observer;

    Listener(Component view, Observer<? super ComponentAttachEvent> observer) {
      this.view = view;
      this.observer = observer;
    }

    @Override protected void onDispose() {
      view.setBindStateChangedListener(this);
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
      if (!isDisposed()) {
        observer.onNext(ComponentAttachAttachedEvent.create(view));
      }
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
      if (!isDisposed()) {
        observer.onNext(ComponentAttachDetachedEvent.create(view));
      }
    }
  }
}
