package com.jakewharton.rxbinding2.component;


import com.google.auto.value.AutoValue;
import ohos.agp.components.Component;

/**
 * A view attached event on a view.
 * <p>
 * <strong>Warning:</strong> Instances keep a strong reference to the view. Operators that
 * cache instances have the potential to leak the associated {@link Context}.
 */
@AutoValue
public abstract class ComponentAttachDetachedEvent extends ComponentAttachEvent {

  public static ComponentAttachDetachedEvent create(Component view) {
    return new AutoValue_ComponentAttachDetachedEvent(view);
  }

  ComponentAttachDetachedEvent() {
  }
}
