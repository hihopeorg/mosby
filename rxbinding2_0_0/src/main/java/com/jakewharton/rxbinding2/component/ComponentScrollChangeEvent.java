package com.jakewharton.rxbinding2.component;

import com.google.auto.value.AutoValue;
import ohos.agp.components.Component;

/**
 * A scroll-change event on a view.
 * <p>
 * <strong>Warning:</strong> Instances keep a strong reference to the view. Operators that cache
 * instances have the potential to leak the associated {@link ohos.agp.components.Component}.
 */
@AutoValue
public abstract class ComponentScrollChangeEvent {
  public static ComponentScrollChangeEvent create(Component view, int scrollX, int scrollY,
                                                               int oldScrollX, int oldScrollY) {
//    return new AutoValue_ComponentScrollChangeEvent(view, scrollX, scrollY, oldScrollX, oldScrollY);
    //TODO
    return null;
  }

  ComponentScrollChangeEvent() {
  }

  /** The view from which this event occurred. */
  public abstract Component view();
  public abstract int scrollX();
  public abstract int scrollY();
  public abstract int oldScrollX();
  public abstract int oldScrollY();
}
