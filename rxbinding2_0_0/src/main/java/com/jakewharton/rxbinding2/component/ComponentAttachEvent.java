package com.jakewharton.rxbinding2.component;

import ohos.agp.components.Component;

/**
 * A view attach event on a view.
 * <p>
 * <strong>Warning:</strong> Instances keep a strong reference to the view. Operators that
 * cache instances have the potential to leak the associated {@link Context}.
 */
public abstract class ComponentAttachEvent {
  ComponentAttachEvent() {
  }

  /** The view from which this event occurred. */
  public abstract Component view();
}
