package com.jakewharton.rxbinding2.component;


import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

public abstract class ComponentContainerHierarchyChangeEvent {
  ComponentContainerHierarchyChangeEvent() {
  }

  /** The view from which this event occurred. */
  public abstract ComponentContainer view();

  /** The child from which this event occurred. */
  public abstract Component child();
}
