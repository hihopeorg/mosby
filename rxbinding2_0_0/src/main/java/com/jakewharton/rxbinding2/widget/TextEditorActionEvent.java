package com.jakewharton.rxbinding2.widget;


import com.google.auto.value.AutoValue;
import ohos.agp.components.Text;

@AutoValue
public abstract class TextEditorActionEvent {
  public static TextEditorActionEvent create(Text view, int actionId
                                                 /*KeyEvent keyEvent*/) {
    return new AutoValue_TextEditorActionEvent(view, actionId/*, keyEvent*/);
  }

  TextEditorActionEvent() {
  }

  /** The view from which this event occurred. */
  public abstract Text view();
  public abstract int actionId();
//  public abstract KeyEvent keyEvent();
}
