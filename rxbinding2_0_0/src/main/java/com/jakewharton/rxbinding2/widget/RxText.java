package com.jakewharton.rxbinding2.widget;

import com.jakewharton.rxbinding2.InitialValueObservable;
import com.jakewharton.rxbinding2.internal.Functions;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import ohos.agp.components.Text;

import static com.jakewharton.rxbinding2.internal.Preconditions.checkNotNull;

/**
 * Static factory methods for creating {@linkplain Observable observables} and {@linkplain Consumer
 * actions} for {@link Text}.
 */
public final class RxText {
//  /**
//   * Create an observable of editor actions on {@code view}.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   * <p>
//   * <em>Warning:</em> The created observable uses {@link Text#setEditorActionListener} to
//   * observe actions. Only one observable can be used for a view at a time.
//   */
//  public static Observable<Integer> editorActions(Text view) {
//    checkNotNull(view, "view == null");
//    return editorActions(view, Functions.PREDICATE_ALWAYS_TRUE);
//  }
//
//  /**
//   * Create an observable of editor actions on {@code view}.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   * <p>
//   * <em>Warning:</em> The created observable uses {@link Text.EditorActionListener} to
//   * observe actions. Only one observable can be used for a view at a time.
//   *
//   * @param handled Predicate invoked each occurrence to determine the return value of the
//   * underlying {@link Text.EditorActionListener}.
//   */
//  public static Observable<Integer> editorActions(Text view,
//                                                  Predicate<? super Integer> handled) {
//    checkNotNull(view, "view == null");
//    checkNotNull(handled, "handled == null");
//    return new TextEditorActionObservable(view, handled);
//  }
//
//  /**
//   * Create an observable of editor action events on {@code view}.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   * <p>
//   * <em>Warning:</em> The created observable uses {@link Text.EditorActionListener} to
//   * observe actions. Only one observable can be used for a view at a time.
//   */
//  public static Observable<TextEditorActionEvent> editorActionEvents(Text view) {
//    checkNotNull(view, "view == null");
//    return editorActionEvents(view, Functions.PREDICATE_ALWAYS_TRUE);
//  }
//
//  /**
//   * Create an observable of editor action events on {@code view}.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   * <p>
//   * <em>Warning:</em> The created observable uses {@link TextView.OnEditorActionListener} to
//   * observe actions. Only one observable can be used for a view at a time.
//   *
//   * @param handled Predicate invoked each occurrence to determine the return value of the
//   * underlying {@link TextView.OnEditorActionListener}.
//   */
//  public static Observable<TextEditorActionEvent> editorActionEvents(Text view,
//      Predicate<? super TextEditorActionEvent> handled) {
//    checkNotNull(view, "view == null");
//    checkNotNull(handled, "handled == null");
//    return new TextEditorActionEventObservable(view, handled);
//  }
//
//  /**
//   * Create an observable of character sequences for text changes on {@code view}.
//   * <p>
//   * <em>Warning:</em> Values emitted by this observable are <b>mutable</b> and owned by the host
//   * {@code TextView} and thus are <b>not safe</b> to cache or delay reading (such as by observing
//   * on a different thread). If you want to cache or delay reading the items emitted then you must
//   * map values through a function which calls {@link String#valueOf} or
//   * {@link CharSequence#toString() .toString()} to create a copy.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   * <p>
//   * <em>Note:</em> A value will be emitted immediately on subscribe.
//   */
//  public static InitialValueObservable<CharSequence> textChanges(Text view) {
//    checkNotNull(view, "view == null");
//    return new TextViewTextObservable(view);
//  }
//
//  /**
//   * Create an observable of text change events for {@code view}.
//   * <p>
//   * <em>Warning:</em> Values emitted by this observable contain a <b>mutable</b>
//   * {@link CharSequence} owned by the host {@code TextView} and thus are <b>not safe</b> to cache
//   * or delay reading (such as by observing on a different thread). If you want to cache or delay
//   * reading the items emitted then you must map values through a function which calls
//   * {@link String#valueOf} or {@link CharSequence#toString() .toString()} to create a copy.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   * <p>
//   * <em>Note:</em> A value will be emitted immediately on subscribe.
//   */
//  public static InitialValueObservable<TextViewTextChangeEvent> textChangeEvents(
//      Text view) {
//    checkNotNull(view, "view == null");
//    return new TextViewTextChangeEventObservable(view);
//  }
//
//  /**
//   * Create an observable of before text change events for {@code view}.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   * <p>
//   * <em>Note:</em> A value will be emitted immediately on subscribe.
//   */
//  public static InitialValueObservable<TextViewBeforeTextChangeEvent> beforeTextChangeEvents(
//      Text view) {
//    checkNotNull(view, "view == null");
//    return new TextViewBeforeTextChangeEventObservable(view);
//  }
//
//  /**
//   * Create an observable of after text change events for {@code view}.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   * <p>
//   * <em>Note:</em> A value will be emitted immediately on subscribe using
//   * {@link TextView#getEditableText()}.
//   */
//  public static InitialValueObservable<TextViewAfterTextChangeEvent> afterTextChangeEvents(
//      Text view) {
//    checkNotNull(view, "view == null");
//    return new TextViewAfterTextChangeEventObservable(view);
//  }
//
//  /**
//   * An action which sets the text property of {@code view} with character sequences.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   */
//  public static Consumer<? super CharSequence> text(final Text view) {
//    checkNotNull(view, "view == null");
//    return new Consumer<CharSequence>() {
//      @Override public void accept(CharSequence text) {
//        view.setText(text);
//      }
//    };
//  }
//
//  /**
//   * An action which sets the text property of {@code view} string resource IDs.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   */
//  public static Consumer<? super Integer> textRes(final Text view) {
//    checkNotNull(view, "view == null");
//    return new Consumer<Integer>() {
//      @Override public void accept(Integer textRes) {
//        view.setText(textRes);
//      }
//    };
//  }
//
//  /**
//   * An action which sets the error property of {@code view} with character sequences.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   */
//  public static Consumer<? super CharSequence> error(final Text view) {
//    checkNotNull(view, "view == null");
//    return new Consumer<CharSequence>() {
//      @Override public void accept(CharSequence text) {
//        view.setError(text);
//      }
//    };
//  }
//
//  /**
//   * An action which sets the error property of {@code view} string resource IDs.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   */
//  public static Consumer<? super Integer> errorRes(final Text view) {
//    checkNotNull(view, "view == null");
//    return new Consumer<Integer>() {
//      @Override public void accept(Integer textRes) {
//        view.setError(view.getContext().getResources().getText(textRes));
//      }
//    };
//  }
//
//  /**
//   * An action which sets the hint property of {@code view} with character sequences.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   */
//  public static Consumer<? super CharSequence> hint(final Text view) {
//    checkNotNull(view, "view == null");
//    return new Consumer<CharSequence>() {
//      @Override public void accept(CharSequence hint) {
//        view.setHint(hint);
//      }
//    };
//  }
//
//  /**
//   * An action which sets the hint property of {@code view} string resource IDs.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   */
//  public static Consumer<? super Integer> hintRes(final Text view) {
//    checkNotNull(view, "view == null");
//    return new Consumer<Integer>() {
//      @Override public void accept(Integer hintRes) {
//        view.setHint(hintRes);
//      }
//    };
//  }
//
//  /**
//   * An action which sets the color property of {@code view} with color integer.
//   * <p>
//   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
//   * to free this reference.
//   */
//  public static Consumer<? super Integer> color(final Text view) {
//    checkNotNull(view, "view == null");
//    return new Consumer<Integer>() {
//      @Override
//      public void accept(Integer color) throws Exception {
//        view.setTextColor(color);
//      }
//    };
//  }

  private RxText() {
    throw new AssertionError("No instances.");
  }
}
