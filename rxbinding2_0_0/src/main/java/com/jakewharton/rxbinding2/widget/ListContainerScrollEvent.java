package com.jakewharton.rxbinding2.widget;

import com.google.auto.value.AutoValue;
import ohos.agp.components.ListContainer;

@AutoValue
public abstract class ListContainerScrollEvent {
  public static ListContainerScrollEvent create(ListContainer listView, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
    return new AutoValue_ListContainerScrollEvent(listView, scrollX, scrollY,
            oldScrollX, oldScrollY);
  }

  ListContainerScrollEvent() {
  }

  /** The view from which this event occurred. */
  public abstract ListContainer view();
  public abstract int scrollState();
  public abstract int firstVisibleItem();
  public abstract int visibleItemCount();
  public abstract int totalItemCount();
}
