package com.jakewharton.rxbinding2.widget;

import com.jakewharton.rxbinding2.InitialValueObservable;
import io.reactivex.Observer;
import io.reactivex.ohos.MainThreadDisposable;
import ohos.agp.components.SearchBar;

import static com.jakewharton.rxbinding2.internal.Preconditions.checkMainThread;

final class SearchBarQueryTextChangeEventsObservable
    extends InitialValueObservable<SearchBarQueryTextEvent> {
  private final SearchBar view;

  SearchBarQueryTextChangeEventsObservable(SearchBar view) {
    this.view = view;
  }

  @Override protected void subscribeListener(Observer<? super SearchBarQueryTextEvent> observer) {
    if (!checkMainThread(observer)) {
      return;
    }
    Listener listener = new Listener(view, observer);
    view.setQueryListener(listener);
    observer.onSubscribe(listener);
  }

  @Override protected SearchBarQueryTextEvent getInitialValue() {
    return SearchBarQueryTextEvent.create(view, view.getQuery(), false);
  }

  static final class Listener extends MainThreadDisposable implements SearchBar.QueryListener {
    private final SearchBar view;
    private final Observer<? super SearchBarQueryTextEvent> observer;

    Listener(SearchBar view, Observer<? super SearchBarQueryTextEvent> observer) {
      this.view = view;
      this.observer = observer;
    }

    @Override protected void onDispose() {
      view.setQueryListener(null);
    }

    @Override
    public boolean onQuerySubmit(String query) {
      if (!isDisposed()) {
        observer.onNext(SearchBarQueryTextEvent.create(view, query, true));
        return true;
      }
      return false;
    }

    @Override
    public boolean onQueryChanged(String s) {
      if (!isDisposed()) {
        observer.onNext(SearchBarQueryTextEvent.create(view, s, false));
        return true;
      }
      return false;
    }
  }
}
