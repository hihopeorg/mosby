package com.jakewharton.rxbinding2.widget;

import io.reactivex.Observable;
import ohos.agp.components.ListContainer;

import static com.jakewharton.rxbinding2.internal.Preconditions.checkNotNull;

public class RxListContainer {

    public static Observable<ListContainerScrollEvent> scrollEvents(ListContainer mListContainer) {
        checkNotNull(mListContainer, "absListView == null");
        return new ListContainerScrollEventObservable(mListContainer);
    }

    private RxListContainer() {
        throw new AssertionError("No instances.");
    }
}


