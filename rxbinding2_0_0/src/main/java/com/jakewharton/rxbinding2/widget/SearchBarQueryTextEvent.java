package com.jakewharton.rxbinding2.widget;


import com.google.auto.value.AutoValue;
import ohos.agp.components.SearchBar;

@AutoValue
public abstract class SearchBarQueryTextEvent {
  public static SearchBarQueryTextEvent create(SearchBar view,
                                               CharSequence queryText, boolean submitted) {

    return new AutoValue_SearchBarQueryTextEvent(view, queryText, submitted);
  }

  SearchBarQueryTextEvent() {
  }

  /** The view from which this event occurred. */
  public abstract SearchBar view();
  public abstract CharSequence queryText();
  public abstract boolean isSubmitted();
}
