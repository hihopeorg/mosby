package com.jakewharton.rxbinding2.widget;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.ohos.MainThreadDisposable;
import io.reactivex.functions.Predicate;
import ohos.agp.components.Text;

import static com.jakewharton.rxbinding2.internal.Preconditions.checkMainThread;

final class TextEditorActionEventObservable extends Observable<TextEditorActionEvent> {
  private final Text view;
  private final Predicate<? super TextEditorActionEvent> handled;

  TextEditorActionEventObservable(Text view,
                                  Predicate<? super TextEditorActionEvent> handled) {
    this.view = view;
    this.handled = handled;
  }

  @Override
  protected void subscribeActual(Observer<? super TextEditorActionEvent> observer) {
    if (!checkMainThread(observer)) {
      return;
    }
    Listener listener = new Listener(view, observer, handled);
    observer.onSubscribe(listener);
    Text text;
    view.setEditorActionListener(listener);
  }

  static final class Listener extends MainThreadDisposable implements Text.EditorActionListener {
    private final Text view;
    private final Observer<? super TextEditorActionEvent> observer;
    private final Predicate<? super TextEditorActionEvent> handled;

    Listener(Text view, Observer<? super TextEditorActionEvent> observer,
        Predicate<? super TextEditorActionEvent> handled) {
      this.view = view;
      this.observer = observer;
      this.handled = handled;
    }


    @Override
    protected void onDispose() {
      view.setEditorActionListener(null);
    }

    @Override
    public boolean onTextEditorAction(int actionId) {
      TextEditorActionEvent event = TextEditorActionEvent.create(view, actionId);
      try {
        if (!isDisposed() && handled.test(event)) {
          observer.onNext(event);
          return true;
        }
      } catch (Exception e) {
        observer.onError(e);
        dispose();
      }
      return false;
    }
  }
}
