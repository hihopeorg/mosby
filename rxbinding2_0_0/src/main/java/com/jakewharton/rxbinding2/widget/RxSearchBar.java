package com.jakewharton.rxbinding2.widget;

import com.jakewharton.rxbinding2.InitialValueObservable;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import ohos.agp.components.SearchBar;

import static com.jakewharton.rxbinding2.internal.Preconditions.checkNotNull;

/**
 * Static factory methods for creating {@linkplain Observable observables} and {@linkplain Consumer
 * consumers} for {@link SearchBar}.
 */
public final class RxSearchBar {
  /**
   * Create an observable of {@linkplain SearchBarQueryTextEvent query text events} on {@code
   * view}.
   * <p>
   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
   * to free this reference.
   * <p>
   * <em>Note:</em> A value will be emitted immediately on subscribe.
   */
  public static InitialValueObservable<SearchBarQueryTextEvent> queryTextChangeEvents(
      SearchBar view) {
    checkNotNull(view, "view == null");
    return new SearchBarQueryTextChangeEventsObservable(view);
  }

  /**
   * Create an observable of character sequences for query text changes on {@code view}.
   * <p>
   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
   * to free this reference.
   * <p>
   * <em>Note:</em> A value will be emitted immediately on subscribe.
   */
  public static InitialValueObservable<CharSequence> queryTextChanges(SearchBar view) {
    checkNotNull(view, "view == null");
    return new SearchBarQueryTextChangesObservable(view);
  }

  /**
   * An action which sets the query property of {@code view} with character sequences.
   * <p>
   * <em>Warning:</em> The created observable keeps a strong reference to {@code view}. Unsubscribe
   * to free this reference.
   *
   * @param submit weather to submit query right after updating query text
   */
  public static Consumer<? super CharSequence> query(final SearchBar view,
      final boolean submit) {
    checkNotNull(view, "view == null");
    return new Consumer<CharSequence>() {
      @Override public void accept(CharSequence text) {
        view.setQuery(text==null?null:String.valueOf(text), submit);
      }
    };
  }

  private RxSearchBar() {
    throw new AssertionError("No instances.");
  }
}
