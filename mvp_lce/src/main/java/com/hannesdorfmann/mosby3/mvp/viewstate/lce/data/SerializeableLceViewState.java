/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.mvp.viewstate.lce.data;

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;
import com.hannesdorfmann.mosby3.mvp.viewstate.RestorableViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.AbsSequenceableLceViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

import java.io.Serializable;

/**
 * A {@link LceViewState} and{@link RestorableViewState} that uses a Serializeable as content data
 * <p>
 * Can be used for Activites and Fragments.
 * </p>
 *
 * @param <D> the data / model type
 * @param <V> the type of the view
 */
public class SerializeableLceViewState<D extends Serializable, V extends MvpLceView<D>>
    extends AbsSequenceableLceViewState<D, V> {

    public SerializeableLceViewState(Parcel parcel) {
        unmarshalling(parcel);
    }

    public static final Sequenceable.Producer PRODUCER = new Producer() {
        @Override
        public Object createFromParcel(Parcel parcel) {
            return new SerializeableLceViewState(parcel);
        }
    };

    @Override
    public boolean marshalling(Parcel out) {
        out.writeSerializable(loadedData);
        return false;
    }

    @Override
    public boolean unmarshalling(Parcel in) {
        this.loadedData = (D) in.readSerializable(loadedData.getClass());
        return false;
    }


    /**
     * The class loader used for deserializing the list of parcelable items
     */
    protected ClassLoader getClassLoader() {
        return getClass().getClassLoader();
    }

}
