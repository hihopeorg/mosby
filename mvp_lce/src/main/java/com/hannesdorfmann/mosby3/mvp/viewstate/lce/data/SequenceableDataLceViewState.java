/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.mvp.viewstate.lce.data;

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;
import com.hannesdorfmann.mosby3.mvp.viewstate.RestorableViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.AbsSequenceableLceViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

/**
 * A {@link LceViewState} and {@link RestorableViewState} that uses Parcelable as content data.
 * Internally it uses {@link Parcel#writeParcelable(Parcelable, int)} and {@link
 * Parcel#readParcelable(ClassLoader)} for serialisation. It uses the default class loader. You can
 * override {@link #getClassLoader()} for provide your own ClassLoader.
 *
 * <p>
 * Can be used for Activites and Fragments.
 * </p>
 *
 * @param <D> the data / model type
 * @param <V> the type of the view
 */
public class SequenceableDataLceViewState<D extends Sequenceable, V extends MvpLceView<D>>
    extends AbsSequenceableLceViewState<D, V> {

  private static final String BUNDLE_PARCELABLE_WORKAROUND =
          "com.hannesdorfmann.mosby.mvp.viewstate.lce.ParcelableLceViewState.workaround";

  /**
   * Get the ClassLoader that is used for deserialization
   */
  protected ClassLoader getClassLoader() {
    return getClass().getClassLoader();
  }

  public SequenceableDataLceViewState(Parcel parcel) {
    unmarshalling(parcel);
  }

  public static final Sequenceable.Producer PRODUCER = new Producer() {
    @Override
    public Object createFromParcel(Parcel parcel) {
      return new SequenceableDataLceViewState(parcel);
    }
  };

  @Override
  public boolean marshalling(Parcel out) {
    out.writeSequenceable(loadedData);
    return false;
  }

  @Override
  public boolean unmarshalling(Parcel in) {
    return in.readSequenceable(loadedData);
  }
}
