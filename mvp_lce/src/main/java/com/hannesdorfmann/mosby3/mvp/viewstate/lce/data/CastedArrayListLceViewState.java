/*
 * Copyright 2017 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.mvp.viewstate.lce.data;

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;
import com.hannesdorfmann.mosby3.mvp.viewstate.RestorableViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.AbsSequenceableLceViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

import java.util.List;

/**
 * A {@link LceViewState} and {@link RestorableViewState} that uses ArrayList containing
 * Parcelables as content data. This class takes a <code>List<? extends Parcelable></code> but
 * assumes that it is an instance of ArrayList. It uses {@link Parcel#writeList(List)}
 * and {@link Parcel#readArrayList(ClassLoader)} for serialisation
 * <p>
 * Can be used for Activites and Fragments.
 * </p>
 *
 * @param <D> the type of the data / model that is put in an ArrayList
 * @param <V> the type of the view
 * @deprecated User {@link SequenceableListLceViewState}
 */
@Deprecated
public class CastedArrayListLceViewState<D extends List<? extends Sequenceable>, V extends MvpLceView<D>>
    extends AbsSequenceableLceViewState<D, V> {

  /**
   * The class loader used for deserializing the list of parcelable items
   */
  protected ClassLoader getClassLoader() {
    return getClass().getClassLoader();
  }

  public CastedArrayListLceViewState() {
  }

  public CastedArrayListLceViewState(Parcel parcel) {
    unmarshalling(parcel);
  }

  public static final Sequenceable.Producer PRODUCER = new Producer() {
    @Override
    public Object createFromParcel(Parcel parcel) {
      return new CastedArrayListLceViewState(parcel);
    }
  };

  @Override
  public boolean marshalling(Parcel out) {
    out.writeList(loadedData);
    return false;
  }

  @Override
  public boolean unmarshalling(Parcel in) {

    this.loadedData = (D) in.readList();
    return false;
  }
}
