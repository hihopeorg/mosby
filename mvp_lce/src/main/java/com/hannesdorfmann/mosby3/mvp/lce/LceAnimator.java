/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.mvp.lce;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.global.resource.ResourceManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import com.hannesdorfmann.mosby3.mvp.animator.AlphaAnimator;
import com.hannesdorfmann.mosby3.mvp.widget.CirleProgressImg;

/**
 * Little helper class for animating content, error and loading view
 *
 * @author Hannes Dorfmann
 */
public class LceAnimator {

    private static final String TAG = LceAnimator.class.getSimpleName();
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0, TAG);

    private LceAnimator() {
    }

    /**
     * Show the loading view. No animations, because sometimes loading things is pretty fast (i.e.
     * retrieve data from memory cache).
     */
    public static void showLoading(CirleProgressImg loadingView, Component contentView,
                                   Component errorView) {
        contentView.setVisibility(Component.HIDE);
        errorView.setVisibility(Component.HIDE);
        loadingView.setVisibility(Component.VISIBLE);
    }

    /**
     * Shows the error view instead of the loading view
     */
    public static void showErrorView(final CirleProgressImg loadingView, final Component contentView,
                                     final Component errorView) {
        contentView.setVisibility(Component.HIDE);
        int durations = 300;

        loadingView.setVisibility(Component.VISIBLE);

        // Not visible yet, so animate the view in
        AnimatorGroup set = new AnimatorGroup();
        AnimatorProperty in = errorView.createAnimatorProperty();
        AnimatorProperty loadingOut = loadingView.createAnimatorProperty();
        in.alpha(1f);
        in.setDuration(durations);
        in.setDelay(durations);
        loadingOut.alpha(0f);
        loadingOut.setDuration(200);
        set.runSerially(in, loadingOut);
        set.setDelay(durations);
        set.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onStop(Animator animator) {
                errorView.setVisibility(Component.VISIBLE);
                loadingView.setVisibility(Component.HIDE);
            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        set.start();

    }

    /**
     * Display the content instead of the loadingView
     */
    public static void showContent(final CirleProgressImg loadingView, final Component contentView,
                                   final Component errorView) {
        if (contentView.getVisibility() == Component.VISIBLE) {
            // No Changing needed, because contentView is already visible
            errorView.setVisibility(Component.HIDE);
            loadingView.setVisibility(Component.HIDE);
        } else {

            errorView.setVisibility(Component.HIDE);
            loadingView.setVisibility(Component.VISIBLE);

            float translateInPixels = AttrHelper.vp2px(40, loadingView.getContext());

            long durations = 200;
            AnimatorProperty contentFadeIn = new AlphaAnimator(0f, 1f);
            contentFadeIn.setDuration(durations);
            contentFadeIn.setTarget(contentView);

            AnimatorProperty loadingFadeOut = new AlphaAnimator(0f, 1f);
            loadingFadeOut.setDuration(durations);
            loadingFadeOut.setTarget(loadingView);

            AnimatorProperty contentTranslateIn = contentView.createAnimatorProperty();
            contentTranslateIn.moveFromY(translateInPixels);
            contentTranslateIn.moveToY(0);
            contentTranslateIn.setDuration(durations);

//            contentView.setTranslationY(0);

            AnimatorGroup set2 = new AnimatorGroup();
            set2.runParallel(contentFadeIn, contentTranslateIn);
            set2.setDuration(durations);
            set2.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {
                    contentView.setTranslationY(0);
                    contentView.setVisibility(Component.VISIBLE);
                }

                @Override
                public void onStop(Animator animator) {
                    loadingView.setVisibility(Component.HIDE);
                    loadingView.setAlpha(1f);
                    contentView.setTranslationY(0);
                    errorView.setVisibility(Component.HIDE);
                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {

                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });


            AnimatorGroup set1 = new AnimatorGroup();
            set1.runParallel(loadingFadeOut);
            set1.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {
                    contentView.setVisibility(Component.HIDE);
                }

                @Override
                public void onStop(Animator animator) {
                    loadingView.setAlpha(1f);
                    loadingView.setVisibility(Component.HIDE);
                    contentView.setVisibility(Component.VISIBLE);
                    set2.start();
                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });
            set1.start();

        }
    }
}
