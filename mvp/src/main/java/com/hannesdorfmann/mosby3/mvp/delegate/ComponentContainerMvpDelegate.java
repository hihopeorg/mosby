/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.mvp.delegate;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.Component;
import ohos.utils.Sequenceable;

/**
 * The mvp delegate used for everything that derives from {@link ohos.agp.components.Component} like {@link ohos.agp.components.StackLayout}
 * etc.
 *
 * <p>
 * The following methods must be called from the corresponding View lifecycle method:
 * <ul>
 * <li>{@link #onAttachedToWindow()}</li>
 * <li>{@link #onDetachedFromWindow()}</li>
 * </ul>
 * </p>
 *
 * @author Hannes Dorfmann
 */
public interface ComponentContainerMvpDelegate<V extends MvpView, P extends MvpPresenter<V>> {

  /**
   * Must be called from {@link Component#onAttachedToWindow()}
   */
  void onAttachedToWindow(/*AbilitySlice mAbilitySlice*/);

  /**
   * Must be called from {@link Component#onDetachedFromWindow()}
   */
  void onDetachedFromWindow();


  void onRestoreInstanceState(Sequenceable state);
  /**
   * Save the instatnce state
   *
   * @return The state with all the saved data
   */
  Sequenceable onSaveInstanceState();
}
