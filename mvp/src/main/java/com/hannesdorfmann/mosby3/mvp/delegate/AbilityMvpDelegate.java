/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.mvp.delegate;

import ohos.aafwk.content.Intent;
import ohos.utils.PacMap;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * A delegate for Activities to attach them to Mosby mvp.
 *
 * <p>
 * The following methods must be invoked from the corresponding Activities lifecycle methods:
 * <ul>
 * <li>{@link #onStart(Intent)}</li>
 * <li>{@link #onPostStart(PacMap)}</li>
 * <li>{@link #onStop()} </li>
 * <li>{@link #onActive()} </li>
 * <li>{@link #onPostActive()} </li>
 * <li>{@link #onInactive()} </li>
 * <li>{@link #onForeground(Intent)} </li>
 * <li>{@link #onBackground()} </li>
 * <li>{@link #onSaveAbilityState(PacMap)} </li>
 * <li>{@link #onRestoreAbilityState(PacMap)} </li>
 * <li>{@link #onStoreDataWhenConfigChange()} </li>
 * <li></li>
 * </ul>
 * </p>
 *
 * @param <V> The type of {@link MvpView}
 * @param <P> The type of {@link MvpPresenter}
 * @author Hannes Dorfmann
 */
public interface AbilityMvpDelegate<V extends MvpView, P extends MvpPresenter<V>> {

    void onStart(Intent intent);

    void onPostStart(PacMap pacMap);

    void onStop();

    void onActive();

    void onPostActive();

    void onInactive();

    void onForeground(Intent intent);

    void onBackground();

    void onSaveAbilityState(PacMap outState);

    void onRestoreAbilityState(PacMap inState);

    Object onStoreDataWhenConfigChange();

}
