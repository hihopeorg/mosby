/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.mvp;

import com.hannesdorfmann.mosby3.mvp.delegate.*;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * A Fragment that uses a {@link MvpPresenter} to implement a Model-View-Presenter architecture.
 */
public abstract class MvpFraction<V extends MvpView, P extends MvpPresenter<V>> extends Fraction
    implements MvpDelegateCallback<V, P>, MvpView {

  protected FractionMvpDelegate<V, P> mvpDelegate;
  public Component mComponent;

  /**
   * The presenter for this view. Will be instantiated with {@link #createPresenter()}
   */
  protected P presenter;

  /**
   * Creates a new presenter instance, if needed. Will reuse the previous presenter instance if
   * {@link #setRetainInstance(boolean)} is set to true. This method will be called from
   * {@link #onViewCreated(View, Bundle)}
   */
  public abstract P createPresenter();
  protected abstract int getUIContent();
  protected abstract void initCompoment();

  /**
   * Gets the mvp delegate. This is internally used for creating presenter, attaching and
   * detaching view from presenter.
   *
   * <p>
   * <b>Please note that only one instance of mvp delegate should be used per fragment instance</b>.
   * </p>
   *
   * <p>
   * Only override this method if you really know what you are doing.
   * </p>
   *
   * @return {@link FractionMvpDelegateImpl}
   */
  protected FractionMvpDelegate<V, P> getMvpDelegate() {
    if (mvpDelegate == null) {
      mvpDelegate = new FractionMvpDelegateImpl<V, P>(this, this, true, true);
    }

    return mvpDelegate;
  }

  @Override public P getPresenter() {
    return presenter;
  }

  @Override public void setPresenter(P presenter) {
    this.presenter = presenter;
  }

  @Override public V getMvpView() {
    return (V) this;
  }

  @Override
  protected void onStart(Intent intent) {
    super.onStart(intent);
    getMvpDelegate().onStart(intent);
  }

  @Override
  protected void onStop() {
    super.onStop();
    getMvpDelegate().onStop();
  }

  @Override
  protected void onActive() {
    super.onActive();
    getMvpDelegate().onActive();
  }

  @Override
  protected void onInactive() {
    super.onInactive();
    getMvpDelegate().onInactive();
  }

  @Override
  protected void onForeground(Intent intent) {
    super.onForeground(intent);
    getMvpDelegate().onForeground(intent);
  }

  @Override
  protected void onBackground() {
    super.onBackground();
    getMvpDelegate().onBackground();
  }

  @Override
  public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
    getMvpDelegate().onComponentAttached(scatter, container, intent);

    mComponent = scatter.parse(getUIContent(), container, false);
    initCompoment();
    return mComponent;
  }

  @Override
  protected void onComponentDetach() {
    super.onComponentDetach();
    getMvpDelegate().onComponentDetach();
  }

}

