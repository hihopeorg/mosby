/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.mvp.layout;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.delegate.ComponentContainerDelegateCallback;
import com.hannesdorfmann.mosby3.mvp.delegate.ComponentContainerMvpDelegate;
import com.hannesdorfmann.mosby3.mvp.delegate.ComponentContainerMvpDelegateImpl;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.app.Context;
import ohos.utils.Sequenceable;

/**
 * A StackLayout that can be used as View with an presenter
 *
 */
public abstract class MvpStackLayout<V extends MvpView, P extends MvpPresenter<V>>
    extends StackLayout implements ComponentContainerDelegateCallback<V, P>, MvpView {

  protected Context context;
  protected P presenter;
  protected ComponentContainerMvpDelegate<V, P> mvpDelegate;
  private boolean retainInstance = false;
  protected AbilitySlice mAbilitySlice;
  protected Context mComponetContext;

  public MvpStackLayout(Context context) {
    super(context);
    this.context = context;
    initBindStateChangedListener();
  }

  public MvpStackLayout(Context context,  AttrSet attrSet) {
    super(context, attrSet);
    this.context = context;
    initBindStateChangedListener();
  }

  public MvpStackLayout(Context context, AttrSet attrSet, String styleName) {
    super(context, attrSet, styleName);
    this.context = context;
    initBindStateChangedListener();
  }

  @Override
  public void setmComponetContext(Context mComponetContext){
    this.mComponetContext = mComponetContext;
  }

  @Override
  public Context getmComponetContext() {
    return mComponetContext;
  }


  private void initBindStateChangedListener(){

    setBindStateChangedListener(new BindStateChangedListener() {
      @Override
      public void onComponentBoundToWindow(Component component) {
        getMvpDelegate().onAttachedToWindow();
        onAttachedToWindowDo(component);
      }

      @Override
      public void onComponentUnboundFromWindow(Component component) {
        getMvpDelegate().onDetachedFromWindow();
        onDetachedFromWindowDo(component);
      }
    });
  }

  protected abstract void onAttachedToWindowDo(Component component);
  protected abstract void onDetachedFromWindowDo(Component component);


  /**
   * Get the mvp delegate. This is internally used for creating presenter, attaching and detaching
   * view from presenter etc.
   *
   * <p><b>Please note that only one instance of mvp delegate should be used per ohos.agp.components.Co11111111111111lonStart22222mponent
   * instance</b>.
   * </p>
   *
   * <p>
   * Only override this method if you really know what you are doing.
   * </p>
   *
   * @return {@link ComponentContainerMvpDelegate}
   */
  protected ComponentContainerMvpDelegate<V, P> getMvpDelegate() {
    if (mvpDelegate == null) {
      mvpDelegate = new ComponentContainerMvpDelegateImpl<V, P>(this, this, true);
    }

    return mvpDelegate;
  }


  @Override
  public void superOnRestoreInstanceState(Sequenceable state) {
    getMvpDelegate().onRestoreInstanceState(state);
  }


  /**
   * Instantiate a presenter instance
   *
   * @return The {@link MvpPresenter} for this view
   */
  public abstract P createPresenter();

  @Override public P getPresenter() {
    return presenter;
  }

  @Override public void setPresenter(P presenter) {
    this.presenter = presenter;
  }

  @Override public V getMvpView() {
    return (V) this;
  }

}
