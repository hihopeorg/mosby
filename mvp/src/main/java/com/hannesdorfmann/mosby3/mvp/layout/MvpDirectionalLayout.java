/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.mvp.layout;


import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.delegate.ComponentContainerDelegateCallback;
import com.hannesdorfmann.mosby3.mvp.delegate.ComponentContainerMvpDelegate;
import com.hannesdorfmann.mosby3.mvp.delegate.ComponentContainerMvpDelegateImpl;
import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

/**
 * A LinearLayout that can be used as view with an presenter
 *
 */
public abstract class MvpDirectionalLayout<V extends MvpView, P extends MvpPresenter<V>>
    extends DirectionalLayout implements MvpView, ComponentContainerDelegateCallback<V, P> {

  protected P presenter;
  protected ComponentContainerMvpDelegate<V, P> mvpDelegate;
  private boolean retainInstance = false;

  public MvpDirectionalLayout(Context context) {
    super(context);
  }

  public MvpDirectionalLayout(Context context, AttrSet attrs) {
    super(context, attrs);
  }

  public MvpDirectionalLayout(Context context, AttrSet attrs, String styleName) {
    super(context, attrs, styleName);
  }


  /**
   * Get the mvp delegate. This is internally used for creating presenter, attaching and detaching
   * view from presenter etc.
   *
   * <p><b>Please note that only one instance of mvp delegate should be used per ohos.agp.components.Component
   * instance</b>.
   * </p>
   *
   * <p>
   * Only override this method if you really know what you are doing.
   * </p>
   *
   * @return {@link ComponentContainerMvpDelegate}
   */
  protected ComponentContainerMvpDelegate<V, P> getMvpDelegate() {
    if (mvpDelegate == null) {
      mvpDelegate = new ComponentContainerMvpDelegateImpl<>(this, this, true);
    }

    return mvpDelegate;
  }


  /**
   * Instantiate a presenter instance
   *
   * @return The {@link MvpPresenter} for this view
   */
  public abstract P createPresenter();

  @Override public P getPresenter() {
    return presenter;
  }

  @Override public void setPresenter(P presenter) {
    this.presenter = presenter;
  }

  @Override public V getMvpView() {
    return (V) this;
  }

}
