/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.swiperefresh.gesture;

import com.hannesdorfmann.swiperefresh.componet.ComponentConfiguration;
import ohos.agp.components.VelocityDetector;
import ohos.multimodalinput.event.TouchEvent;

/**
 * gesture recognizer
 *
 * @date 2020/12/27
 */
public class GestureDetector {

    public interface OnGestureListener {

        /**
         * Callback when scrolling occurs
         *
         * @param e1 Events when fingers are pressed
         * @param e2 Events when fingers move
         * @param distanceX Lateral movement distance
         * @param distanceY Longitudinal movement distance
         * @return
         */
        boolean onScroll(TouchEvent e1, TouchEvent e2, float distanceX, float distanceY);

    }

    private int mTouchSlopSquare;
    private final OnGestureListener mListener;

    private boolean mAlwaysInTapRegion;

    private TouchEvent mCurrentDownEvent;
    private TouchEvent mCurrentMotionEvent;
    private TouchEvent mPreviousUpEvent;

    private float mLastFocusX;
    private float mLastFocusY;
    private float mDownFocusX;
    private float mDownFocusY;

    private VelocityDetector mVelocityTracker;

    public GestureDetector(OnGestureListener listener) {
        mListener = listener;
        int touchSlop = ComponentConfiguration.getScaledTouchSlop();
        // When sliding, the finger movement should be greater than this distance before rolling
        mTouchSlopSquare = touchSlop * touchSlop;
    }

    public boolean onTouchEvent(TouchEvent ev) {
        final int action = ev.getAction();
        mCurrentMotionEvent = ev;

        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityDetector.obtainInstance();
        }
        mVelocityTracker.addEvent(ev);

        final boolean pointerUp =
                action == TouchEvent.OTHER_POINT_UP;
        final int skipIndex = pointerUp ? ev.getIndex() : -1;

        float sumX = 0, sumY = 0;
        final int count = ev.getPointerCount();
        // Add up the positions X and y of all the fingers that are still touching, and then average them to calculate the center focus
        for (int i = 0; i < count; i++) {
            if (skipIndex == i) {
                // Skip the lifting action of non main pointer
                continue;
            }
            sumX += ev.getPointerPosition(i).getX();
            sumY += ev.getPointerPosition(i).getY();
        }
        final int div = pointerUp ? count - 1 : count;
        // Find the average value and calculate the center focus
        final float focusX = sumX / div;
        final float focusY = sumY / div;

        boolean handled = false;

        switch (action) {
            case TouchEvent.OTHER_POINT_DOWN:
                mDownFocusX = mLastFocusX = focusX;
                mDownFocusY = mLastFocusY = focusY;
                break;

            case TouchEvent.OTHER_POINT_UP:
                mDownFocusX = mLastFocusX = focusX;
                mDownFocusY = mLastFocusY = focusY;
                break;

            case TouchEvent.PRIMARY_POINT_DOWN:
                mDownFocusX = mLastFocusX = focusX;
                mDownFocusY = mLastFocusY = focusY;
                mCurrentDownEvent = ev;
                mAlwaysInTapRegion = true;
                break;

            case TouchEvent.POINT_MOVE:
                final float scrollX = mLastFocusX - focusX;
                final float scrollY = mLastFocusY - focusY;
                if (mAlwaysInTapRegion) {
                    final int deltaX = (int) (focusX - mDownFocusX);
                    final int deltaY = (int) (focusY - mDownFocusY);
                    int distance = (deltaX * deltaX) + (deltaY * deltaY);
                    int slopSquare = mTouchSlopSquare;

                    if (distance > slopSquare) {
                        try {
                            handled = mListener.onScroll(mCurrentDownEvent, ev, scrollX, scrollY);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        mLastFocusX = focusX;
                        mLastFocusY = focusY;
                        mAlwaysInTapRegion = false;
                    }
                } else if ((Math.abs(scrollX) >= 1) || (Math.abs(scrollY) >= 1)) {
                    try {
                        handled = mListener.onScroll(mCurrentDownEvent, ev, scrollX, scrollY);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    mLastFocusX = focusX;
                    mLastFocusY = focusY;
                }
                break;

            case TouchEvent.PRIMARY_POINT_UP:
                break;

            case TouchEvent.CANCEL:
                break;
        }
        return handled;
    }
}
