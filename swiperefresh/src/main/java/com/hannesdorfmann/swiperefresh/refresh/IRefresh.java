/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.swiperefresh.refresh;

/**
 * Drop down refresh interface
 *
 *
 * @date 2020/12/27
 */
public interface IRefresh {

    /**
     * Do you want to disable scrolling when the pull-down refresh
     *
     * @param disableRefreshScroll
     */
    void setDisableRefreshScroll(boolean disableRefreshScroll);

    /**
     * Refresh complete
     */
    void refreshFinish();

    /**
     * Set drop down refresh monitor
     *
     * @param listener
     */
    void setRefreshListener(RefreshListener listener);

    /**
     *
     * @param headOverComponent
     */
    void setRefreshOverComponent(HeadOverComponent headOverComponent);

    interface RefreshListener {

        /**
         * Tells the caller that a refresh is in progress at this time
         */
        void onRefresh();

        /**
         * Allow refresh
         *
         * @return
         */
        boolean enableRefresh();
    }
}
