/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.swiperefresh.refresh;


import com.hannesdorfmann.swiperefresh.ResourceTable;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;

/**
 * Custom drop down refresh header component
 *
 * @date 2020/12/27
 */
public class TextOverComponent extends HeadOverComponent {

    private Image mImage;
    private Text mText;
    private AnimatorProperty mAnimator;
    TaskDispatcher dispatcher;
    private Context context;

    public TextOverComponent(Context context) {
        this(context, null);
        this.context = context;
    }

    public TextOverComponent(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
        this.context = context;
    }

    public TextOverComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        this.context = context;
    }

    @Override
    public void init() {
        LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_refresh_over_view, this, true);
        mImage = (Image) findComponentById(ResourceTable.Id_image);
        mText = (Text) findComponentById(ResourceTable.Id_text);
    }

    /**
     * roll
     *
     * @param scrollY The rolling distance of the vertical axis
     * @param pullRefreshHeight The minimum height at which a pull-down refresh is triggered
     */
    @Override
    public void onScroll(int scrollY, int pullRefreshHeight) {

    }

    /**
     * Display head assembly
     */
    @Override
    public void onVisible() {
        mText.setText("下拉刷新");
    }

    /**
     * Beyond the height of the head view, let go and start loading
     */
    @Override
    public void onOver() {
        mText.setText("松开刷新");
    }

    /**
     * Refreshing
     */
    @Override
    public void onRefresh() {
        mText.setText("正在刷新...");
        mAnimator = mImage.createAnimatorProperty();
        mAnimator.setDuration(700).rotate(360).setLoopedCount(AnimatorProperty.INFINITE).setTarget(mImage);//.start();
        firtLoadStart();
    }


    public void startAnime(){
        if(mAnimator != null){
            mAnimator.start();
        }
    }

    public void firtLoadStart(){
        if(context != null){
            dispatcher =  context.getMainTaskDispatcher();
            dispatcher.delayDispatch(new Runnable() {
                @Override
                public void run() {
                    startAnime();
                }
            }, 50);
        }else{
            startAnime();
        }

    }

    /**
     * Refresh complete
     */
    @Override
    public void onFinish() {
        if(mAnimator != null){
            mAnimator.cancel();
        }
    }
}
