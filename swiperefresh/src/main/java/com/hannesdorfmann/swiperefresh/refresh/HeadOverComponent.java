/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.swiperefresh.refresh;

import com.hannesdorfmann.swiperefresh.util.DisplayUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

/**
 * The subclass needs to inherit this class to implement its own drop-down refresh header component
 *
 * @date 2020/12/27
 */
public abstract class HeadOverComponent extends StackLayout {

    public enum RefreshState {
        /**
         * Initial state
         */
        STATE_INIT,
        /**
         * The head of the drop-down refresh is visible
         */
        STATE_VISIBLE,
        /**
         * Refreshing status of
         */
        STATE_REFRESH,
        /**
         * Status beyond refresh distance
         */
        STATE_OVER,
        /**
         * The state after releasing the hand beyond the refresh position
         */
        STATE_OVER_RELEASE
    }

    protected RefreshState mState = RefreshState.STATE_INIT;
    /**
     * The minimum height at which the pull-down refresh is triggered. When the pull-down refresh happens to reach this distance, it will be refreshed directly,
     * If the drop-down distance exceeds this distance, scroll to this distance before refreshing
     */
    public int mPullRefreshHeight;
    /**
     * Minimum damping, the more users pull down, the less they follow
     */
    public float minDamp = 1.6f;
    /**
     * Maximum damping
     */
    public float maxDamp = 2.2f;

    public HeadOverComponent(Context context) {
        this(context, null);
    }

    public HeadOverComponent(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public HeadOverComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        mPullRefreshHeight = DisplayUtils.vp2px(context, 80);
        init();
    }

    /**
     * initialization
     */
    public abstract void init();

    /**
     * scroll
     *
     * @param scrollY
     * @param pullRefreshHeight The minimum height at which a pull-down refresh is triggered
     */
    public abstract void onScroll(int scrollY, int pullRefreshHeight);

    /**
     * Display head assembly
     */
    public abstract void onVisible();

    /**
     * Beyond the height of the head assembly, let go and start loading
     */
    public abstract void onOver();

    /**
     * Start refreshing
     */
    public abstract void onRefresh();

    /**
     * Refresh complete
     */
    public abstract void onFinish();

    public void setState(RefreshState state) {
        this.mState = state;
    }

    public RefreshState getState() {
        return mState;
    }
}
