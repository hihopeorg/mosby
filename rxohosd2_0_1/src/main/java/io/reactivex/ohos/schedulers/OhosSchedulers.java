/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.reactivex.ohos.schedulers;


import java.util.concurrent.Callable;

import io.reactivex.Scheduler;
import io.reactivex.ohos.plugins.RxOhosPlugins;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

public final class OhosSchedulers {

    private static final class MainHolder {

        static final Scheduler DEFAULT = new HandlerScheduler(new EventHandler(EventRunner.getMainEventRunner()));
    }

    private static final Scheduler MAIN_THREAD = RxOhosPlugins.initMainThreadScheduler(
            new Callable<Scheduler>() {
                @Override public Scheduler call() throws Exception {
                    return MainHolder.DEFAULT;
                }
            });

    /** A {@link Scheduler} which executes actions on the HarmonyOS main thread. */
    public static Scheduler mainThread() {
        return RxOhosPlugins.onMainThreadScheduler(MAIN_THREAD);
    }

    /** A {@link Scheduler} which executes actions on {@code looper}. */
    public static Scheduler from(EventRunner looper) {
        if (looper == null) throw new NullPointerException("looper == null");
        return new HandlerScheduler(new EventHandler(looper));
    }

    private OhosSchedulers() {
        throw new AssertionError("No instances.");
    }
}
