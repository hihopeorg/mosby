package com.hannesdorfmann.mosby3.sample.slice;

import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.data.RetainingLceViewState;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesView;
import com.hannesdorfmann.mosby3.sample.mvp.model.Country;
import ohos.aafwk.content.Intent;

import java.util.List;

public class RetainingCountriesAbilitySlice extends NotRetainingCountriesAbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override public LceViewState<List<Country>, CountriesView> createViewState() {
        return new RetainingLceViewState<>();
    }
}
