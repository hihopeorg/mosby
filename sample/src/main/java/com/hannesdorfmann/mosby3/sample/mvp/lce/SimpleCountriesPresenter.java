/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.sample.mvp.lce;


import com.hannesdorfmann.mosby3.mvp.MvpQueuingBasePresenter;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesPresenter;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesView;
import com.hannesdorfmann.mosby3.sample.mvp.model.CountriesAsyncLoader;
import com.hannesdorfmann.mosby3.sample.mvp.model.Country;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.List;

/**
 * @author Hannes Dorfmann
 */
public class SimpleCountriesPresenter extends MvpQueuingBasePresenter<CountriesView>
    implements CountriesPresenter {

  private static int PRESENTER_ID_FOR_LOGGING = 0;
  private static final String TAG = "CountriesPresenter" + (PRESENTER_ID_FOR_LOGGING++);
  private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0, TAG);

  private int failingCounter = 0;
  private CountriesAsyncLoader countriesLoader;

  public SimpleCountriesPresenter() {
  }

  @Override public void loadCountries(final boolean pullToRefresh) {


    onceViewAttached(new ViewAction<CountriesView>() {
      @Override public void run(CountriesView view) {
        view.showLoading(pullToRefresh);
      }
    });

    if (countriesLoader != null && !countriesLoader.isCancelled()) {
      countriesLoader.cancel(true);
    }

    countriesLoader = new CountriesAsyncLoader(++failingCounter % 2 != 0,
        new CountriesAsyncLoader.CountriesLoaderListener() {

          @Override public void onSuccess(final List<Country> countries) {

            onceViewAttached(new ViewAction<CountriesView>() {
              @Override public void run(CountriesView view) {
                view.setData(countries);
                view.showContent();
              }
            });
          }

          @Override public void onError(final Exception e) {

            onceViewAttached(new ViewAction<CountriesView>() {
              @Override public void run(CountriesView view) {
                view.showError(e, pullToRefresh);
              }
            });
          }
        });

    countriesLoader.execute();
  }

  @Override public void detachView() {
    super.detachView();
  }

  @Override public void destroy() {
    super.destroy();
    if (countriesLoader != null) {
      countriesLoader.cancel(true);
    }
  }

  @Override public void attachView(CountriesView view) {
    super.attachView(view);
  }
}
