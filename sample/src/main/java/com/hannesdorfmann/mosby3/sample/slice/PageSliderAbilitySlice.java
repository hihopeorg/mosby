package com.hannesdorfmann.mosby3.sample.slice;

import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.lce.containner.ComponentOwner;
import com.hannesdorfmann.mosby3.sample.mvp.lce.containner.MyCustomComponentOwner;
import com.hannesdorfmann.mosby3.sample.mvp.lce.containner.RetainingCountriesComponentOwner;
import com.hannesdorfmann.swiperefresh.util.DisplayUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import static ohos.agp.components.DependentLayout.LayoutConfig.MATCH_PARENT;
import static ohos.agp.components.DependentLayout.LayoutConfig.MATCH_CONTENT;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PageSliderAbilitySlice extends AbilitySlice {

    public static final String KEY_STATEPAGER = "ViewPagerActivity.STATE_PATER";
    PageSlider mPageSlider;
    TabList mTabList;
    private List<ComponentOwner> list = new ArrayList<>();
    boolean mStateFlag = false;

    private final String NESTED_TAG = "nestedFrag";
    private int selectPos = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_pageslider);

        mPageSlider = (PageSlider) findComponentById(ResourceTable.Id_mPageSlider);
        mTabList = (TabList) findComponentById(ResourceTable.Id_mTabList);

        mStateFlag = intent.getBooleanParam(KEY_STATEPAGER, false);
        initTabList();
        initPageSlider();
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void initTabList(){
        TabList.Tab tab;
        for(int i=0;i<5;i++){
            tab = mTabList.new Tab(getContext());
            tab.setText("TAB"+i);
            mTabList.addTab(tab);
        }

        mTabList.setTabLength(150);

        mTabList.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                selectPos = tab.getPosition();
                mPageSlider.setCurrentPage(tab.getPosition());
            }

            @Override
            public void onUnselected(TabList.Tab tab) {

            }

            @Override
            public void onReselected(TabList.Tab tab) {

            }
        });
        mTabList.selectTab(mTabList.getTabAt(0));
    }

    private void initPageSlider(){

        ComponentOwner mComponent;
        for(int i=0;i<5;i++){
            if(i % 2 == 0){
                mComponent = new RetainingCountriesComponentOwner(getAbility());
            }else{
                mComponent = new MyCustomComponentOwner(getAbility());
            }

            list.add(mComponent);
        }

        mPageSlider.setProvider(provider);
        mPageSlider.setPageCacheSize(1);
        mPageSlider.addPageChangedListener(new PageSlider.PageChangedListener(){
            @Override
            public void onPageSliding(int i, float v, int i1) {

            }

            @Override
            public void onPageSlideStateChanged(int i) {

            }

            @Override
            public void onPageChosen(int i) {
                selectPos = i;
                mTabList.selectTab(mTabList.getTabAt(i));

                ComponentOwner container = (ComponentOwner) list.get(i);
                container.instantiateComponent();
                provider.notifyDataChanged();
            }
        });
        mPageSlider.setCurrentPage(0,false);
        mPageSlider.setPageCacheSize(1);
        mPageSlider.setDraggedListener(Component.DRAG_VERTICAL, new Component.DraggedListener() {
            @Override
            public void onDragDown(Component component, DragInfo dragInfo) {
            }

            @Override
            public void onDragStart(Component component, DragInfo dragInfo) {
            }

            @Override
            public void onDragUpdate(Component component, DragInfo dragInfo) {
            }

            @Override
            public void onDragEnd(Component component, DragInfo dragInfo) {
            }

            @Override
            public void onDragCancel(Component component, DragInfo dragInfo) {
            }
        });
    }

    private PageSliderProvider provider = new PageSliderProvider() {
        @Override
        public int getCount() {
            return mTabList.getTabCount();
        }

        @Override
        public Object createPageInContainer(ComponentContainer componentContainer, int index) {
            if (index >= mTabList.getTabCount() || componentContainer == null) {
                return Optional.empty();
            }
            ComponentOwner container = (ComponentOwner) list.get(index);
            Component mComponent = container.getComponent();
            componentContainer.addComponent(mComponent, (int) DisplayUtils.getDisplayWidthInPx(getContext()), (int) DisplayUtils.getDisplayHeightInPx(getContext())-AttrHelper.vp2px(40, getContext()));
            if(index == selectPos)
                container.instantiateComponent();

            return mComponent;

        }

        @Override
        public void destroyPageFromContainer(ComponentContainer componentContainer, int index, Object object) {
            if (index >=  mTabList.getTabCount() || componentContainer == null) {
                return;
            }
            Component content = list.get(index).getComponent();
            componentContainer.removeComponent(content);
        }

        @Override
        public boolean isPageMatchToObject(Component component, Object object) {
            return component == object;
        }

        @Override
        public void startUpdate(ComponentContainer container) {
            super.startUpdate(container);
        }
    };
}
