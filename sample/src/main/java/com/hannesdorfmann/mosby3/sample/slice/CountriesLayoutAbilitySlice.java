package com.hannesdorfmann.mosby3.sample.slice;

import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.lce.layout.CountriesLayout;
import com.hannesdorfmann.swiperefresh.util.DisplayUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.LayoutScatter;

public class CountriesLayoutAbilitySlice extends AbilitySlice {

    CountriesLayout mCountriesLayout;
    private DependentLayout root;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_countries_layout_new);
        mCountriesLayout = (CountriesLayout) findComponentById(ResourceTable.Id_dummyId);
        mCountriesLayout.setmComponetContext(this);

        LayoutScatter scatter = LayoutScatter.getInstance(this);
        root = (DependentLayout) scatter.parse(ResourceTable.Layout_countries_list, null, false);
        mCountriesLayout.removeAllComponents();
        mCountriesLayout.addComponent(root, (int)DisplayUtils.getDisplayWidthInPx(getContext()), (int)DisplayUtils.getDisplayHeightInPx(getContext()));
        mCountriesLayout.initComponent();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
