package com.hannesdorfmann.mosby3.sample.mvp.lce.layout;

import com.hannesdorfmann.mosby3.sample.slice.CountriesLayoutAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class CountriesLayoutAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(CountriesLayoutAbilitySlice.class.getName());
    }
}
