package com.hannesdorfmann.mosby3.sample.mvp.model.custom;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

public class A implements Sequenceable {
  String name;

  public A(String name) {
    this.name = name;
  }
  private A(){

  }

  public String getName() {
    return name;
  }

  public static final Sequenceable.Producer PRODUCER = new Producer() {
    @Override
    public Object createFromParcel(Parcel parcel) {
      return new A(parcel);
    }
  };

  public A(Parcel parcel) {
    unmarshalling(parcel);
  }

  @Override
  public boolean marshalling(Parcel out) {
    return out.writeString(name);
  }

  @Override
  public boolean unmarshalling(Parcel in) {
    name = in.readString();
    return false;
  }
}
