/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.sample.mvp.lce.Fraction;


import com.hannesdorfmann.mosby3.mvp.lce.MvpLceFraction;
import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesAdapter;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesErrorMessage;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesPresenter;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesView;
import com.hannesdorfmann.mosby3.sample.mvp.lce.SimpleCountriesPresenter;
import com.hannesdorfmann.mosby3.sample.mvp.model.Country;
import com.hannesdorfmann.swiperefresh.refresh.IRefresh;
import com.hannesdorfmann.swiperefresh.refresh.SwipeRefreshLayout;
import com.hannesdorfmann.swiperefresh.refresh.TextOverComponent;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

import java.util.List;

public class CountriesFraction
    extends MvpLceFraction<SwipeRefreshLayout, List<Country>, CountriesView, CountriesPresenter>
    implements CountriesView, IRefresh.RefreshListener {

  protected ListContainer listContainer;
  private CountriesAdapter adapter;
  private Context context;

  public CountriesFraction(Context context){
    this.context = context;
  }

    public CountriesFraction() {

    }


    @Override
  public int getUIContent() {
    return ResourceTable.Layout_countries_list;
  }

  @Override
  public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
    return super.onComponentAttached(scatter, container, intent);
  }

  @Override
  public void initCompoment() {
    super.initCompoment();
    listContainer = (ListContainer) mComponent.findComponentById(ResourceTable.Id_listView);
    TextOverComponent overView = new TextOverComponent(context);
    contentView.setRefreshOverComponent(overView);
    contentView.setRefreshListener(this);
  }

  @Override
  protected void initLceComponetID() {
    loadingViewID = ResourceTable.Id_loadingView;
    contentViewID = ResourceTable.Id_contentView;
    errorViewID = ResourceTable.Id_errorView;
  }

  @Override
  protected void onStart(Intent intent) {
    super.onStart(intent);

    adapter = new CountriesAdapter(context);
    listContainer.setItemProvider(adapter);
    loadData(false);
  }

  @Override public void loadData(boolean pullToRefresh) {
    presenter.loadCountries(pullToRefresh);
  }

  @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {

    String result = CountriesErrorMessage.get(e, pullToRefresh, context);
    return result;
  }

  @Override public CountriesPresenter createPresenter() {
    return new SimpleCountriesPresenter();
  }


  @Override public void setData(List<Country> data) {
    adapter.setCountries(data);
    adapter.notifyDataChanged();
  }


  @Override
  public void showLoading(boolean pullToRefresh) {

  }

  @Override public void showContent() {
    super.showContent();
    contentView.refreshFinish();
  }

  @Override public void showError(Throwable e, boolean pullToRefresh) {
    super.showError(e, pullToRefresh);
    contentView.refreshFinish();
  }

  @Override
  public void onRefresh() {
    loadData(true);
  }

  @Override
  public boolean enableRefresh() {
    return true;
  }

}
