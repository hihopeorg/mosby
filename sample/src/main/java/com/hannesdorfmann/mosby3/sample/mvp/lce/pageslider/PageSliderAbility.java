package com.hannesdorfmann.mosby3.sample.mvp.lce.pageslider;

import com.hannesdorfmann.mosby3.sample.slice.PageSliderAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.utils.PacMap;

public class PageSliderAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(PageSliderAbilitySlice.class.getName());
    }

    @Override
    public void onSaveAbilityState(PacMap outState) {
        super.onSaveAbilityState(outState);
    }
}
