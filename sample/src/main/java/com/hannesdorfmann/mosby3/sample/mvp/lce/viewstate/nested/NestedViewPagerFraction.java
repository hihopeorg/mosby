/*
 * Copyright 2015. Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvp.lce.viewstate.nested;


import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.lce.containner.ComponentOwner;
import com.hannesdorfmann.mosby3.sample.mvp.lce.containner.NotRetainingCountriesComponentOwner;
import com.hannesdorfmann.swiperefresh.util.DisplayUtils;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Hannes Dorfmann
 */
public class NestedViewPagerFraction extends Fraction {

  Component mComponent;
  PageSlider mPageSlider;
  TabList mTabList;
  private List<ComponentOwner> list = new ArrayList<>();

  private Context context;
  public NestedViewPagerFraction(Context context){
    this.context = context;
  }
  private int selectedPos = 0;

  @Override
  protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
    mComponent = scatter.parse(ResourceTable.Layout_ability_pageslider, container, false);
    initComponent();
    return mComponent;
  }
  private void initComponent(){
    mPageSlider = (PageSlider) mComponent.findComponentById(ResourceTable.Id_mPageSlider);
    mTabList = (TabList) mComponent.findComponentById(ResourceTable.Id_mTabList);
    initTabList();
    initPageSlider();
  }

  @Override
  protected void onStop() {
    super.onStop();
  }

  private void initTabList(){
    TabList.Tab tab;
    for(int i=0;i<5;i++){
      tab = mTabList.new Tab(context);
      tab.setText("FRACTION "+i);
      mTabList.addTab(tab);
    }

    mTabList.setTabLength(300);

    mTabList.addTabSelectedListener(new TabList.TabSelectedListener() {
      @Override
      public void onSelected(TabList.Tab tab) {
        selectedPos = tab.getPosition();
        mPageSlider.setCurrentPage(tab.getPosition());

      }

      @Override
      public void onUnselected(TabList.Tab tab) {

      }

      @Override
      public void onReselected(TabList.Tab tab) {

      }
    });
    mTabList.selectTab(mTabList.getTabAt(0));
  }

  private void initPageSlider(){

    for(int i=0;i<5;i++){
      ComponentOwner mComponent = new NotRetainingCountriesComponentOwner(getFractionAbility());
      list.add(mComponent);
    }

    mPageSlider.setProvider(provider);
    mPageSlider.addPageChangedListener(new PageSlider.PageChangedListener(){
      @Override
      public void onPageSliding(int i, float v, int i1) {

      }

      @Override
      public void onPageSlideStateChanged(int i) {

      }

      @Override
      public void onPageChosen(int i) {
        selectedPos = i;
        mTabList.selectTab(mTabList.getTabAt(i));

        ComponentOwner container = (ComponentOwner) list.get(i);
        container.instantiateComponent();
        provider.notifyDataChanged();
      }
    });
    mPageSlider.setCurrentPage(0,false);
    mPageSlider.setDraggedListener(Component.DRAG_VERTICAL, new Component.DraggedListener() {
      @Override
      public void onDragDown(Component component, DragInfo dragInfo) {
      }

      @Override
      public void onDragStart(Component component, DragInfo dragInfo) {
      }

      @Override
      public void onDragUpdate(Component component, DragInfo dragInfo) {
      }

      @Override
      public void onDragEnd(Component component, DragInfo dragInfo) {
      }

      @Override
      public void onDragCancel(Component component, DragInfo dragInfo) {
      }
    });

  }

  private PageSliderProvider provider = new PageSliderProvider() {
    @Override
    public int getCount() {
      return mTabList.getTabCount();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int index) {

      if (index >= mTabList.getTabCount() || componentContainer == null) {
        return Optional.empty();
      }

      ComponentOwner container = (ComponentOwner) list.get(index);
      componentContainer.addComponent(container.getComponent(), (int) DisplayUtils.getDisplayWidthInPx(context), (int) DisplayUtils.getDisplayHeightInPx(context));
      if(index == selectedPos)
        container.instantiateComponent();
      return container.getComponent();
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int index, Object object) {
      if (index >=  mTabList.getTabCount() || componentContainer == null) {
        return;
      }
      Component content = list.get(index).getComponent();
      componentContainer.removeComponent(content);
      return;
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
      return component == object;
    }

    @Override
    public void startUpdate(ComponentContainer container) {
      super.startUpdate(container);
    }
  };

}
