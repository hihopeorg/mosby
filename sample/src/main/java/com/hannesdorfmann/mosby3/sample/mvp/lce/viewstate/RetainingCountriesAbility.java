package com.hannesdorfmann.mosby3.sample.mvp.lce.viewstate;

import com.hannesdorfmann.mosby3.sample.slice.RetainingCountriesAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class RetainingCountriesAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(RetainingCountriesAbilitySlice.class.getName());
    }
}
