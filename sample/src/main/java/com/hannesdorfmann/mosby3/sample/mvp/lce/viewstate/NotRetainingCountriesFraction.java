/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.sample.mvp.lce.viewstate;

import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.MvpLceViewStateFraction;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.data.CastedArrayListLceViewState;
import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesAdapter;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesErrorMessage;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesPresenter;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesView;
import com.hannesdorfmann.mosby3.sample.mvp.lce.SimpleCountriesPresenter;
import com.hannesdorfmann.mosby3.sample.mvp.model.Country;
import com.hannesdorfmann.swiperefresh.refresh.IRefresh;
import com.hannesdorfmann.swiperefresh.refresh.SwipeRefreshLayout;
import com.hannesdorfmann.swiperefresh.refresh.TextOverComponent;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

import java.util.List;

/**
 * This is an example of a NOT retaining fragment. It serializes and deserializes the viewstate
 * into a bundle
 *
 * @author Hannes Dorfmann
 */
public class NotRetainingCountriesFraction extends
        MvpLceViewStateFraction<SwipeRefreshLayout, List<Country>, CountriesView, CountriesPresenter>
        implements CountriesView, IRefresh.RefreshListener {

    protected ListContainer listContainer;
    private Context context;
    CountriesAdapter adapter;

    public NotRetainingCountriesFraction(Context context) {
        this.context = context;
    }

    @Override
    protected void initCompoment() {
        super.initCompoment();
        listContainer = (ListContainer) mComponent.findComponentById(ResourceTable.Id_listView);
        TextOverComponent overView = new TextOverComponent(context);
        contentView.setRefreshOverComponent(overView);
        contentView.setRefreshListener(this);
    }

    @Override
    protected void initLceComponetID() {
        loadingViewID = ResourceTable.Id_loadingView;
        contentViewID = ResourceTable.Id_contentView;
        errorViewID = ResourceTable.Id_errorView;
    }

    @Override
    public LceViewState<List<Country>, CountriesView> createViewState() {
        return new CastedArrayListLceViewState<>();
    }


    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.loadCountries(pullToRefresh);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return CountriesErrorMessage.get(e, pullToRefresh, getFractionAbility());
    }

    @Override
    public CountriesPresenter createPresenter() {
        return new SimpleCountriesPresenter();
    }

    @Override
    protected int getUIContent() {
        return ResourceTable.Layout_countries_list;
    }

    @Override
    public void setData(List<Country> data) {
        adapter.setCountries(data);
        adapter.notifyDataChanged();
    }

    @Override
    public void showContent() {
        super.showContent();
        contentView.refreshFinish();
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        contentView.refreshFinish();
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);
    }

    @Override
    public List<Country> getData() {
        return adapter == null ? null : adapter.getCountries();
    }

    @Override
    public void onNewViewStateInstance() {
        super.onNewViewStateInstance();
    }

    @Override
    public void onViewStateInstanceRestored(boolean instanceStateRetainedInMemory) {
        super.onViewStateInstanceRestored(instanceStateRetainedInMemory);
    }

    @Override
    public void setRestoringViewState(boolean restoringViewState) {
        super.setRestoringViewState(restoringViewState);
    }

    @Override
    public String toString() {
        return super.toString() + " NotRetainingCountriesFraction" ;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        adapter = new CountriesAdapter(getFractionAbility());
        listContainer.setItemProvider(adapter);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public boolean enableRefresh() {
        return true;
    }

}
