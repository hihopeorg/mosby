/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.sample.mvp;

import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.model.Country;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

public class CountriesAdapter extends BaseItemProvider {

  private Context context;
  private List<Country> countries;
  public CountriesAdapter(){
    super();
  }

  public CountriesAdapter(Context context){
    this.context = context;
  }

  public CountriesAdapter(Context context, List<Country> countries){
    this.context = context;
    this.countries = countries;
  }

  public void setCountries(List<Country> countries) {
    this.countries = countries;
  }

  public List<Country> getCountries() {
    return countries;
  }

  @Override
  public int getCount() {
    return countries == null ? 0 : countries.size();
  }

  @Override
  public Country getItem(int i) {
    return countries == null ? null : countries.get(i);
  }

  @Override
  public long getItemId(int i) {
    return i;
  }

  @Override
  public Component getComponent(int index, Component component, ComponentContainer componentContainer) {
    Component itemComponent = component;
    ViewHolder viewHolder;
    if (itemComponent == null) {
      itemComponent = LayoutScatter.getInstance(componentContainer.getContext())
              .parse(ResourceTable.Layout_row_text, componentContainer, false);
    }
    Country content = (Country) getItem(index);
    viewHolder = new ViewHolder(itemComponent);
    viewHolder.textView.setText(content.toString());

    return itemComponent;
  }

  public class ViewHolder{
    public Text textView;
    public ViewHolder(Component itemComponent) {
      textView = (Text) itemComponent.findComponentById(ResourceTable.Id_textView);
    }

  }
}



