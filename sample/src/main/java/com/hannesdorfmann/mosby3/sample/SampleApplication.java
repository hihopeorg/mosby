package com.hannesdorfmann.mosby3.sample;

import ohos.aafwk.ability.AbilityPackage;

public class SampleApplication extends AbilityPackage {


    @Override
    public void onInitialize() {
        super.onInitialize();

        /*
        ActivityMvpViewStateDelegateImpl.DEBUG = true;
        FragmentMvpViewStateDelegateImpl.DEBUG = true;
        ViewGroupMvpViewStateDelegateImpl.DEBUG = true;
        ActivityMvpDelegateImpl.DEBUG = true;
        FragmentMvpDelegateImpl.DEBUG=true;
        ViewGroupMvpDelegateImpl.DEBUG = true;
        */
    }
}
