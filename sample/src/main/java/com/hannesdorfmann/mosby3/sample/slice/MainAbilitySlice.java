/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.sample.slice;

import com.hannesdorfmann.mosby3.sample.mvp.lce.viewstate.RetainingCountriesAbility;
import com.hannesdorfmann.mosby3.sample.StackLayoutContainerAbility;
import com.hannesdorfmann.mosby3.sample.mvp.lce.ability.CountriesAbility;
import com.hannesdorfmann.mosby3.sample.mvp.lce.layout.CountriesLayoutAbility;
import com.hannesdorfmann.mosby3.sample.mvp.lce.pageslider.PageSliderAbility;
import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.customviewstate.MyCustomAbility;
import com.hannesdorfmann.mosby3.sample.mvp.lce.viewstate.NotRetainingCountriesAbility;
import com.hannesdorfmann.mosby3.sample.mvp.lce.viewstate.RetainingCountriesFragmentEmbededInXmlAbility;
import com.hannesdorfmann.mosby3.sample.provider.ListItemProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;

public class MainAbilitySlice extends AbilitySlice implements ListContainer.ItemClickedListener {

    Demo[] demos;

    ListContainer listView;

    public static final String BOUNDEL_NAME = "com.hannesdorfmann.mosby3.sample";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        listView = (ListContainer) findComponentById(ResourceTable.Id_listView);
        demos = createDemos();
        listView.setItemProvider(new ListItemProvider(demos, this));
        listView.setItemClickedListener(this);

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private Demo[] createDemos() {
        return new Demo[]{
                new Demo("Simple LceActivity", getIntetntData(BOUNDEL_NAME, CountriesAbility.class.getName())),
                new Demo("Simple LceFragment", getIntetntData(BOUNDEL_NAME, StackLayoutContainerAbility.class.getName()).setParam("fragment",
                        "CountriesFragment")),
                new Demo("RetainingViewsState LceFragment", getIntetntData(BOUNDEL_NAME, StackLayoutContainerAbility.class.getName()).setParam("fragment",
                        "RetainingCountriesFragment")),
                new Demo("Retaining by using Parcelable ViewsState LceFragment",
                        getIntetntData(BOUNDEL_NAME, StackLayoutContainerAbility.class.getName()).setParam("fragment",
                                "NotRetainingCountriesFragment")),

                new Demo("Retaining ViewsState LceActivity",
                        getIntetntData(BOUNDEL_NAME, RetainingCountriesAbility.class.getName())),
                new Demo("Retaining by using Parcelable ViewsState LceActivity",
                        getIntetntData(BOUNDEL_NAME, NotRetainingCountriesAbility.class.getName())),

                new Demo("MVP FrameLayout", getIntetntData(BOUNDEL_NAME, CountriesLayoutAbility.class.getName())),

                new Demo("Custom ViewsState Fragment",
                        getIntetntData(BOUNDEL_NAME, StackLayoutContainerAbility.class.getName()).setParam("fragment",
                                "CustomViewStateFragment")),

                new Demo("Custom ViewState Activity", getIntetntData(BOUNDEL_NAME, MyCustomAbility.class.getName())),
                new Demo("Nested ViewState CountriesFragment",
                        getIntetntData(BOUNDEL_NAME, StackLayoutContainerAbility.class.getName()).setParam("fragment",
                                "NestedNotRetainingFragment")),
                new Demo("Nested ViewState CountriesFragment ViewPager",
                        getIntetntData(BOUNDEL_NAME, StackLayoutContainerAbility.class.getName()).setParam("fragment",
                                "NestedNotRetainingViewPagerFragment")),
                new Demo("Retaining ViewState Fragment embededed in activities xml layout ",
                        getIntetntData(BOUNDEL_NAME, RetainingCountriesFragmentEmbededInXmlAbility.class.getName())),

                new Demo("ViewPager", getIntetntData(BOUNDEL_NAME, PageSliderAbility.class.getName())),

                new Demo("ViewPager with FragmentSTATEPagerAdapter",
                        getIntetntData(BOUNDEL_NAME, PageSliderAbility.class.getName()).setParam(PageSliderAbilitySlice.KEY_STATEPAGER, true))
        };
    }

    @Override
    public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
        startAbility(demos[position].intent);
    }

    public static class Demo {
        String name;
        Intent intent;

        private Demo(String name, Intent intent) {
            this.name = name;
            this.intent = intent;
        }

        public String toString() {
            return name;
        }
    }

    public Intent getIntetntData(String bundelName, String abilityName) {
        Intent secondIntent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(bundelName)
                .withAbilityName(abilityName)
                .build();
        secondIntent.setOperation(operation);
        return secondIntent;
    }
}
