package com.hannesdorfmann.mosby3.sample.slice;

import com.hannesdorfmann.mosby3.sample.ResourceTable;
import ohos.aafwk.content.Intent;

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceAbilitySlice;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesAdapter;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesErrorMessage;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesPresenter;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesView;
import com.hannesdorfmann.mosby3.sample.mvp.lce.SimpleCountriesPresenter;
import com.hannesdorfmann.mosby3.sample.mvp.model.Country;
import com.hannesdorfmann.swiperefresh.refresh.IRefresh;
import com.hannesdorfmann.swiperefresh.refresh.SwipeRefreshLayout;
import com.hannesdorfmann.swiperefresh.refresh.TextOverComponent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;

import java.util.List;

public class CountriesAbilitySlice extends MvpLceAbilitySlice<SwipeRefreshLayout, List<Country>, CountriesView, CountriesPresenter>
        implements CountriesView, IRefresh.RefreshListener {

    protected ListContainer listContainer;

    public CountriesAdapter getAdapter() {
        return adapter;
    }

    private CountriesAdapter adapter;

    @Override
    protected void initCompoment() {
        super.initCompoment();
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_listView);
    }

    @Override
    protected void initLceComponetID() {
        loadingViewID = ResourceTable.Id_loadingView;
        contentViewID = ResourceTable.Id_contentView;
        errorViewID = ResourceTable.Id_errorView;
    }

    @Override
    public CountriesPresenter createPresenter() {
        return new SimpleCountriesPresenter();
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        TextOverComponent overView = new TextOverComponent(this);
        contentView.setRefreshOverComponent(overView);
        contentView.setRefreshListener(this);

        adapter = new CountriesAdapter(this);
        if (listContainer != null && adapter != null) {
            listContainer.setItemProvider(adapter);
        }

        loadData(false);
    }

    @Override
    protected ComponentContainer getUIContent() {
        LayoutScatter mLayoutScatter = LayoutScatter.getInstance(this);
        return (ComponentContainer) mLayoutScatter.parse(ResourceTable.Layout_countries_list, null, false);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return CountriesErrorMessage.get(e, pullToRefresh, this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void setData(List<Country> data) {
        adapter.setCountries(data);
        adapter.notifyDataChanged();
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.loadCountries(pullToRefresh);
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);
    }

    @Override
    public void showContent() {
        super.showContent();
        contentView.refreshFinish();
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        contentView.refreshFinish();
    }


    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public boolean enableRefresh() {
        return true;
    }
}
