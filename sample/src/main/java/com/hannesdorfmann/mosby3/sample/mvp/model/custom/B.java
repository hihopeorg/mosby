package com.hannesdorfmann.mosby3.sample.mvp.model.custom;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

public class B implements Sequenceable {
  String foo;

  public B(String foo) {
    this.foo = foo;
  }
  private B(){

  }

  public String getFoo() {
    return foo;
  }

  @Override
  public boolean hasFileDescriptor() {
    return false;
  }

  public static final Sequenceable.Producer PRODUCER = new Producer() {
    @Override
    public Object createFromParcel(Parcel parcel) {
      return new A(parcel);
    }
  };

  public B(Parcel parcel) {
    unmarshalling(parcel);
  }

  @Override
  public boolean marshalling(Parcel out) {
    return out.writeString(foo);
  }

  @Override
  public boolean unmarshalling(Parcel in) {
    foo = in.readString();
    return false;
  }

}
