package com.hannesdorfmann.mosby3.sample.mvp.lce.viewstate;

import com.hannesdorfmann.mosby3.sample.slice.RetainingCountriesFragmentEmbededInXmlAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

public class RetainingCountriesFragmentEmbededInXmlAbility extends FractionAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(RetainingCountriesFragmentEmbededInXmlAbilitySlice.class.getName());
    }
}
