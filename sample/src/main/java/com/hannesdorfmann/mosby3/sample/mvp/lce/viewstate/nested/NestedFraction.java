/*
 * Copyright 2015. Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvp.lce.viewstate.nested;

import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.lce.viewstate.NotRetainingCountriesFraction;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.hiviewdfx.HiLog;

/**
 * @author Hannes Dorfmann
 */

public class NestedFraction extends Fraction {

  private final String NESTED_TAG = "nestedFrag";
  private Component mComponent;

  @Override
  protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
    mComponent = scatter.parse(ResourceTable.Layout_ability_slice_container, container, false);
    initCompoment();
    return super.onComponentAttached(scatter, container, intent);
  }

  private void initCompoment(){

  }

  @Override
  protected void onStart(Intent intent) {
    super.onStart(intent);
    {
      ((FractionAbility) getFractionAbility()).getFractionManager()
              .startFractionScheduler()
              .add(ResourceTable.Id_stackContainer, new NotRetainingCountriesFraction(this), NESTED_TAG)
              .submit();
    }

  }

  @Override
  protected void onComponentDetach() {
    super.onComponentDetach();
  }

  @Override
  protected void onStop() {
    super.onStop();
  }

}
