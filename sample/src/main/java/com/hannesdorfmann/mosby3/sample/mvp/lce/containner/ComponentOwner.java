package com.hannesdorfmann.mosby3.sample.mvp.lce.containner;

import ohos.agp.components.Component;

public interface ComponentOwner {

    Component getComponent();

    void instantiateComponent();
}
