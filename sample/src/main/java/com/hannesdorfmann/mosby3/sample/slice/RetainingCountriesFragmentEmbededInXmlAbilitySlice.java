package com.hannesdorfmann.mosby3.sample.slice;

import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.lce.viewstate.RetainingCountriesFraction;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.StackLayout;

//TODO　布局不能直接引用Fraction,只能在代码中引用实现，该功能未实现
public class RetainingCountriesFragmentEmbededInXmlAbilitySlice extends AbilitySlice {

    private StackLayout stackContainer;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_retaining_countries_fragment_embeded_in_xml);

        stackContainer = (StackLayout) findComponentById(ResourceTable.Id_stackContainer);
        ((FractionAbility) getAbility()).getFractionManager()
                .startFractionScheduler()
                .add(ResourceTable.Id_stackContainer, new RetainingCountriesFraction(this))
                .submit();

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
