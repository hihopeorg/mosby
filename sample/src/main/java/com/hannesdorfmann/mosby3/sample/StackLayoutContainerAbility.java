package com.hannesdorfmann.mosby3.sample;

import ohos.aafwk.ability.fraction.FractionAbility;
import com.hannesdorfmann.mosby3.sample.slice.StackLayoutContainerAbilitySlice;
import ohos.aafwk.content.Intent;

public class StackLayoutContainerAbility extends FractionAbility {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(StackLayoutContainerAbilitySlice.class.getName());
    }

}
