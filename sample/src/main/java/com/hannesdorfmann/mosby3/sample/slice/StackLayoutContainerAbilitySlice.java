package com.hannesdorfmann.mosby3.sample.slice;

import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.customviewstate.MyCustomFraction;
import com.hannesdorfmann.mosby3.sample.mvp.lce.Fraction.CountriesFraction;
import com.hannesdorfmann.mosby3.sample.mvp.lce.viewstate.NotRetainingCountriesFraction;
import com.hannesdorfmann.mosby3.sample.mvp.lce.viewstate.RetainingCountriesFraction;
import com.hannesdorfmann.mosby3.sample.mvp.lce.viewstate.nested.NestedFraction;
import com.hannesdorfmann.mosby3.sample.mvp.lce.viewstate.nested.NestedViewPagerFraction;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.StackLayout;

public class StackLayoutContainerAbilitySlice extends AbilitySlice {

    private StackLayout stackContainer;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_slice_container);

        stackContainer = (StackLayout) findComponentById(ResourceTable.Id_stackContainer);
        String fragmentName = intent.getStringParam("fragment");
        Fraction mFraction = getFraction(fragmentName);
        if(mFraction != null){
            ((FractionAbility) getAbility()).getFractionManager()
                    .startFractionScheduler()
                    .add(ResourceTable.Id_stackContainer, mFraction, fragmentName)
                    .submit();
        }

        //使用AbilitySlice实现方式
//        AbilitySlice mAbilitySlice = getAbilitySlice(intent);
//        if(mAbilitySlice!= null){
//            present(mAbilitySlice, new Intent());
//        }else{
//            onBackPressed();
//        }

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private Fraction getFraction(String fragmentName){
        Fraction mFraction;

        if (fragmentName == null || fragmentName.equals("")) {
            return null;
        }

        if ("CountriesFragment".equals(fragmentName)) {
            return new CountriesFraction(this);
        }

        if ("RetainingCountriesFragment".equals(fragmentName)){
            return new RetainingCountriesFraction(this);
        }

        if ("NotRetainingCountriesFragment".equals(fragmentName)){
            return new NotRetainingCountriesFraction(this);
        }

        if ("CustomViewStateFragment".equals(fragmentName)){
            return new MyCustomFraction();
        }

        if ("NestedNotRetainingFragment".equals(fragmentName)){
            return new NotRetainingCountriesFraction(this);
            //TODO 不支持Fraction嵌套，直接调用
//            return new NestedFraction();
        }

        if ("NestedNotRetainingViewPagerFragment".equals(fragmentName)){
            return new NestedViewPagerFraction(this);
        }

        return null;
    }

}
