/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.sample.mvp;

import com.hannesdorfmann.mosby3.sample.ResourceTable;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;


public class CountriesErrorMessage {
  private static final String TAG = CountriesErrorMessage.class.getSimpleName();
  private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0, TAG);

  public static String get(Throwable e, boolean pullToRefresh, Context c) {
    // TODO distinguish type of exception and retrun different strings

    String errorCountries = "";
    String errorCountriesRetry = "";
    try{
      errorCountries = c.getResourceManager().getElement(ResourceTable.String_error_countries).getString();
      errorCountriesRetry = c.getResourceManager().getElement(ResourceTable.String_error_countries_retry).getString();
    }catch (Exception err){
      HiLog.error(LABEL,"err e:"+err.getMessage());
    }

    if (pullToRefresh) {
      return errorCountries;
    } else {
      return errorCountriesRetry;
    }
  }
}
