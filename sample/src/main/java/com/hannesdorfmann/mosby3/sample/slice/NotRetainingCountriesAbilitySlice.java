package com.hannesdorfmann.mosby3.sample.slice;

import com.hannesdorfmann.mosby3.sample.ResourceTable;
import ohos.aafwk.content.Intent;

import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.MvpLceViewStateAbility;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.MvpLceViewStateAbilitySlice;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.data.CastedArrayListLceViewState;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesAdapter;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesErrorMessage;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesPresenter;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesView;
import com.hannesdorfmann.mosby3.sample.mvp.lce.SimpleCountriesPresenter;
import com.hannesdorfmann.mosby3.sample.mvp.model.Country;
import com.hannesdorfmann.swiperefresh.refresh.IRefresh;
import com.hannesdorfmann.swiperefresh.refresh.SwipeRefreshLayout;
import com.hannesdorfmann.swiperefresh.refresh.TextOverComponent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.List;

public class NotRetainingCountriesAbilitySlice extends
        MvpLceViewStateAbilitySlice<SwipeRefreshLayout, List<Country>, CountriesView, CountriesPresenter>
        implements CountriesView, IRefresh.RefreshListener {

    protected ListContainer listContainer;
    CountriesAdapter adapter;

    @Override
    protected ComponentContainer getUIContent() {
        LayoutScatter mLayoutScatter = LayoutScatter.getInstance(this);
        return (ComponentContainer) mLayoutScatter.parse(ResourceTable.Layout_countries_list, null, false);
    }

    @Override
    protected void initCompoment() {
        super.initCompoment();
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_listView);
        TextOverComponent overView = new TextOverComponent(this);
        contentView.setRefreshOverComponent(overView);
        contentView.setRefreshListener(this);
    }

    @Override
    protected void initLceComponetID() {
        loadingViewID = ResourceTable.Id_loadingView;
        contentViewID = ResourceTable.Id_contentView;
        errorViewID = ResourceTable.Id_errorView;
    }

    @Override
    public CountriesPresenter createPresenter() {
        return new SimpleCountriesPresenter();
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        adapter = new CountriesAdapter(this);
        listContainer.setItemProvider(adapter);
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);
    }

    @Override
    public List<Country> getData() {
        return adapter == null ? null : adapter.getCountries();
    }

    @Override
    public void showContent() {
        super.showContent();
        contentView.refreshFinish();
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return CountriesErrorMessage.get(e, pullToRefresh, this);
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        contentView.refreshFinish();
    }

    @Override
    public void setData(List<Country> data) {
        adapter.setCountries(data);
        adapter.notifyDataChanged();
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.loadCountries(pullToRefresh);
    }

    @Override
    public LceViewState<List<Country>, CountriesView> createViewState() {
        return new CastedArrayListLceViewState<>();
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public boolean enableRefresh() {
        return true;
    }

}
