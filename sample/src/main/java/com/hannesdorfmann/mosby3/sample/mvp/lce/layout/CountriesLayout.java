package com.hannesdorfmann.mosby3.sample.mvp.lce.layout;

import com.hannesdorfmann.mosby3.mvp.viewstate.layout.MvpViewStateStackLayout;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.data.CastedArrayListLceViewState;


import java.util.List;

import com.hannesdorfmann.mosby3.mvp.widget.CirleProgressImg;
import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesAdapter;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesErrorMessage;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesPresenter;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesView;
import com.hannesdorfmann.mosby3.sample.mvp.lce.SimpleCountriesPresenter;
import com.hannesdorfmann.mosby3.sample.mvp.model.Country;
import com.hannesdorfmann.swiperefresh.refresh.IRefresh;
import com.hannesdorfmann.swiperefresh.refresh.SwipeRefreshLayout;
import com.hannesdorfmann.swiperefresh.refresh.TextOverComponent;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.Sequenceable;

/**
 * @author Hannes Dorfmann
 */
public class CountriesLayout extends MvpViewStateStackLayout<CountriesView, CountriesPresenter, CastedArrayListLceViewState<List<Country>, CountriesView>>
    implements CountriesView, IRefresh.RefreshListener {

  CirleProgressImg loadingView;
  Text errorView;
  SwipeRefreshLayout contentView;
  ListContainer listView;
  private CountriesAdapter adapter;


  public CountriesLayout(Context context) {
    super(context);
  }

  public CountriesLayout(Context context, AttrSet attrs) {
    super(context, attrs);
  }

  public CountriesLayout(Context context, AttrSet attrs, String styleName) {
    super(context, attrs, styleName);
  }

  @Override
  protected void onAttachedToWindowDo(Component component){

  }

  @Override
  protected void onDetachedFromWindowDo(Component component) {
  }


  public void initComponent(){
    loadingView = (CirleProgressImg) findComponentById(ResourceTable.Id_loadingView);
    errorView = (Text) findComponentById(ResourceTable.Id_errorView);
    contentView = (SwipeRefreshLayout) findComponentById(ResourceTable.Id_contentView);

    TextOverComponent overView = new TextOverComponent(context);
    contentView.setRefreshOverComponent(overView);
    contentView.setRefreshListener(this);

    listView = (ListContainer) findComponentById(ResourceTable.Id_listView);
    errorView.setClickedListener(new ClickedListener() {
      @Override
      public void onClick(Component component) {
        loadData(false);
      }
    });

    adapter = new CountriesAdapter(context);
    listView.setItemProvider(adapter);
  }



  public void initComponent(Component component){
    loadingView = (CirleProgressImg) component.findComponentById(ResourceTable.Id_loadingView);
    errorView = (Text) component.findComponentById(ResourceTable.Id_errorView);
    contentView = (SwipeRefreshLayout) component.findComponentById(ResourceTable.Id_contentView);

    TextOverComponent overView = new TextOverComponent(context);
    contentView.setRefreshOverComponent(overView);
    contentView.setRefreshListener(this);

    listView = (ListContainer) component.findComponentById(ResourceTable.Id_listView);
    errorView.setClickedListener(new ClickedListener() {
      @Override
      public void onClick(Component component) {
        loadData(false);
      }
    });

    adapter = new CountriesAdapter(context);
    listView.setItemProvider(adapter);
  }


  @Override public CountriesPresenter createPresenter() {
    return new SimpleCountriesPresenter();
  }

  @Override public CastedArrayListLceViewState<List<Country>, CountriesView> createViewState() {
    return new CastedArrayListLceViewState<List<Country>, CountriesView>();
  }

  @Override public void onNewViewStateInstance() {
    loadData(false);
  }

  @Override public void showLoading(boolean pullToRefresh) {
    errorView.setVisibility(Component.HIDE);
    loadingView.setVisibility(Component.VISIBLE);

    if (pullToRefresh) {
      contentView.setVisibility(Component.VISIBLE);
    } else {
      loadingView.setVisibility(Component.VISIBLE);
      contentView.setVisibility(Component.HIDE);
    }
    getViewState().setStateShowLoading(pullToRefresh);
  }

  @Override public void showContent() {
    loadingView.setVisibility(Component.HIDE);
    errorView.setVisibility(Component.HIDE);
    contentView.setVisibility(Component.VISIBLE);

    contentView.refreshFinish();
    getViewState().setStateShowContent(adapter.getCountries());
  }

  @Override public void showError(Throwable e, boolean pullToRefresh) {
    getViewState().setStateShowError(e, pullToRefresh);
    loadingView.setVisibility(Component.HIDE);

    if(contentView != null){
      contentView.refreshFinish();
    }
    if (pullToRefresh) {
      if (!isRestoringViewState()) {
      }
    } else {
      contentView.setVisibility(Component.HIDE);
      String msg = CountriesErrorMessage.get(e, pullToRefresh, getContext());
      errorView.setText(msg);
      errorView.setVisibility(Component.VISIBLE);
    }
  }

  @Override public void setData(List<Country> data) {
    adapter.setCountries(data);
    adapter.notifyDataChanged();
  }

  @Override public void loadData(boolean pullToRefresh) {
    presenter.loadCountries(pullToRefresh);
  }

  @Override
  public Sequenceable superOnSaveInstanceState() {
    return null;
  }

  @Override
  public void onRefresh() {
    loadData(true);
  }

  @Override
  public boolean enableRefresh() {
    return true;
  }

}

