/*
 * Copyright 2015 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.sample.mvp.customviewstate;

import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.mvp.viewstate.MvpViewStateAbilitySlice;
import com.hannesdorfmann.mosby3.sample.mvp.model.custom.A;
import com.hannesdorfmann.mosby3.sample.mvp.model.custom.B;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

/**
 * @author Hannes Dorfmann
 */
public class MyCustomAbilitySlice extends MvpViewStateAbilitySlice<MyCustomView, MyCustomPresenter, MyCustomViewState>
    implements MyCustomView {


  Text aView;
  Text bView;
  Button loadA,loadB;


  @Override
  protected void onStart(Intent intent) {
    super.onStart(intent);
    setUIContent(ResourceTable.Layout_my_custom_view);
    aView = (Text) findComponentById(ResourceTable.Id_textViewA);
    bView = (Text) findComponentById(ResourceTable.Id_textViewB);
    loadA = (Button) findComponentById(ResourceTable.Id_loadA);
    loadB = (Button) findComponentById(ResourceTable.Id_loadB);

    loadA.setClickedListener(new Component.ClickedListener() {
      @Override
      public void onClick(Component component) {
        presenter.doA();
      }
    });

    loadB.setClickedListener(new Component.ClickedListener() {
      @Override
      public void onClick(Component component) {
        presenter.doB();
      }
    });
  }

  @Override public MyCustomViewState createViewState() {
    return new MyCustomViewState();
  }

  @Override public void onNewViewStateInstance() {
    presenter.doA();
  }

  @Override public MyCustomPresenter createPresenter() {
    return new MyCustomPresenter();
  }

  @Override public void showA(A a) {
    viewState.setShowingA(true);
    viewState.setData(a);
    aView.setText(a.getName());
    aView.setVisibility(Component.VISIBLE);
    bView.setVisibility(Component.HIDE);
  }

  @Override public void showB(B b) {
    viewState.setShowingA(false);
    viewState.setData(b);
    bView.setText(b.getFoo());
    aView.setVisibility(Component.HIDE);
    bView.setVisibility(Component.VISIBLE);
  }

}
