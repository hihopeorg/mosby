package com.hannesdorfmann.mosby3.sample.mvp.lce.containner;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.customviewstate.MyCustomPresenter;
import com.hannesdorfmann.mosby3.sample.mvp.customviewstate.MyCustomView;
import com.hannesdorfmann.mosby3.sample.mvp.model.custom.A;
import com.hannesdorfmann.mosby3.sample.mvp.model.custom.B;
import com.hannesdorfmann.swiperefresh.refresh.TextOverComponent;
import ohos.agp.components.*;
import ohos.app.AbilityContext;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class MyCustomComponentOwner<V extends MvpView, P extends MvpPresenter<V>> implements ComponentOwner, MyCustomView {

    private AbilityContext myContext;
    private ComponentContainer root;

    MyCustomPresenter presenter;

    Text aView;
    Text bView;
    Button loadA,loadB;

    public MyCustomComponentOwner(AbilityContext context) {
        myContext = context;
        init(context);
    }

    private void init(AbilityContext context) {

        //引用存在的XMl文件
        LayoutScatter scatter = LayoutScatter.getInstance(context);
        root = (ComponentContainer) scatter.parse(ResourceTable.Layout_my_custom_view, null, false);

    }
    private void initComponent(){
        presenter = createPresenter();
        presenter.attachView(this);

        aView = (Text) root.findComponentById(ResourceTable.Id_textViewA);
        bView = (Text) root.findComponentById(ResourceTable.Id_textViewB);
        loadA = (Button) root.findComponentById(ResourceTable.Id_loadA);
        loadB = (Button) root.findComponentById(ResourceTable.Id_loadB);

        loadA.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                presenter.doA();
            }
        });

        loadB.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                presenter.doB();
                ;
            }
        });
    }

    @Override
    public Component getComponent() {
        if(root == null){
            init(myContext);
        }
        return root;
    }

    @Override
    public void instantiateComponent() {
        initComponent();
    }

//    @Override
    public MyCustomPresenter createPresenter() {
        return new MyCustomPresenter();
    }
    @Override
    public void showA(A a) {
//        viewState.setShowingA(true);
//        viewState.setData(a);
        aView.setText(a.getName());
        aView.setVisibility(Component.VISIBLE);
        bView.setVisibility(Component.HIDE);
    }

    @Override
    public void showB(B b) {
//        viewState.setShowingA(false);
//        viewState.setData(b);
        bView.setText(b.getFoo());
        aView.setVisibility(Component.HIDE);
        bView.setVisibility(Component.VISIBLE);
    }
}
