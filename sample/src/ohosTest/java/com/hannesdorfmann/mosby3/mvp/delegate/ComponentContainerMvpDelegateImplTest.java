package com.hannesdorfmann.mosby3.mvp.delegate;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.layout.MvpStackLayout;
import com.hannesdorfmann.mosby3.sample.EventHelper;
import com.hannesdorfmann.mosby3.sample.MainAbility;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.utils.Sequenceable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ComponentContainerMvpDelegateImplTest {

    ComponentContainerMvpDelegateImpl componentContainerMvpDelegate;
    ComponentContainerDelegateCallback componentContainerDelegateCallback;
    MvpPresenter mvpPresenter;
    MvpView mvpView;

    boolean attachFlag, detachFlag;

    @Before
    public void setUp() throws Exception {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        MainAbility ability = EventHelper.startAbility(MainAbility.class);
        mvpView = new MvpView() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }
        };
        mvpPresenter = new MvpPresenter(){
            @Override
            public void attachView(MvpView view) {
                attachFlag = true;
            }

            @Override
            public void detachView(boolean retainInstance) {

            }

            @Override
            public void detachView() {
                detachFlag = true;
            }

            @Override
            public void destroy() {

            }
        };
        MvpStackLayout mvpStackLayout = new MvpStackLayout(context) {
            @Override
            protected void onAttachedToWindowDo(Component component) {

            }

            @Override
            protected void onDetachedFromWindowDo(Component component) {

            }

            @Override
            public MvpPresenter createPresenter() {
                return mvpPresenter;
            }

            @Override
            public Sequenceable superOnSaveInstanceState() {
                return null;
            }
        };
        mvpStackLayout.setmComponetContext(ability);
        componentContainerDelegateCallback = new ComponentContainerDelegateCallback() {
            @Override
            public Sequenceable superOnSaveInstanceState() {
                return null;
            }

            @Override
            public void superOnRestoreInstanceState(Sequenceable state) {

            }

            @Override
            public Context getmComponetContext() {
                return ability;
            }

            @Override
            public void setmComponetContext(Context context) {

            }

            @Override
            public Context getContext() {
                return context;
            }

            @Override
            public MvpPresenter createPresenter() {
                return mvpPresenter;
            }

            @Override
            public MvpPresenter getPresenter() {
                return mvpPresenter;
            }

            @Override
            public void setPresenter(MvpPresenter presenter) {

            }

            @Override
            public MvpView getMvpView() {
                return mvpView;
            }
        };
        componentContainerMvpDelegate = new ComponentContainerMvpDelegateImpl(mvpStackLayout, componentContainerDelegateCallback, true);

    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }

    @Test
    public void onAttachedToWindow() {
        attachFlag = false;
        componentContainerMvpDelegate.onAttachedToWindow();
        assertTrue(attachFlag);
    }

    @Test
    public void onSaveInstanceState() {
        assertNull(componentContainerMvpDelegate.onSaveInstanceState());
    }

    @Test
    public void onDetachedFromWindow() {
        detachFlag = false;
        componentContainerMvpDelegate.onDetachedFromWindow();
        assertTrue(detachFlag);
    }
}