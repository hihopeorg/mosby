package com.hannesdorfmann.mosby3.mvp.lce;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesErrorMessage;
import com.hannesdorfmann.mosby3.sample.mvp.lce.SimpleCountriesPresenter;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MvpLceFractionTest {

    MvpLceFraction mvpLceFraction;
    Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    boolean isClicked;
    LayoutScatter scatter;

    @Before
    public void setUp() throws Exception {
        scatter = LayoutScatter.getInstance(context);
        mvpLceFraction = new MvpLceFraction(){

            @Override
            public void setData(Object data) {

            }

            @Override
            public void loadData(boolean pullToRefresh) {
                isClicked = true;
            }

            @Override
            public MvpPresenter createPresenter() {
                return new SimpleCountriesPresenter();
            }

            @Override
            protected int getUIContent() {
                return ResourceTable.Layout_countries_list;
            }

            @Override
            protected void initLceComponetID() {
                loadingViewID = ResourceTable.Id_loadingView;
                contentViewID = ResourceTable.Id_contentView;
                errorViewID = ResourceTable.Id_errorView;
            }

            @Override
            protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
                return CountriesErrorMessage.get(e, pullToRefresh, this);
            }
        };
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void initLceComponetID() {
        mvpLceFraction.initLceComponetID();
        assertTrue(mvpLceFraction.loadingViewID >0);
        assertTrue(mvpLceFraction.contentViewID >0);
        assertTrue(mvpLceFraction.errorViewID >0);
    }

    @Test
    public void initCompoment() {
        mvpLceFraction.mComponent = scatter.parse(mvpLceFraction.getUIContent(), null, false);
        mvpLceFraction.initCompoment();
        assertNotNull(mvpLceFraction.loadingView);
        assertNotNull(mvpLceFraction.contentView);
        assertNotNull(mvpLceFraction.errorView);
    }

    @Test
    public void createLoadingView() {
        mvpLceFraction.mComponent = scatter.parse(mvpLceFraction.getUIContent(), null, false);
        mvpLceFraction.initLceComponetID();

        mvpLceFraction.loadingView = mvpLceFraction.createLoadingView(mvpLceFraction.mComponent);
        assertNotNull(mvpLceFraction.loadingView);
    }

    @Test
    public void createContentView() {
        mvpLceFraction.mComponent = scatter.parse(mvpLceFraction.getUIContent(), null, false);
        mvpLceFraction.initLceComponetID();

        mvpLceFraction.contentView = mvpLceFraction.createContentView(mvpLceFraction.mComponent);
        assertNotNull(mvpLceFraction.contentView);
    }

    @Test
    public void createErrorView() {
        mvpLceFraction.mComponent = scatter.parse(mvpLceFraction.getUIContent(), null, false);
        mvpLceFraction.initLceComponetID();

        mvpLceFraction.errorView = mvpLceFraction.createErrorView(mvpLceFraction.mComponent);
        assertNotNull(mvpLceFraction.errorView);
    }

    @Test
    public void showLoading() {
        mvpLceFraction.mComponent = scatter.parse(mvpLceFraction.getUIContent(), null, false);
        mvpLceFraction.initLceComponetID();
        mvpLceFraction.initCompoment();

        mvpLceFraction.showLoading(false);
        assertTrue(mvpLceFraction.loadingView.getVisibility() == Component.VISIBLE);
        assertTrue(mvpLceFraction.errorView.getVisibility() != Component.VISIBLE);

    }

    @Test
    public void animateLoadingViewIn() {
        mvpLceFraction.mComponent = scatter.parse(mvpLceFraction.getUIContent(), null, false);
        mvpLceFraction.initLceComponetID();
        mvpLceFraction.initCompoment();

        mvpLceFraction.animateLoadingViewIn();
        assertTrue(mvpLceFraction.loadingView.getVisibility() == Component.VISIBLE);
        assertTrue(mvpLceFraction.errorView.getVisibility() != Component.VISIBLE);
    }

    @Test
    public void showContent() {
        mvpLceFraction.mComponent = scatter.parse(mvpLceFraction.getUIContent(), null, false);
        mvpLceFraction.initLceComponetID();
        mvpLceFraction.initCompoment();

        mvpLceFraction.showContent();
        assertTrue(mvpLceFraction.errorView.getVisibility() != Component.VISIBLE);
        assertTrue(mvpLceFraction.contentView.getVisibility() == Component.VISIBLE);
    }

    @Test
    public void animateContentViewIn() {
        mvpLceFraction.mComponent = scatter.parse(mvpLceFraction.getUIContent(), null, false);
        mvpLceFraction.initLceComponetID();
        mvpLceFraction.initCompoment();

        mvpLceFraction.animateLoadingViewIn();
        assertTrue(mvpLceFraction.loadingView.getVisibility() == Component.VISIBLE);
        assertTrue(mvpLceFraction.errorView.getVisibility() != Component.VISIBLE);
    }

    @Test
    public void getErrorMessage() {
        assertNotNull(mvpLceFraction.getErrorMessage(new Throwable("test active err"), false));
    }

    @Test
    public void onErrorViewClicked() {
        mvpLceFraction.mComponent = scatter.parse(mvpLceFraction.getUIContent(), null, false);
        mvpLceFraction.initLceComponetID();
        mvpLceFraction.initCompoment();

        isClicked = false;
        mvpLceFraction.onErrorViewClicked();
        assertTrue(isClicked);
    }

    @Test
    public void showError() {
        mvpLceFraction.mComponent = scatter.parse(mvpLceFraction.getUIContent(), null, false);
        mvpLceFraction.initLceComponetID();
        mvpLceFraction.initCompoment();

        mvpLceFraction.showError(new Throwable("test showError"), false);
        assertNotNull(mvpLceFraction.errorView.getText().toString());
    }

    @Test
    public void animateErrorViewIn() {
        mvpLceFraction.mComponent = scatter.parse(mvpLceFraction.getUIContent(), null, false);
        mvpLceFraction.initLceComponetID();
        mvpLceFraction.initCompoment();

        mvpLceFraction.animateErrorViewIn();
        assertTrue(mvpLceFraction.errorView.getVisibility() == Component.VISIBLE);
    }

}