package com.hannesdorfmann.mosby3.mvp.lce;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesErrorMessage;
import com.hannesdorfmann.mosby3.sample.mvp.lce.SimpleCountriesPresenter;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MvpLceAbilityTest {

    MvpLceAbility mvpLceAbility;
    Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    boolean isClicked;

    @Before
    public void setUp() throws Exception {
        mvpLceAbility = new MvpLceAbility(){
            @Override
            public void setData(Object data) {

            }

            @Override
            public void loadData(boolean pullToRefresh) {
                isClicked = true;
            }

            @Override
            public MvpPresenter createPresenter() {
                return new SimpleCountriesPresenter();
            }

            @Override
            protected ComponentContainer getUIContent() {
                LayoutScatter mLayoutScatter = LayoutScatter.getInstance(context);
                return (ComponentContainer) mLayoutScatter.parse(ResourceTable.Layout_countries_list, null, false);
            }

            @Override
            protected void initLceComponetID() {
                loadingViewID = ResourceTable.Id_loadingView;
                contentViewID = ResourceTable.Id_contentView;
                errorViewID = ResourceTable.Id_errorView;
            }

            @Override
            protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
                return CountriesErrorMessage.get(e, pullToRefresh, this);
            }
        };
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void getUIContent() {
        assertNotNull(mvpLceAbility.getUIContent());
    }

    @Test
    public void initLceComponetID() {
        mvpLceAbility.initLceComponetID();
        assertTrue(mvpLceAbility.loadingViewID >0);
        assertTrue(mvpLceAbility.contentViewID >0);
        assertTrue(mvpLceAbility.errorViewID >0);
    }

    @Test
    public void initCompoment() {
        mvpLceAbility.mComponentContainer = mvpLceAbility.getUIContent();
        mvpLceAbility.initLceComponetID();

        mvpLceAbility.initCompoment();
        assertNotNull(mvpLceAbility.loadingView);
        assertNotNull(mvpLceAbility.contentView);
        assertNotNull(mvpLceAbility.errorView);
    }

    @Test
    public void onStoreDataWhenConfigChange() {
    }

    @Test
    public void createLoadingView() {
        mvpLceAbility.mComponentContainer = mvpLceAbility.getUIContent();
        mvpLceAbility.initLceComponetID();

        mvpLceAbility.loadingView = mvpLceAbility.createLoadingView();
        assertNotNull(mvpLceAbility.loadingView);
    }

    @Test
    public void createContentView() {
        mvpLceAbility.mComponentContainer = mvpLceAbility.getUIContent();
        mvpLceAbility.initLceComponetID();

        mvpLceAbility.contentView = mvpLceAbility.createContentView();
        assertNotNull(mvpLceAbility.contentView);
    }

    @Test
    public void createErrorView() {
        mvpLceAbility.mComponentContainer = mvpLceAbility.getUIContent();
        mvpLceAbility.initLceComponetID();

        mvpLceAbility.errorView = mvpLceAbility.createErrorView();
        assertNotNull(mvpLceAbility.errorView);
    }

    @Test
    public void onErrorViewClicked() {
        mvpLceAbility.mComponentContainer = mvpLceAbility.getUIContent();
        mvpLceAbility.initLceComponetID();
        mvpLceAbility.initCompoment();

        isClicked = false;
        mvpLceAbility.onErrorViewClicked();
        assertTrue(isClicked);
    }

    @Test
    public void showLoading() {
        mvpLceAbility.mComponentContainer = mvpLceAbility.getUIContent();
        mvpLceAbility.initLceComponetID();
        mvpLceAbility.initCompoment();

        mvpLceAbility.showLoading(false);
        assertTrue(mvpLceAbility.loadingView.getVisibility() == Component.VISIBLE);
        assertTrue(mvpLceAbility.errorView.getVisibility() != Component.VISIBLE);
    }

    @Test
    public void animateLoadingViewIn() {
        mvpLceAbility.mComponentContainer = mvpLceAbility.getUIContent();
        mvpLceAbility.initLceComponetID();
        mvpLceAbility.initCompoment();

        mvpLceAbility.animateLoadingViewIn();
        assertTrue(mvpLceAbility.loadingView.getVisibility() == Component.VISIBLE);
        assertTrue(mvpLceAbility.errorView.getVisibility() != Component.VISIBLE);
    }

    @Test
    public void showContent() {
        mvpLceAbility.mComponentContainer = mvpLceAbility.getUIContent();
        mvpLceAbility.initLceComponetID();
        mvpLceAbility.initCompoment();

        mvpLceAbility.showContent();
        assertTrue(mvpLceAbility.errorView.getVisibility() != Component.VISIBLE);
        assertTrue(mvpLceAbility.contentView.getVisibility() == Component.VISIBLE);
    }

    @Test
    public void animateContentViewIn() {
        mvpLceAbility.mComponentContainer = mvpLceAbility.getUIContent();
        mvpLceAbility.initLceComponetID();
        mvpLceAbility.initCompoment();

        mvpLceAbility.animateContentViewIn();
        assertTrue(mvpLceAbility.errorView.getVisibility() != Component.VISIBLE);
        assertTrue(mvpLceAbility.contentView.getVisibility() == Component.VISIBLE);
    }

    @Test
    public void getErrorMessage() {
        assertNotNull(mvpLceAbility.getErrorMessage(new Throwable("test active err"), false));
    }

    @Test
    public void showError() {
        mvpLceAbility.mComponentContainer = mvpLceAbility.getUIContent();
        mvpLceAbility.initLceComponetID();
        mvpLceAbility.initCompoment();

        mvpLceAbility.showError(new Throwable("test showError"), false);
        assertNotNull(mvpLceAbility.errorView.getText().toString());
    }

    @Test
    public void animateErrorViewIn() {
        mvpLceAbility.mComponentContainer = mvpLceAbility.getUIContent();
        mvpLceAbility.initLceComponetID();
        mvpLceAbility.initCompoment();

        mvpLceAbility.animateErrorViewIn();
        assertTrue(mvpLceAbility.errorView.getVisibility() == Component.VISIBLE);
    }
}