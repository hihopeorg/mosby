package com.hannesdorfmann.mosby3.mvp.lce;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.LceViewState;
import com.hannesdorfmann.mosby3.mvp.viewstate.lce.MvpLceViewStateFraction;
import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesErrorMessage;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MvpLceViewStateFractionTest {

    MvpLceViewStateFraction mvpLceViewStateFraction;
    Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    LceViewState viewState;
    LayoutScatter scatter;
    String content  = null;
    String errMsg = null;
    boolean loadDataRefreshFlag;
    boolean setStateShowLoadingFlag;

    @Before
    public void setUp() throws Exception {
        scatter = LayoutScatter.getInstance(context);
        viewState = new LceViewState() {
            @Override
            public void setStateShowContent(Object loadedData) {
                content = (String) loadedData;
            }

            @Override
            public void setStateShowError(Throwable e, boolean pullToRefresh) {
                errMsg = e.getMessage();
            }

            @Override
            public void setStateShowLoading(boolean pullToRefresh) {
                setStateShowLoadingFlag = pullToRefresh;
            }

            @Override
            public boolean isLoadingState() {
                return true;
            }

            @Override
            public boolean isPullToRefreshLoadingState() {
                return true;
            }

            @Override
            public void apply(MvpView view, boolean retained) {

            }
        };
        mvpLceViewStateFraction = new MvpLceViewStateFraction() {
            @Override
            public Object getData() {
                return "value";
            }

            @Override
            public void setViewState(ViewState viewState) {
            }

            @Override
            public ViewState createViewState() {
                return viewState;
            }

            @Override
            protected void initLceComponetID() {
                loadingViewID = ResourceTable.Id_loadingView;
                contentViewID = ResourceTable.Id_contentView;
                errorViewID = ResourceTable.Id_errorView;
            }

            @Override
            protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
                return CountriesErrorMessage.get(e/*new Throwable("active test err2")*/, pullToRefresh, context);
            }

            @Override
            public MvpPresenter createPresenter() {
                return null;
            }

            @Override
            protected int getUIContent() {
                return ResourceTable.Layout_countries_list;
            }

            @Override
            public void setData(Object data) {

            }

            @Override
            public void loadData(boolean pullToRefresh) {
                loadDataRefreshFlag = pullToRefresh;
            }
        };
//        mvpLceViewStateFraction.onComponentAttached(LayoutScatter.getInstance(context), null, null);
        mvpLceViewStateFraction.setViewState(viewState);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void showContent() {
        content = null;
        mvpLceViewStateFraction.mComponent = scatter.parse(mvpLceViewStateFraction.getUIContent(), null, false);
        mvpLceViewStateFraction.initCompoment();
        mvpLceViewStateFraction.showContent();
        assertEquals(content, "value");
    }

    @Test
    public void showError() {
        errMsg = null;
        mvpLceViewStateFraction.mComponent = scatter.parse(mvpLceViewStateFraction.getUIContent(), null, false);
        mvpLceViewStateFraction.initCompoment();
        String err = "test active err";
        mvpLceViewStateFraction.showError(new Throwable(err), false);
        assertEquals(errMsg, err);
    }

    @Test
    public void showLoading() {
        mvpLceViewStateFraction.mComponent = scatter.parse(mvpLceViewStateFraction.getUIContent(), null, false);
        mvpLceViewStateFraction.initCompoment();
        mvpLceViewStateFraction.showLoading(false);
        assertFalse(setStateShowLoadingFlag);
    }

    @Test
    public void onViewStateInstanceRestored() {
        loadDataRefreshFlag = false;
        mvpLceViewStateFraction.mComponent = scatter.parse(mvpLceViewStateFraction.getUIContent(), null, false);
        mvpLceViewStateFraction.initCompoment();
        mvpLceViewStateFraction.onViewStateInstanceRestored(false);
        assertTrue(loadDataRefreshFlag);
    }

    @Test
    public void onNewViewStateInstance() {
        loadDataRefreshFlag = true;
        mvpLceViewStateFraction.mComponent = scatter.parse(mvpLceViewStateFraction.getUIContent(), null, false);
        mvpLceViewStateFraction.initCompoment();
        mvpLceViewStateFraction.onNewViewStateInstance();
        assertFalse(loadDataRefreshFlag);

    }

}