package com.hannesdorfmann.mosby3.mvp.delegate;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.sample.EventHelper;
import com.hannesdorfmann.mosby3.sample.MainAbility;
import com.hannesdorfmann.mosby3.sample.TestUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AbilitySliceMvpDelegateImplTest {

    AbilitySliceMvpDelegateImpl mvpDelegate;
    MvpDelegateCallback mvpDelegateCallback;
    Ability ability;
    AbilitySlice slice;
    MvpPresenter mvpPresenter;
    MvpView mvpView;
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    boolean detachFlag, stopFlag;

    @Before
    public void setUp() throws Exception {
        ability = EventHelper.startAbility(MainAbility.class);
        TestUtils.sleep(2000);
        slice = sAbilityDelegator.getCurrentAbilitySlice(ability);
        mvpPresenter = new MvpPresenter() {
            @Override
            public void attachView(MvpView view) {

            }

            @Override
            public void detachView(boolean retainInstance) {

            }

            @Override
            public void detachView() {
                detachFlag = true;
            }

            @Override
            public void destroy() {
                stopFlag = true;
            }
        };
        mvpView = new MvpView() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }
        };

        mvpDelegateCallback = new MvpDelegateCallback() {
            @Override
            public MvpPresenter createPresenter() {
                return mvpPresenter;
            }

            @Override
            public MvpPresenter getPresenter() {
                return mvpPresenter;
            }

            @Override
            public void setPresenter(MvpPresenter presenter) {

            }

            @Override
            public MvpView getMvpView() {
                return mvpView;
            }
        };
        mvpDelegate = new AbilitySliceMvpDelegateImpl(slice, mvpDelegateCallback, false, false);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }

    @Test
    public void onStart() {
        mvpDelegate.onStart(null);
        assertNotNull(mvpDelegate.getPresenter());
    }

    @Test
    public void onBackground() {
        detachFlag = false;
        mvpDelegate.onBackground();
        assertTrue(detachFlag);
    }

    @Test
    public void onStop() {
        stopFlag = false;
        mvpDelegate.onStop();
        assertTrue(stopFlag);
    }
}