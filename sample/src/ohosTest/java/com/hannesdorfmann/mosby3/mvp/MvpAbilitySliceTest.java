package com.hannesdorfmann.mosby3.mvp;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MvpAbilitySliceTest {

    MvpAbilitySlice mvpAbilitySlice;

    @Before
    public void setUp() throws Exception {
        mvpAbilitySlice = new MvpAbilitySlice() {
            @Override
            public MvpPresenter createPresenter() {
                return null;
            }
        };
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getMvpDelegate() {
        assertNotNull(mvpAbilitySlice.getMvpDelegate());
    }

    @Test
    public void getMvpView() {
        assertNotNull(mvpAbilitySlice.getMvpView());
    }

}