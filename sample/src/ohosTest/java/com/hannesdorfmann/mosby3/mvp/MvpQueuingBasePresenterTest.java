package com.hannesdorfmann.mosby3.mvp;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import static org.junit.Assert.*;

public class MvpQueuingBasePresenterTest {

    MvpQueuingBasePresenter mvpQueuingBasePresenter;
    MvpView mvpView;

    @Before
    public void setUp() throws Exception {
        mvpQueuingBasePresenter = new MvpQueuingBasePresenter();
        mvpView = new MvpView() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }
        };
    }

    @Test
    public void onceViewAttached() {
        mvpQueuingBasePresenter.attachView(mvpView);
        mvpQueuingBasePresenter.onceViewAttached(new MvpQueuingBasePresenter.ViewAction<MvpView>() {
            @Override
            public void run(MvpView view) {
                assertNotNull(view);
            }
        });

    }

    @Test
    public void attachView() {
        mvpQueuingBasePresenter.attachView(mvpView);
        assertNotNull(mvpQueuingBasePresenter.getViewRef());

    }

    @Test
    public void detachView() {
        mvpQueuingBasePresenter.attachView(mvpView);
        mvpQueuingBasePresenter.detachView();
        assertNull(mvpQueuingBasePresenter.getViewRef().get());

    }

    @Test
    public void destroy() {
        mvpQueuingBasePresenter.destroy();
        assertTrue(mvpQueuingBasePresenter.getViewActionQueue().isEmpty());

    }

}