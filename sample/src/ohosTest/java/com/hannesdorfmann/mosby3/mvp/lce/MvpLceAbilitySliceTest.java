package com.hannesdorfmann.mosby3.mvp.lce;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesErrorMessage;
import com.hannesdorfmann.mosby3.sample.mvp.lce.SimpleCountriesPresenter;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MvpLceAbilitySliceTest {

    MvpLceAbilitySlice mvpLceAbilitySlice;
    Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    boolean isClicked;

    @Before
    public void setUp() throws Exception {
        mvpLceAbilitySlice = new MvpLceAbilitySlice() {
            @Override
            protected ComponentContainer getUIContent() {
                LayoutScatter mLayoutScatter = LayoutScatter.getInstance(context);
                return (ComponentContainer) mLayoutScatter.parse(ResourceTable.Layout_countries_list, null, false);
            }

            @Override
            protected void initLceComponetID() {
                loadingViewID = ResourceTable.Id_loadingView;
                contentViewID = ResourceTable.Id_contentView;
                errorViewID = ResourceTable.Id_errorView;
            }

            @Override
            protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
                return CountriesErrorMessage.get(e, pullToRefresh, this);
            }

            @Override
            public MvpPresenter createPresenter() {
                return new SimpleCountriesPresenter();
            }

            @Override
            public void setData(Object data) {

            }

            @Override
            public void loadData(boolean pullToRefresh) {
                isClicked = true;
            }
        };
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getUIContent() {
        assertNotNull(mvpLceAbilitySlice.getUIContent());
    }

    @Test
    public void initLceComponetID() {
        mvpLceAbilitySlice.initLceComponetID();
        assertTrue(mvpLceAbilitySlice.loadingViewID >0);
        assertTrue(mvpLceAbilitySlice.contentViewID >0);
        assertTrue(mvpLceAbilitySlice.errorViewID >0);
    }

    @Test
    public void initCompoment() {
        mvpLceAbilitySlice.mComponentContainer = mvpLceAbilitySlice.getUIContent();
        mvpLceAbilitySlice.initLceComponetID();

        mvpLceAbilitySlice.initCompoment();
        assertNotNull(mvpLceAbilitySlice.loadingView);
        assertNotNull(mvpLceAbilitySlice.contentView);
        assertNotNull(mvpLceAbilitySlice.errorView);
    }

    @Test
    public void createLoadingView() {
        mvpLceAbilitySlice.mComponentContainer = mvpLceAbilitySlice.getUIContent();
        mvpLceAbilitySlice.initLceComponetID();

        mvpLceAbilitySlice.loadingView = mvpLceAbilitySlice.createLoadingView();
        assertNotNull(mvpLceAbilitySlice.loadingView);
    }

    @Test
    public void createContentView() {
        mvpLceAbilitySlice.mComponentContainer = mvpLceAbilitySlice.getUIContent();
        mvpLceAbilitySlice.initLceComponetID();

        mvpLceAbilitySlice.contentView = mvpLceAbilitySlice.createContentView();
        assertNotNull(mvpLceAbilitySlice.contentView);
    }

    @Test
    public void createErrorView() {
        mvpLceAbilitySlice.mComponentContainer = mvpLceAbilitySlice.getUIContent();
        mvpLceAbilitySlice.initLceComponetID();

        mvpLceAbilitySlice.errorView = mvpLceAbilitySlice.createErrorView();
        assertNotNull(mvpLceAbilitySlice.errorView);
    }

    @Test
    public void showLoading() {
        mvpLceAbilitySlice.mComponentContainer = mvpLceAbilitySlice.getUIContent();
        mvpLceAbilitySlice.initLceComponetID();
        mvpLceAbilitySlice.initCompoment();

        mvpLceAbilitySlice.showLoading(false);
        assertTrue(mvpLceAbilitySlice.loadingView.getVisibility() == Component.VISIBLE);
        assertTrue(mvpLceAbilitySlice.errorView.getVisibility() != Component.VISIBLE);
    }

    @Test
    public void animateLoadingViewIn() {
        mvpLceAbilitySlice.mComponentContainer = mvpLceAbilitySlice.getUIContent();
        mvpLceAbilitySlice.initLceComponetID();
        mvpLceAbilitySlice.initCompoment();

        mvpLceAbilitySlice.animateLoadingViewIn();
        assertTrue(mvpLceAbilitySlice.loadingView.getVisibility() == Component.VISIBLE);
        assertTrue(mvpLceAbilitySlice.errorView.getVisibility() != Component.VISIBLE);
    }

    @Test
    public void showContent() {
        mvpLceAbilitySlice.mComponentContainer = mvpLceAbilitySlice.getUIContent();
        mvpLceAbilitySlice.initLceComponetID();
        mvpLceAbilitySlice.initCompoment();

        mvpLceAbilitySlice.showContent();

        assertTrue(mvpLceAbilitySlice.errorView.getVisibility() != Component.VISIBLE);
        assertTrue(mvpLceAbilitySlice.contentView.getVisibility() == Component.VISIBLE);
    }

    @Test
    public void animateContentViewIn() {
        mvpLceAbilitySlice.mComponentContainer = mvpLceAbilitySlice.getUIContent();
        mvpLceAbilitySlice.initLceComponetID();
        mvpLceAbilitySlice.initCompoment();

        mvpLceAbilitySlice.animateContentViewIn();
        assertTrue(mvpLceAbilitySlice.errorView.getVisibility() != Component.VISIBLE);
        assertTrue(mvpLceAbilitySlice.contentView.getVisibility() == Component.VISIBLE);
    }

    @Test
    public void getErrorMessage() {
        assertNotNull(mvpLceAbilitySlice.getErrorMessage(new Throwable("test active err"), false));
    }

    @Test
    public void onErrorViewClicked() {
        mvpLceAbilitySlice.mComponentContainer = mvpLceAbilitySlice.getUIContent();
        mvpLceAbilitySlice.initLceComponetID();
        mvpLceAbilitySlice.initCompoment();

        isClicked = false;
        mvpLceAbilitySlice.onErrorViewClicked();
        assertTrue(isClicked);
    }

    @Test
    public void showError() {
        mvpLceAbilitySlice.mComponentContainer = mvpLceAbilitySlice.getUIContent();
        mvpLceAbilitySlice.initLceComponetID();
        mvpLceAbilitySlice.initCompoment();

        mvpLceAbilitySlice.showError(new Throwable("test showError"), false);
        assertNotNull(mvpLceAbilitySlice.errorView.getText().toString());
    }

    @Test
    public void animateErrorViewIn() {
        mvpLceAbilitySlice.mComponentContainer = mvpLceAbilitySlice.getUIContent();
        mvpLceAbilitySlice.initLceComponetID();
        mvpLceAbilitySlice.initCompoment();

        mvpLceAbilitySlice.animateErrorViewIn();
        assertTrue(mvpLceAbilitySlice.errorView.getVisibility() == Component.VISIBLE);
    }

}