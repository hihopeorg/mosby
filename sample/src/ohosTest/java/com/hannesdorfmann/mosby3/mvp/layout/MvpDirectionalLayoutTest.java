package com.hannesdorfmann.mosby3.mvp.layout;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.mvp.delegate.AbilityMvpDelegateImpl;
import com.hannesdorfmann.mosby3.mvp.delegate.MvpDelegateCallback;
import com.hannesdorfmann.mosby3.sample.EventHelper;
import com.hannesdorfmann.mosby3.sample.MainAbility;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.utils.Sequenceable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class MvpDirectionalLayoutTest {

    MvpDirectionalLayout mvpStackLayout;
    AbilityMvpDelegateImpl mvpDelegate;
    MvpDelegateCallback mvpDelegateCallback;
    Ability ability;

    @Before
    public void setUp() throws Exception {
        ability = EventHelper.startAbility(MainAbility.class);
        mvpDelegateCallback = new MvpDelegateCallback() {
            @Override
            public MvpPresenter createPresenter() {
                return null;
            }

            @Override
            public MvpPresenter getPresenter() {
                return null;
            }

            @Override
            public void setPresenter(MvpPresenter presenter) {

            }

            @Override
            public MvpView getMvpView() {
                return null;
            }
        };

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        mvpStackLayout = new MvpDirectionalLayout(ability) {

            @Override
            public MvpPresenter createPresenter() {
                return null;
            }

            @Override
            public Sequenceable superOnSaveInstanceState() {
                return null;
            }

            @Override
            public void superOnRestoreInstanceState(Sequenceable state) {

            }

            @Override
            public Context getmComponetContext() {
                return ability;
            }

            @Override
            public void setmComponetContext(Context context) {

            }
        };
        mvpStackLayout.setmComponetContext(ability);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }

    @Test
    public void getMvpDelegate() {
        assertNotNull(mvpStackLayout.getMvpDelegate());
    }

    @Test
    public void getMvpView() {
        assertNotNull(mvpStackLayout.getMvpView());
    }
}