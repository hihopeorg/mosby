package com.hannesdorfmann.mosby3.mvp;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class MvpAbilityTest {

    MvpAbility mvpAbility;

    @Before
    public void setUp() throws Exception {
        mvpAbility = new MvpAbility() {
            @Override
            public MvpPresenter createPresenter() {
                return null;
            }
        };
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getMvpDelegate() {
        assertNotNull(mvpAbility.getMvpDelegate());
    }

    @Test
    public void getMvpView() {
        assertNotNull(mvpAbility.getMvpView());
    }
}