package com.hannesdorfmann.mosby3.mvp;

import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvp.lce.SimpleCountriesPresenter;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MvpFractionTest {

    MvpFraction mvpFraction;
    Context context;

    @Before
    public void setUp() throws Exception {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        mvpFraction = new MvpFraction() {
            @Override
            public MvpPresenter createPresenter() {
                return new SimpleCountriesPresenter();
            }

            @Override
            protected int getUIContent() {
                return ResourceTable.Layout_countries_list;
            }

            @Override
            protected void initCompoment() {

            }
        };
    }

    @Test
    public void getMvpDelegate() {
        assertNotNull(mvpFraction.getMvpDelegate());
    }

    @Test
    public void getMvpView() {
        assertNotNull(mvpFraction.getMvpView());
    }

}