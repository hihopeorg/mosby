package com.hannesdorfmann.mosby3.mvp.lce;

import com.hannesdorfmann.mosby3.mvp.widget.CirleProgressImg;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LceAnimatorTest {
    public CirleProgressImg loadingView;
    Text contentView;
    Text errorView;

    @Before
    public void setUp() throws Exception {

        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        loadingView = new CirleProgressImg(context);

        contentView = new Text(context);
        contentView.setText("contentView");
        contentView.setTextSize(AttrHelper.fp2px(15, context));
        contentView.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT));


        errorView = new Text(context);
        errorView.setText("errorView");
        errorView.setTextSize(AttrHelper.fp2px(15, context));
        errorView.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT));
    }

    @After
    public void tearDown() throws Exception {
        loadingView = null;
        contentView = null;
        errorView = null;
    }

    @Test
    public void testShowLoading() {
        LceAnimator.showLoading(loadingView, contentView, errorView);
        assertTrue(loadingView.getVisibility() == Component.VISIBLE);
        assertTrue(contentView.getVisibility() == Component.HIDE);
    }

    @Test
    public void testShowErrorView() {
        LceAnimator.showErrorView(loadingView, contentView, errorView);
        assertTrue(contentView.getVisibility() == Component.HIDE);
        assertTrue(errorView.getVisibility() == Component.VISIBLE);
    }

    @Test
    public void testShowContent() {
        LceAnimator.showContent(loadingView, contentView, errorView);
        assertTrue(contentView.getVisibility() == Component.VISIBLE);
        assertTrue(errorView.getVisibility() == Component.HIDE);
    }
}