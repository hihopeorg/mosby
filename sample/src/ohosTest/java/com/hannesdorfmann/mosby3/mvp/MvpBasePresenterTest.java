package com.hannesdorfmann.mosby3.mvp;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;
import static org.junit.Assert.*;

public class MvpBasePresenterTest {

    MvpBasePresenter mvpBasePresenter;
    MvpView mvpView;
    Random random = new Random();
    boolean detachFlag;

    @Before
    public void setUp() throws Exception {
        mvpBasePresenter = new MvpBasePresenter(){
            @Override
            public void detachView(boolean retainInstance) {
                super.detachView(retainInstance);
                detachFlag = retainInstance;
            }
        };
        mvpView = new MvpView() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }
        };
    }

    @Test
    public void attachView() {
        mvpBasePresenter.attachView(mvpView);
        assertNotNull(mvpBasePresenter.getView());
    }

    @Test
    public void getView() {
        assertNull(mvpBasePresenter.getView());
    }

    @Test
    public void isViewAttached() {
        mvpBasePresenter.attachView(mvpView);
        assertTrue(mvpBasePresenter.isViewAttached());
    }

    @Test
    public void detachView() {
        mvpBasePresenter.attachView(mvpView);
        mvpBasePresenter.detachView();
        assertNull(mvpBasePresenter.getView());
    }

    @Test
    public void destroy() {
        detachFlag = true;
        mvpBasePresenter.destroy();
        assertFalse(detachFlag);
    }
}