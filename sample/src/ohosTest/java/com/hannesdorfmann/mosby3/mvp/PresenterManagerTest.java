package com.hannesdorfmann.mosby3.mvp;

import com.hannesdorfmann.mosby3.PresenterManager;
import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;
import com.hannesdorfmann.mosby3.sample.EventHelper;
import com.hannesdorfmann.mosby3.sample.MainAbility;
import com.hannesdorfmann.mosby3.sample.TestUtils;
import com.hannesdorfmann.mosby3.sample.mvp.customviewstate.MyCustomAbilitySlice;
import ohos.aafwk.ability.Ability;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PresenterManagerTest {

    Ability activity;
    String viewId;
    MvpPresenter<? extends MvpView> presenter;
    ViewState viewState;

    @Before
    public void setUp() throws Exception {
        activity = new Ability();
//        activity = EventHelper.startAbility(MainAbility.class);
        viewId = "1234";
        presenter = new MvpPresenter<MvpView>() {
            @Override
            public void attachView(MvpView view) {

            }

            @Override
            public void detachView(boolean retainInstance) {

            }

            @Override
            public void detachView() {

            }

            @Override
            public void destroy() {

            }
        };
        viewState = new ViewState() {
            @Override
            public void apply(MvpView view, boolean retained) {

            }
        };
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        TestUtils.sleep(2000);
    }

    @Test
    public void getActivity() {

        Ability ability = PresenterManager.getActivity(activity);
        assertEquals(ability, activity);

        TestUtils.sleep(2000);
        MyCustomAbilitySlice abilitySlice = EventHelper.startAbilitySlice(MainAbility.class, MyCustomAbilitySlice.class);
        Ability mSlice = PresenterManager.getActivity(abilitySlice);
        assertNotNull(mSlice);
        abilitySlice.getAbility().terminateAbility();
    }

    @Test
    public void putPresenter() {
        MainAbility activity = EventHelper.startAbility(MainAbility.class);
        PresenterManager.putPresenter(activity, viewId, presenter);
        MvpPresenter mvpPresenter = PresenterManager.getPresenter(activity, viewId);
        assertEquals(mvpPresenter, presenter);
    }

    @Test
    public void putViewState() {
        MainAbility ability = EventHelper.startAbility(MainAbility.class);
        PresenterManager.putViewState(ability, viewId, viewState);
        ViewState mViewState = PresenterManager.getViewState(ability, viewId);
        assertEquals(mViewState, viewState);
    }

}