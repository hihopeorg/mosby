package com.hannesdorfmann.mosby3.mvp.delegate;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.sample.EventHelper;
import com.hannesdorfmann.mosby3.sample.MainAbility;
import ohos.aafwk.ability.Ability;
import ohos.utils.PacMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AbilityMvpDelegateImplTest {

    AbilityMvpDelegateImpl mvpDelegate;
    MvpDelegateCallback mvpDelegateCallback;
    Ability ability;
    PacMap pacMap;
    MvpPresenter mvpPresenter;
    MvpView mvpView;
    boolean detachFlag;

    @Before
    public void setUp() throws Exception {

        ability = EventHelper.startAbility(MainAbility.class);
        mvpPresenter = new MvpPresenter() {
            @Override
            public void attachView(MvpView view) {

            }

            @Override
            public void detachView(boolean retainInstance) {

            }

            @Override
            public void detachView() {
                detachFlag = true;
            }

            @Override
            public void destroy() {

            }
        };
        mvpView = new MvpView() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }
        };

        mvpDelegateCallback = new MvpDelegateCallback() {
            @Override
            public MvpPresenter createPresenter() {
                return mvpPresenter;
            }

            @Override
            public MvpPresenter getPresenter() {
                return mvpPresenter;
            }

            @Override
            public void setPresenter(MvpPresenter presenter) {

            }

            @Override
            public MvpView getMvpView() {
                return mvpView;
            }
        };
        mvpDelegate = new AbilityMvpDelegateImpl(ability, mvpDelegateCallback, true);
        mvpDelegate.onActive();
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }

    @Test
    public void onStop() {
        detachFlag = false;
        mvpDelegate.onStop();
        assertTrue(detachFlag);
    }

    @Test
    public void onActive() {
        mvpDelegate.onActive();
        assertNotNull(mvpDelegate.getPresenter());
    }

    @Test
    public void onSaveAbilityState() {
        pacMap = new PacMap();
        mvpDelegate.onSaveAbilityState(pacMap);
        assertNotNull(pacMap.getString("com.hannesdorfmann.mosby3.activity.mvp.id"));
    }

    @Test
    public void onRestoreAbilityState() {
        pacMap = new PacMap();
        mvpDelegate.onSaveAbilityState(pacMap);
        mvpDelegate.onRestoreAbilityState(pacMap);
        assertNotNull(pacMap.getString("com.hannesdorfmann.mosby3.activity.mvp.id"));
    }

}