package com.hannesdorfmann.mosby3.sample.mvp.customviewstate;

import com.hannesdorfmann.mosby3.sample.EventHelper;
import com.hannesdorfmann.mosby3.sample.MainAbility;
import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.TestUtils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyCustomAbilitySliceTest {

    MyCustomAbilitySlice abilitySlice;
    IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @Before
    public void setUp() throws Exception {
        abilitySlice = EventHelper.startAbilitySlice(MainAbility.class, MyCustomAbilitySlice.class);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    @Test
    public void onStart() {
        assertNotNull(abilitySlice);
        TestUtils.sleep(2000);

        Text textA = (Text) abilitySlice.findComponentById(ResourceTable.Id_textViewA);
        Text textB = (Text) abilitySlice.findComponentById(ResourceTable.Id_textViewB);

        Component aBtn = abilitySlice.findComponentById(ResourceTable.Id_loadA);
        assertTrue(aBtn != null);
        String a1 = textA.getText().toString();
        assertNotNull(a1);
        EventHelper.triggerClickEvent(abilitySlice.getAbility(), aBtn);
        TestUtils.sleep(2000);
        String a2 = textA.getText().toString();
        assertNotNull(a2);
        assertNotSame(a1, a2);

        Component bBtn = abilitySlice.findComponentById(ResourceTable.Id_loadB);
        assertTrue(bBtn != null);
        String b1 = textB.getText().toString();
        assertNotNull(b1);
        EventHelper.triggerClickEvent(abilitySlice.getAbility(), bBtn);
        TestUtils.sleep(2000);
        String b2 = textB.getText().toString();
        assertNotNull(b2);
        assertNotSame(b1, b2);
    }

}