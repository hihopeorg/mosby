/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 */

package com.hannesdorfmann.mosby3.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.media.image.PixelMap;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TestUtils {

    public static Context getContext(){
        return AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    }

    /**
     * 启动一个 abilitySlice 并按下一个按钮 等待2秒后銷毀這個slice
     * @param abilitySlice
     * @param btnId
     */
    public static void startAbilitySliceAndClickBtn(Class<? extends AbilitySlice> abilitySlice , int btnId) {
        AbilitySlice slice = EventHelper.startAbilitySlice(MainAbility.class, abilitySlice);
        assertTrue(slice != null);
        TestUtils.sleep(2000);
        Component downloadBtn = slice.findComponentById(btnId);
        assertTrue(downloadBtn != null);
        EventHelper.triggerClickEvent(slice.getAbility(), downloadBtn);
        TestUtils.sleep(2000);
        slice.terminateAbility();
//        EventHelper.clearAbilities();
    }

    /**
     * 启动一个 abilitySlice
     * @param abilitySlice
     * @return
     */
    public static AbilitySlice startAbilitySlice(Class<? extends AbilitySlice> abilitySlice) {
        AbilitySlice slice = EventHelper.startAbilitySlice(MainAbility.class, abilitySlice);
        assertNotNull(slice);
        TestUtils.sleep(2000);
//        slice.terminateAbility();
        return slice;
    }

    public static Ability startAbility(Class<? extends Ability> ability) {
        Ability ability1 = EventHelper.startAbility(ability);
        assertNotNull(ability1);
        TestUtils.sleep(2000);
        return ability1;
    }

    public static void sleep(long time){
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();

        }
    }
}
