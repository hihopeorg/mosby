package com.hannesdorfmann.mosby3.sample.mvp.lce.Fraction;

import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.StackLayoutContainerAbility;
import com.hannesdorfmann.mosby3.sample.TestUtils;
import com.hannesdorfmann.mosby3.sample.EventHelper;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesAdapter;
import com.hannesdorfmann.mosby3.sample.mvp.model.Country;
import com.hannesdorfmann.swiperefresh.refresh.SwipeRefreshLayout;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class CountriesFractionTest {

    StackLayoutContainerAbility ability;
    final String tag = "CountriesFragment";

    @Before
    public void setUp() throws Exception {

        String tag = "CountriesFragment";
        IntentParams params = new IntentParams();
        params.setParam("fragment", tag);
        ability = EventHelper.startAbilityWithParams(StackLayoutContainerAbility.class, params);

    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    @Test
    public void onStart() {

        assertNotNull(ability);
        TestUtils.sleep(2000);

        Optional<Fraction> fractionOptional = ability.getFractionManager().getFractionByTag(tag);
        CountriesFraction fraction = null;
        if(fractionOptional.isPresent()){
            fraction = (CountriesFraction) fractionOptional.get();
        }
        assertNotNull(fraction);
        TestUtils.sleep(2000);

        ListContainer listContainer = (ListContainer) ability.findComponentById(ResourceTable.Id_listView);
        CountriesAdapter adapter = (CountriesAdapter) listContainer.getItemProvider();
        assertNotNull(adapter);

        List<Country> list = adapter.getCountries();
        if(list == null || list.size() == 0){
            Text errorView = (Text) ability.findComponentById(ResourceTable.Id_errorView);
            assertNotNull(errorView.getText().toString());
            TestUtils.sleep(2000);
            EventHelper.triggerClickEvent(ability, errorView);

            TestUtils.sleep(2000);

            list = adapter.getCountries();
            if(list != null && list.size() > 0){
                SwipeRefreshLayout contentView = (SwipeRefreshLayout) ability.findComponentById(ResourceTable.Id_contentView);
                TestUtils.sleep(2000);
                ability.getUITaskDispatcher().syncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        contentView.refresh();

                        TestUtils.sleep(3000);
                        List<Country> tmpList = adapter.getCountries();
                        assertNotNull(tmpList);
                    }
                });

            }else{
                assertNotNull(errorView.getText().toString());
            }
        }else{
            SwipeRefreshLayout contentView = (SwipeRefreshLayout) ability.findComponentById(ResourceTable.Id_contentView);
            ability.getUITaskDispatcher().syncDispatch(new Runnable() {
                @Override
                public void run() {
                    contentView.refresh();

                    TestUtils.sleep(3000);
                    List<Country> tmpList = adapter.getCountries();
                    assertNotNull(tmpList);
                }
            });

        }
    }

}