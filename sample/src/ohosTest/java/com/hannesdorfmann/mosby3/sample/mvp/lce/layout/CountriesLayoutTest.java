package com.hannesdorfmann.mosby3.sample.mvp.lce.layout;

import com.hannesdorfmann.mosby3.sample.EventHelper;
import com.hannesdorfmann.mosby3.sample.MainAbility;
import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.TestUtils;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesAdapter;
import com.hannesdorfmann.mosby3.sample.mvp.model.Country;
import com.hannesdorfmann.mosby3.sample.slice.CountriesLayoutAbilitySlice;
import com.hannesdorfmann.swiperefresh.refresh.SwipeRefreshLayout;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CountriesLayoutTest {

    CountriesLayoutAbilitySlice slice;
    IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @Before
    public void setUp() throws Exception {
        slice = (CountriesLayoutAbilitySlice) EventHelper.startAbilitySlice(MainAbility.class, CountriesLayoutAbilitySlice.class);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    @Test
    public void loadData() {
        TestUtils.sleep(2000);
        assertNotNull(slice);

        CountriesLayout mCountriesLayout = (CountriesLayout) slice.findComponentById(ResourceTable.Id_dummyId);
        assertNotNull(mCountriesLayout);

        ListContainer listContainer = (ListContainer) slice.findComponentById(ResourceTable.Id_listView);
        CountriesAdapter adapter = (CountriesAdapter) listContainer.getItemProvider();
        assertNotNull(adapter);

        List<Country> list = adapter.getCountries();
        if(list == null || list.size() == 0){
            Text errorView = (Text) slice.findComponentById(ResourceTable.Id_errorView);
            assertNotNull(errorView.getText().toString());
            TestUtils.sleep(2000);
            EventHelper.triggerClickEvent(slice.getAbility(), errorView);

            TestUtils.sleep(2000);

            list = adapter.getCountries();
            if(list != null && list.size() > 0){
                SwipeRefreshLayout contentView = (SwipeRefreshLayout) slice.findComponentById(ResourceTable.Id_contentView);
                TestUtils.sleep(2000);
                slice.getUITaskDispatcher().syncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        contentView.refresh();

                        TestUtils.sleep(3000);
                        List<Country> tmpList = adapter.getCountries();
                        assertNotNull(tmpList);
                    }
                });

            }else{
                assertNotNull(errorView.getText().toString());
            }
        }else{
            SwipeRefreshLayout contentView = (SwipeRefreshLayout) slice.findComponentById(ResourceTable.Id_contentView);
            slice.getUITaskDispatcher().syncDispatch(new Runnable() {
                @Override
                public void run() {
                    contentView.refresh();

                    TestUtils.sleep(3000);
                    List<Country> tmpList = adapter.getCountries();
                    assertNotNull(tmpList);
                }
            });

        }
    }

}