package com.hannesdorfmann.mosby3.sample.mvp.customviewstate;

import com.hannesdorfmann.mosby3.sample.*;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class MyCustomFractionTest {

    String tag = "CustomViewStateFragment";
    StackLayoutContainerAbility ability;
    IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @Before
    public void setUp() throws Exception {
        String tag = "CustomViewStateFragment";
        IntentParams params = new IntentParams();
        params.setParam("fragment", tag);
        ability = EventHelper.startAbilityWithParams(StackLayoutContainerAbility.class, params);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    @Test
    public void onStart() {
        assertNotNull(ability);
        TestUtils.sleep(2000);

        Optional<Fraction> fraction = ability.getFractionManager().getFractionByTag(tag);
        assertNotNull(fraction);
        TestUtils.sleep(2000);

        Text textA = (Text) ability.findComponentById(ResourceTable.Id_textViewA);
        Text textB = (Text) ability.findComponentById(ResourceTable.Id_textViewB);


        Component aBtn = ability.findComponentById(ResourceTable.Id_loadA);
        assertTrue(aBtn != null);
        String a1 = textA.getText().toString();
        assertNotNull(a1);
        EventHelper.triggerClickEvent(ability, aBtn);

        TestUtils.sleep(2000);
        String a2 = textA.getText().toString();
        assertNotNull(a2);
        assertNotSame(a1, a2);

        Component bBtn = ability.findComponentById(ResourceTable.Id_loadB);
        assertTrue(bBtn != null);
        String b1 = textB.getText().toString();
        assertNotNull(b1);
        EventHelper.triggerClickEvent(ability, bBtn);
        TestUtils.sleep(2000);
        String b2 = textB.getText().toString();
        assertNotNull(b2);
        assertNotSame(b1, b2);
    }
}