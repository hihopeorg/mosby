package com.hannesdorfmann.mosby3.sample.slice;

import com.hannesdorfmann.mosby3.sample.EventHelper;
import com.hannesdorfmann.mosby3.sample.ResourceTable;
import com.hannesdorfmann.mosby3.sample.TestUtils;
import com.hannesdorfmann.mosby3.sample.mvp.CountriesAdapter;
import com.hannesdorfmann.mosby3.sample.mvp.lce.ability.CountriesAbilityNew;
import com.hannesdorfmann.mosby3.sample.mvp.model.Country;
import com.hannesdorfmann.swiperefresh.refresh.SwipeRefreshLayout;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Text;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CountriesAbilitySliceTest {

    CountriesAbilityNew countriesAbilityNew;
    IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    CountriesAbilitySlice slice;

    @Before
    public void setUp() throws Exception {
        countriesAbilityNew = EventHelper.startAbility(CountriesAbilityNew.class);
        TestUtils.sleep(2000);
        slice = (CountriesAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(countriesAbilityNew);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    @Test
    public void onStart() {
        TestUtils.sleep(3000);
        assertNotNull(slice);

        CountriesAdapter adapter = slice.getAdapter();
        assertNotNull(adapter);

        List<Country> list = adapter.getCountries();

        if(list == null || list.size() == 0){
            Text errorView = ((Text)slice.findComponentById(ResourceTable.Id_errorView));
            assertNotNull(errorView.getText().toString());
            TestUtils.sleep(2000);
            EventHelper.triggerClickEvent(slice.getAbility(), errorView);

            TestUtils.sleep(3000);
            list = adapter.getCountries();
            if(list != null && list.size() > 0){
                SwipeRefreshLayout contentView = (SwipeRefreshLayout) slice.findComponentById(ResourceTable.Id_contentView);
                TestUtils.sleep(2000);

                slice.getUITaskDispatcher().syncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        contentView.refresh();
                        TestUtils.sleep(3000);
                        List<Country> tmpList = adapter.getCountries();
                        assertNotNull(tmpList);
                    }
                });

            }
        }else{
            slice.getUITaskDispatcher().syncDispatch(new Runnable() {
                @Override
                public void run() {
                    SwipeRefreshLayout contentView = (SwipeRefreshLayout) slice.findComponentById(ResourceTable.Id_contentView);
                    contentView.refresh();

                    TestUtils.sleep(3000);
                    List<Country> tmpList = adapter.getCountries();
                    assertNotNull(tmpList);
                }
            });
        }

    }

}