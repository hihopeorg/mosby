# mosby

**本项目是基于开源项目mosby进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/sockeqwe/mosby
   ）追踪到原项目版本**

#### 项目介绍

- 项目名称：开源mvi、mvp模式适配项目
- 所属系列：ohos的第三方组件适配移植
- 功能：mvi、mvp模式下对Ability、AbilitySlice、Fraction、自定义布局页中适配
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/sockeqwe/mosby
- 原项目基线版本：V3.1.1，shal:c234625fdfb46ce7fe2a0695a6272953db20865f
- 编程语言：Java 
- 外部库依赖：rxJava2


#### 效果展示

![效果展示](.\screenshot\sample.gif)
![效果展示](.\screenshot\sample_mvi.gif)

#### 安装教程

- MVP功能集成

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址 

   ```
   repositories {   
       maven {      
       	url 'http://106.15.92.248:8081/repository/Releases/'    
       } 
    } 
   ```

2. 在需要引入项目的build.gradle文件下添加如下：    

   ```
   dependencies {
   	implementation 'com.hannesdorfmann.mosby3.ohos:mvp:1.0.1'
   	implementation 'com.hannesdorfmann.mosby3.ohos:mvp_lce:1.0.1'
   	implementation 'com.hannesdorfmann.mosby3.ohos:viewstate:1.0.1'
   	implementation 'com.hannesdorfmann.mosby3.ohos:mvp_queuing_presenter:1.0.1'
   	implementation 'com.hannesdorfmann.mosby3.ohos:swiperefresh:1.0.1'
   }
   ```

   

- MVI功能集成

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址 

   ```
   repositories {   
       maven {      
       	url 'http://106.15.92.248:8081/repository/Releases/'    
       } 
    } 
   ```

2. 在需要引入项目的build.gradle文件下添加如下：

   ```
   dependencies {
       implementation 'com.hannesdorfmann.mosby3.ohos:mvi:1.0.1'
       implementation 'com.hannesdorfmann.mosby3.ohos:rxbinding2_0_0:1.0.1'
   }
   ```

#### 使用说明

- MVP 使用说明

  1. mvp 在Ability中使用：       

     ```
     CountriesView extends MvpLceView<List<Country>>,可根据需求增加操作方法。
     ```

     ```
     SimpleCountriesPresenter extends MvpQueuingBasePresenter<CountriesView>
                                      implements CountriesPresenter,可根据需求增加操作方法。                                    
     
     ```

     ```
     MyAbility extends MvpLceAbility<SwipeRefreshLayout, List<Country>, CountriesView, CountriesPresenter>
                     implements CountriesView, IRefresh.RefreshListener
     ```

     实现下面方法      

     ```
         ComponentContainer getUIContent()获取页面布局，
         void initCompoment()初始化页面控件，
         CountriesPresenter createPresenter()初始化presenter,
         void onRefresh()请求刷新执行代码，
         boolean enableRefresh()是否可以下拉刷新设置，
         void showLoading(boolean pullToRefresh)请求加载时操作处理，
         void showContent()加载完成显示处理，
         void showError(Throwable e, boolean pullToRefresh)加载数据异常时处理，
         void setData(M data)请求后设置数据，
         void loadData(boolean pullToRefresh)请求数据执行方法，
         以及自定义方法
     ```

       

  2. mvp 在AbilitySlice中使用：    

     ```
     CountriesView extends MvpLceView<List<Country>>,可根据需求增加操作方法。
     ```

     ```
     SimpleCountriesPresenter extends MvpQueuingBasePresenter<CountriesView>
                                      implements CountriesPresenter,可根据需求增加操作方法。 
     ```

     ```
     MyAbilitySlice extends MvpLceAbilitySlice<SwipeRefreshLayout, List<Country>, CountriesView, CountriesPresenter>
                     implements CountriesView, IRefresh.RefreshListener
     ```

     实现下面方法      

     ```
     ComponentContainer getUIContent()获取页面布局，
     void initCompoment()初始化页面控件，
     CountriesPresenter createPresenter()初始化presenter,
     void onRefresh()请求刷新执行代码，
     boolean enableRefresh()是否可以下拉刷新设置，
     void showLoading(boolean pullToRefresh)请求加载时操作处理，
     void showContent()加载完成显示处理，
     void showError(Throwable e, boolean pullToRefresh)加载数据异常时处理，
     void setData(M data)请求后设置数据，
     void loadData(boolean pullToRefresh)请求数据执行方法，
     以及自定义方法
     ```

  3. mvp 在Fraction中使用：    

     ```
     CountriesView extends MvpLceView<List<Country>>,可根据需求增加操作方法。
     ```

     ```
     SimpleCountriesPresenter extends MvpQueuingBasePresenter<CountriesView>
                                      implements CountriesPresenter,可根据需求增加操作方法。     
     ```

     ```
     MyFraction extends MvpLceViewStateFraction<SwipeRefreshLayout, List<Country>, CountriesView, CountriesPresenter>
             implements CountriesView, IRefresh.RefreshListener
     ```

     实现下面方法       

     ```
      int getUIContent()获取页面布局，
      void initCompoment()初始化页面控件，
      CountriesPresenter createPresenter()初始化presenter,
      void onRefresh()请求刷新执行代码，
      boolean enableRefresh()是否可以下拉刷新设置，
      void showLoading(boolean pullToRefresh)请求加载时操作处理，
      void showContent()加载完成显示处理，
      void showError(Throwable e, boolean pullToRefresh)加载数据异常时处理，
      void setData(M data)请求后设置数据，
      void loadData(boolean pullToRefresh)请求数据执行方法，
      以及自定义方法
     ```

  4. mvp在自定义布局中使用：    

     ```
     CountriesView extends MvpLceView<List<Country>>,可根据需求增加操作方法。    
     ```

     ```
     SimpleCountriesPresenter extends MvpQueuingBasePresenter<CountriesView>
                                      implements CountriesPresenter,可根据需求增加操作方法。 
     ```

     ```
     MyLayout extends MvpViewStateStackLayout<SwipeRefreshLayout, List<Country>, CountriesView, CountriesPresenter>
             implements CountriesView, IRefresh.RefreshListener
     ```

     实现下面方法       

     ```
      int getUIContent()获取页面布局，
      void initCompoment()初始化页面控件，
      CountriesPresenter createPresenter()初始化presenter,
      void onRefresh()请求刷新执行代码，
      boolean enableRefresh()是否可以下拉刷新设置，
      void showLoading(boolean pullToRefresh)请求加载时操作处理，
      void showContent()加载完成显示处理，
      void showError(Throwable e, boolean pullToRefresh)加载数据异常时处理，
      void setData(M data)请求后设置数据，
      void loadData(boolean pullToRefresh)请求数据执行方法,
      onAttachedToWindowDo(Component component)该布局attached后需执行的操作,
      void onDetachedFromWindowDo(Component component)该布局dettached后需执行的操作，
      以及自定义方法
     ```


- MVI 使用说明

  1. mvi在Ability中的使用：    

     ```
     MyProductDetailsPresenter  extends MviBasePresenter<ProductDetailsView, ProductDetailsViewState>
         实现void bindIntents()；    
     ```

     ```
     ProductDetailsView extends MvpView
         定义处理方法；
     ```

     ```
     MyAbility extends MviAbility<ProductDetailsView, ProductDetailsPresenter> implements ProductDetailsView
         实现 void createPresenter()初始化presenter和ProductDetailsView自定义的方法
     ```

  2. mvi在AbilitySlice中的使用：    

     ```
     MyProductDetailsPresenter  extends MviBasePresenter<ProductDetailsView, ProductDetailsViewState>
         实现void bindIntents()；    
     ```

     ```
      ProductDetailsView extends MvpView
         定义处理方法；
     ```

     ```
     MyAbilitySlice extends MviAbilitySlice<ProductDetailsView, ProductDetailsPresenter> implements ProductDetailsView
         实现
         void createPresenter()初始化presenter和ProductDetailsView自定义的方法
     ```

  3. mvi在Fraction中的使用：    

     ```
     MyProductDetailsPresenter  extends MviBasePresenter<ProductDetailsView, ProductDetailsViewState>
         实现void bindIntents()；
     ```

     ```
     ProductDetailsView extends MvpView
         定义处理方法；
     ```

     ```
     MyFraction extends MviFraction<ProductDetailsView, ProductDetailsPresenter> implements ProductDetailsView
         实现
         void createPresenter()初始化presenter和ProductDetailsView自定义的方法
     ```

  4. mvi在自定义布局中的使用：
         

     ```
     MainMenuPresenter  extends MviBasePresenter<ProductDetailsView, ProductDetailsViewState>
         实现void bindIntents()；    
     ```

     ```
     MainMenuView extends MvpView
         定义处理方法；    
     ```

     ```
     MyLayout extends MviStackLayout<MainMenuView, MainMenuPresenter> implements MainMenuView
         实现
         void createPresenter()初始化presenter和ProductDetailsView自定义的方法
     ```


     


#### 版本迭代

- v1.0.1

  

#### 版权和许可信息

Apache License，version 2.0
