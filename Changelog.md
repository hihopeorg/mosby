## version 1.0.1

- 添加JUnit单元测试和自动化测试

## version 1.0.0

- mvi依賴包rxjava源码导入工程，用于辅助实现mvi功能

- mvi依赖包rxbinding引用部分的功能源码导入工程，用于辅助实现mvi功能

- mvi依賴包rxAnroid源码导入工程，用于辅助实现mvi功能

- 增加下拉刷新swiperefresh组件

- 增加CirleProgressImg加载图，辅助请求时加载显示

- mvp功能实现，针对Ability、AbilitySlice、Fraction、自定义布局中引用

- mvi功能实现，针对Ability、AbilitySlice、Fraction、自定义布局中引用

  

## MVP说明：

1. 增加SwipeRfresh，下拉刷新模块，实现扩充实现下拉刷新功能
2. 增加CirleProgressImg加载图，辅助请求时加载显示

- mvp smaple未实现：

   1. 在布局中直接引用界面，ohos不支持，替换方案：代码动态加载

   2. ohos不支持PageSlider和Fraction组合使用，原fraction引用逻辑，在ComponentOwner实现类中以实现效果，并未在lib中做封装处理

- mvi smaple未实现：

​    抽屉组件SlideDrawer因暂未开放，该共能只是模拟实现页面功能，后续开放后，如有需要可在sample中补全