package com.hannesdorfmann.mosby3.sample.mvi.slice;

import com.hannesdorfmann.mosby3.sample.mvi.EventHelper;
import com.hannesdorfmann.mosby3.sample.mvi.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvi.TestUtils;
import com.hannesdorfmann.mosby3.sample.mvi.view.detail.ProductDetailsAbilityNew;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.Text;
import org.junit.Test;
import static org.junit.Assert.*;

public class ProductDetailsAbilitySliceTest {

    @Test
    public void onStart() {

        IntentParams params = new IntentParams();
        params.setParam("productId", 8);
        ProductDetailsAbilityNew slice = EventHelper.startAbilityWithParams(ProductDetailsAbilityNew.class, params);

        TestUtils.sleep(5000);
        assertNotNull(slice);

        Text price = (Text) slice.findComponentById(ResourceTable.Id_price);
        assertNotNull(price.getText().toString());

        Text description = (Text) slice.findComponentById(ResourceTable.Id_description);
        assertNotNull(description.getText().toString());
    }

}