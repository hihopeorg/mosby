package com.hannesdorfmann.mosby3.sample.mvi.view.menu;

import com.hannesdorfmann.mosby3.sample.mvi.*;
import com.hannesdorfmann.mosby3.sample.mvi.view.widge.CirleProgressImg;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class MainMenuLayoutTest {

    MainAbility ability;
    AbilitySlice slice;
    IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @Before
    public void setUp() throws Exception {
        ability = EventHelper.startAbility(MainAbility.class);
        TestUtils.sleep(3000);
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        slice = sAbilityDelegator.getCurrentAbilitySlice(ability);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
//        Thread.sleep(2000);
    }



    @Test
    public void render() {
        TestUtils.sleep(3000);

        Image imMenu = (Image) slice.findComponentById(ResourceTable.Id_imMenu);
        EventHelper.triggerClickEvent(slice.getAbility(), imMenu);
        TestUtils.sleep(8000);

        MainMenuLayout mainMenu = (MainMenuLayout) slice.findComponentById(ResourceTable.Id_mainMenu);
        assertNotNull(mainMenu);

        ListContainer listContainer = (ListContainer) mainMenu.findComponentById(ResourceTable.Id_listContainer);
        assertNotNull(listContainer);

        CirleProgressImg loadingView = (CirleProgressImg) mainMenu.findComponentById(ResourceTable.Id_loadingView);

        do{
            TestUtils.sleep(2000);
        }while(loadingView.getVisibility() == Component.VISIBLE);
        int count = listContainer.getChildCount();

        Text errorView = (Text) mainMenu.findComponentById(ResourceTable.Id_errorView);
        if(errorView.getVisibility() == Component.VISIBLE){
            assertTrue(count == 0);
        }else{
            assertTrue(count > 0);
        }
    }
}