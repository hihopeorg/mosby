package com.hannesdorfmann.mosby3.sample.mvi.view.shoppingcartoverview;

import com.hannesdorfmann.mosby3.sample.mvi.EventHelper;
import com.hannesdorfmann.mosby3.sample.mvi.MainAbility;
import com.hannesdorfmann.mosby3.sample.mvi.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvi.TestUtils;
import com.hannesdorfmann.mosby3.sample.mvi.view.shoppingcartlabel.ShoppingCartLabel;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.agp.components.ListContainer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class ShoppingCartOverviewFractionTest {

    MainAbility slice;
    @Before
    public void setUp() throws Exception {
        slice = EventHelper.startAbility(MainAbility.class);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }

    @Test
    public void render() {
        TestUtils.sleep(2000);
        assertNotNull(slice);

        Optional<Fraction> fractionOptional = slice.getFractionManager().getFractionByTag(ShoppingCartOverviewFraction.class.getSimpleName());

        ShoppingCartOverviewFraction fraction;
        if(fractionOptional.isPresent()){
            fraction = (ShoppingCartOverviewFraction) fractionOptional.get();

            assertNotNull(fraction);
            ListContainer listContainer = (ListContainer) slice.findComponentById(ResourceTable.Id_shoppingCartListContainer);
            assertNotNull(listContainer);

            ShoppingCartOverviewAdapter adapter = (ShoppingCartOverviewAdapter) listContainer.getItemProvider();
            assertNotNull(adapter);

            int count = adapter.getCount();

            ShoppingCartLabel shoppingCartLabel = (ShoppingCartLabel) slice.findComponentById(ResourceTable.Id_shoppingCartLabel);
            String str = shoppingCartLabel.getText();

            if(count == 0){
                assertEquals("", str);
            }

        }
    }
}