package com.hannesdorfmann.mosby3.sample.mvi.view.home;

import com.hannesdorfmann.mosby3.sample.mvi.EventHelper;
import com.hannesdorfmann.mosby3.sample.mvi.MainAbility;
import com.hannesdorfmann.mosby3.sample.mvi.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvi.TestUtils;
import com.hannesdorfmann.mosby3.sample.mvi.businesslogic.model.FeedItem;
import com.hannesdorfmann.mosby3.sample.mvi.slice.MainAbilitySlice;
import com.hannesdorfmann.mosby3.sample.mvi.view.widge.CirleProgressImg;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.agp.components.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class HomeFractionTest {

    MainAbility ability;
    @Before
    public void setUp() throws Exception {
        ability = EventHelper.startAbility(MainAbility.class);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
        Thread.sleep(2000);
    }

    @Test
    public void onProductClicked() {
        TestUtils.sleep(2000);
        assertNotNull(ability);

        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        MainAbilitySlice slice = (MainAbilitySlice) sAbilityDelegator.getCurrentAbilitySlice(ability);
        HomeFraction fraction = slice.getHomeFraction();
        if(fraction != null)
        {
            TestUtils.sleep(1000);
            ComponentContainer container = (ComponentContainer) slice.findComponentById(ResourceTable.Id_fragmentContainer);
            assertNotNull(container);
            ListContainer listContainer = (ListContainer) container.findComponentById(ResourceTable.Id_listContainer);
            assertNotNull(listContainer);

            HomeAdapter adapter = (HomeAdapter) listContainer.getItemProvider();
            assertNotNull(adapter);

            TestUtils.sleep(8000);

            CirleProgressImg loadingView = (CirleProgressImg) container.findComponentById(ResourceTable.Id_loadingView);
            do{
                TestUtils.sleep(2000);
            }while(loadingView.getVisibility() == Component.VISIBLE);

            List<FeedItem> list =  adapter.getItems();
            Text errorView = (Text) slice.findComponentById(ResourceTable.Id_errorView);
            if(errorView.getVisibility() == Component.VISIBLE){
                assertTrue(list == null || list.size() == 0);
            }else{
                assertTrue(list != null && list.size() > 0);
            }

        }
    }

}