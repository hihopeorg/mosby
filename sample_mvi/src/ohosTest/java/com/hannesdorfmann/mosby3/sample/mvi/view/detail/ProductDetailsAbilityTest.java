package com.hannesdorfmann.mosby3.sample.mvi.view.detail;

import com.hannesdorfmann.mosby3.sample.mvi.EventHelper;
import com.hannesdorfmann.mosby3.sample.mvi.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvi.TestUtils;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.Text;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProductDetailsAbilityTest {

    @Test
    public void onStart() {
        IntentParams params = new IntentParams();
        params.setParam("productId", 8);
        ProductDetailsAbility ability = EventHelper.startAbilityWithParams(ProductDetailsAbility.class, params);
        assertNotNull(ability);
        TestUtils.sleep(2000);

        Text price = (Text) ability.findComponentById(ResourceTable.Id_price);
        assertNotNull(price.getText().toString());

        Text description = (Text) ability.findComponentById(ResourceTable.Id_description);
        assertNotNull(description.getText().toString());
    }
}