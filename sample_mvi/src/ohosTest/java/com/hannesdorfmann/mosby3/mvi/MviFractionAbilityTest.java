package com.hannesdorfmann.mosby3.mvi;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class MviFractionAbilityTest {

    MviFractionAbility mviFractionAbility;

    @Before
    public void setUp() throws Exception {
        mviFractionAbility = new MviFractionAbility() {
            @Override
            public MviPresenter createPresenter() {
                return new MviPresenter() {
                    @Override
                    public void attachView(MvpView view) {

                    }

                    @Override
                    public void detachView(boolean retainInstance) {

                    }

                    @Override
                    public void detachView() {

                    }

                    @Override
                    public void destroy() {

                    }
                };
            }
        };
    }

    @Test
    public void getMvpDelegate() {
        assertNotNull(mviFractionAbility.getMvpDelegate());
    }

    @Test
    public void getMvpView() {
        assertNotNull(mviFractionAbility.getMvpView());
    }

}