package com.hannesdorfmann.mosby3.mvi;

import com.hannesdorfmann.mosby3.AbilityMviDelegateImpl;
import com.hannesdorfmann.mosby3.MviDelegateCallback;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.sample.mvi.EventHelper;
import com.hannesdorfmann.mosby3.sample.mvi.view.detail.ProductDetailsAbility;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.aafwk.content.IntentParams;
import ohos.utils.PacMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AbilityMviDelegateImplTest {

    AbilityMviDelegateImpl abilityMviDelegate;
    MviDelegateCallback mviDelegateCallback;
    ProductDetailsAbility ability;
    IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    boolean detachFlag;

    @Before
    public void setUp() throws Exception {
        IntentParams params = new IntentParams();
        params.setParam("productId", 8);
        ability = EventHelper.startAbilityWithParams(ProductDetailsAbility.class, params);

        mviDelegateCallback = new MviDelegateCallback() {
            @Override
            public MviPresenter createPresenter() {
                return new MviPresenter(){
                    @Override
                    public void attachView(MvpView view) {

                    }

                    @Override
                    public void detachView(boolean retainInstance) {

                    }

                    @Override
                    public void detachView() {
                        detachFlag = true;
                    }

                    @Override
                    public void destroy() {

                    }
                };
            }

            @Override
            public MvpView getMvpView() {
                return new MvpView() {
                    @Override
                    public int hashCode() {
                        return super.hashCode();
                    }
                };
            }

            @Override
            public void setRestoringViewState(boolean restoringViewState) {

            }
        };

        abilityMviDelegate = new AbilityMviDelegateImpl(ability, mviDelegateCallback);
    }

    @After
    public void tearDown() throws Exception {
        sAbilityDelegator.stopAbility(ability);
        EventHelper.clearAbilities();
    }

    @Test
    public void onStop() {
        abilityMviDelegate.onStop();
        assertNull(abilityMviDelegate.getPresenter());
    }

    @Test
    public void onActive() {
        abilityMviDelegate.onActive();
        assertNotNull(abilityMviDelegate.getPresenter());
    }

    @Test
    public void onBackground() {
        detachFlag = false;
        abilityMviDelegate.onActive();
        abilityMviDelegate.onBackground();
        assertTrue(detachFlag);
    }

    @Test
    public void onSaveAbilityState() {
        abilityMviDelegate.onActive();
        PacMap outState = new PacMap();
        abilityMviDelegate.onSaveAbilityState(outState);

        assertNotNull(outState.getString("com.hannesdorfmann.mosby3.activity.mvi.id"));
    }

}