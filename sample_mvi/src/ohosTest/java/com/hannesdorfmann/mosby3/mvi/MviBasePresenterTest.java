package com.hannesdorfmann.mosby3.mvi;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MviBasePresenterTest {
    MviBasePresenter mviBasePresenter;
    MvpView mvpView;
    boolean attrachFlag;
    boolean destoryFlag;
    boolean detachFlag;

    @Before
    public void setUp() throws Exception {
        mvpView = new MvpView() {
            @Override
            public int hashCode() {
                return super.hashCode();
            }
        };
        mviBasePresenter = new MviBasePresenter() {
            @Override
            protected void bindIntents() {
                attrachFlag = true;
            }

            @Override
            protected void unbindIntents() {
                super.unbindIntents();
                destoryFlag = true;
            }

            @Override
            public void detachView() {
                super.detachView();
                detachFlag = true;
            }
        };
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getViewStateObservable() {
        assertNotNull(mviBasePresenter.getViewStateObservable());
    }

    @Test
    public void attachView() {
        attrachFlag = false;
        mviBasePresenter.attachView(mvpView);
        assertTrue(attrachFlag);
    }

    @Test
    public void detachView() {
        detachFlag = false;
        mviBasePresenter.detachView();
        assertTrue(detachFlag);
    }

    @Test
    public void destroy() {
        destoryFlag = false;
        mviBasePresenter.destroy();
        assertTrue(destoryFlag);
    }

}