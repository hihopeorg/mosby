package com.hannesdorfmann.mosby3.mvi.layout;

import com.hannesdorfmann.mosby3.ComponentContainerMviDelegateImpl;
import com.hannesdorfmann.mosby3.mvi.MviPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.sample.mvi.EventHelper;
import com.hannesdorfmann.mosby3.sample.mvi.TestAbility;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.*;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MviStackLayoutTest {

    Context context;
    MviStackLayout mviStackLayout;
    Ability ability;
    IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @Before
    public void setUp() throws Exception {
        ability = EventHelper.startAbility(TestAbility.class);
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        mviStackLayout = new MviStackLayout(ability) {
            @Override
            protected void onAttachedToWindowDo(Component component) {

            }

            @Override
            protected void onDetachedFromWindowDo(Component component) {
            }

            @Override
            public Context getmComponetContext() {
                return ability;
            }

            @Override
            public MviPresenter createPresenter() {

                return new MviPresenter(){

                    @Override
                    public void attachView(MvpView view) {

                    }

                    @Override
                    public void detachView(boolean retainInstance) {
                    }

                    @Override
                    public void detachView() {

                    }

                    @Override
                    public void destroy() {

                    }
                };
            }
        };
        mviStackLayout.setmComponetContext(ability);
        mviStackLayout.getMviDelegate().onAttachedToWindow();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getMviDelegate() {
        assertNotNull(mviStackLayout.getMviDelegate());
        assertTrue(mviStackLayout.getMviDelegate() instanceof ComponentContainerMviDelegateImpl);
    }

    @Test
    public void getMvpView() {
        assertNotNull(mviStackLayout.getMvpView());
        assertTrue(mviStackLayout.getMvpView() instanceof MvpView);
    }

}