package com.hannesdorfmann.mosby3.mvi;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MviAbilitySliceTest {

    MviAbilitySlice mviAbilitySlice;

    @Before
    public void setUp() throws Exception {
        mviAbilitySlice = new MviAbilitySlice() {
            @Override
            public MviPresenter createPresenter() {
                return new MviPresenter() {
                    @Override
                    public void attachView(MvpView view) {

                    }

                    @Override
                    public void detachView(boolean retainInstance) {

                    }

                    @Override
                    public void detachView() {

                    }

                    @Override
                    public void destroy() {

                    }
                };
            }
        };
    }

    @Test
    public void getMvpDelegate() {
        assertNotNull(mviAbilitySlice.getMvpDelegate());
    }

    @Test
    public void getMvpView() {
        assertNotNull(mviAbilitySlice.getMvpView());
    }

}