package com.hannesdorfmann.mosby3.mvi;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.Subject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;

public class DisposableIntentObserverTest {

    DisposableIntentObserver disposableIntentObserver;
    private String str;
    Throwable err;
    private boolean flag = false;
    Observable observable;

    @Before
    public void setUp() throws Exception {
        flag = false;
        disposableIntentObserver = new DisposableIntentObserver(new Subject<String>() {
            @Override
            public boolean hasObservers() {
                return false;
            }

            @Override
            public boolean hasThrowable() {
                return false;
            }

            @Override
            public boolean hasComplete() {
                return false;
            }

            @Override
            public Throwable getThrowable() {
                return null;
            }

            @Override
            protected void subscribeActual(Observer observer) {

            }

            @Override
            public void onSubscribe(Disposable disposable) {

            }

            @Override
            public void onNext(String o) {
                str = o;

            }

            @Override
            public void onError(Throwable throwable) {
                err = throwable;
            }

            @Override
            public void onComplete() {
                flag = true;
            }
        }){
            @Override
            public void onError(Throwable e) {
                err = e;
            }
        };
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void onNext() {
        String test = "test onNext";
        str = null;
        observable = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> e) throws Exception {
                e.onNext(test);
                e.onComplete();
            }
        });
        observable.subscribe(disposableIntentObserver);
        assertEquals(str, test);
    }

    @Test
    public void onError() {
        err = null;
        Throwable throwable = new Throwable("test onError");
        observable = Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(ObservableEmitter<Integer> e) throws Exception {
                e.onError(throwable);
            }
        });
        observable.subscribe(disposableIntentObserver);
        assertNotNull(err);
    }

    @Test
    public void onComplete() {
        flag = false;
        observable = Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> e) throws Exception {
                e.onNext("test onNext");
                e.onComplete();
            }
        });
        observable.subscribe(disposableIntentObserver);
        assertTrue(flag);
    }
}