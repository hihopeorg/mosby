package com.hannesdorfmann.mosby3.mvi;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.sample.mvi.ResourceTable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MviFractionTest {

    MviFraction fraction;

    @Before
    public void setUp() throws Exception {
        fraction = new MviFraction() {
            @Override
            protected int getUIContent() {
                return ResourceTable.Layout_fragment_home;
            }

            @Override
            protected void initCompoment() {
            }

            @Override
            public MviPresenter createPresenter() {
                return new MviPresenter() {
                    @Override
                    public void attachView(MvpView view) {

                    }

                    @Override
                    public void detachView(boolean retainInstance) {

                    }

                    @Override
                    public void detachView() {

                    }

                    @Override
                    public void destroy() {

                    }
                };
            }
        };
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getMvpDelegate() {
        assertNotNull(fraction.getMvpDelegate());
    }

    @Test
    public void getMvpView() {
        assertNotNull(fraction.getMvpView());
    }

}