package com.hannesdorfmann.mosby3.mvi;

import com.hannesdorfmann.mosby3.ComponentContainerMviDelegateCallback;
import com.hannesdorfmann.mosby3.ComponentContainerMviDelegateImpl;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.sample.mvi.EventHelper;
import com.hannesdorfmann.mosby3.sample.mvi.TestUtils;
import com.hannesdorfmann.mosby3.sample.mvi.view.detail.ProductDetailsAbilityNew;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.Component;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ComponentContainerMviDelegateImplTest {

    Component view;
    ComponentContainerMviDelegateCallback componentContainerMviDelegateCallback;
    ComponentContainerMviDelegateImpl componentContainerMviDelegate;
    ProductDetailsAbilityNew ability;
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    boolean attachFlag;
    boolean detachFlag;
    boolean stopFlag;

    @Before
    public void setUp() throws Exception {
        IntentParams params = new IntentParams();
        params.setParam("productId", 8);
        ability = EventHelper.startAbilityWithParams(ProductDetailsAbilityNew.class, params);

        view = new Component(ability);
        componentContainerMviDelegateCallback =new ComponentContainerMviDelegateCallback() {
            @Override
            public Context getmComponetContext() {
                return ability;
            }

            @Override
            public void setmComponetContext(Context context) {

            }

            @Override
            public MviPresenter createPresenter() {
                return new MviBasePresenter() {
                    @Override
                    protected void bindIntents() {
                        attachFlag = true;
                    }

                    @Override
                    public void detachView() {
                        super.detachView();
                        detachFlag = true;
                    }
                };
            }

            @Override
            public MvpView getMvpView() {
                return new MvpView() {
                    @Override
                    public int hashCode() {
                        return super.hashCode();
                    }
                };
            }

            @Override
            public void setRestoringViewState(boolean restoringViewState) {

            }
        };
        componentContainerMviDelegate = new ComponentContainerMviDelegateImpl(view, componentContainerMviDelegateCallback, true){
            @Override
            public void onAbilityStop(Ability ability) {
                super.onAbilityStop(ability);
                stopFlag = true;
            }
        };
    }

    @After
    public void tearDown() throws Exception {
        sAbilityDelegator.stopAbility(ability);
        EventHelper.clearAbilities();
        TestUtils.sleep(2000);
    }

    @Test
    public void onAttachedToWindow() {
        attachFlag = false;
        componentContainerMviDelegate.onAttachedToWindow();
        assertTrue(attachFlag);
    }

    @Test
    public void onDetachedFromWindow() {
        detachFlag = false;
        componentContainerMviDelegate.onAttachedToWindow();
        componentContainerMviDelegate.onDetachedFromWindow();
        assertTrue(detachFlag);
    }

    @Test
    public void onAbilityStop() {
        stopFlag = false;
        componentContainerMviDelegate.onAbilityStop(ability);
        assertTrue(stopFlag);

    }

}