package com.hannesdorfmann.mosby3.mvi;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MviAbilityTest {

    MviAbility mviAbility;

    @Before
    public void setUp() throws Exception {
        mviAbility = new MviAbility(){

            @Override
            public MviPresenter createPresenter() {
                return null;
            }
        };
    }

    @Test
    public void getMvpDelegate() {
        assertNotNull(mviAbility.getMvpDelegate());
    }

    @Test
    public void getMvpView() {
        assertNotNull(mviAbility.getMvpView());
    }
}