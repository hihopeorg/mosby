package com.hannesdorfmann.mosby3.mvi;

import com.hannesdorfmann.mosby3.AbilitySliceMviDelegateImpl;
import com.hannesdorfmann.mosby3.MviDelegateCallback;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.sample.mvi.EventHelper;
import com.hannesdorfmann.mosby3.sample.mvi.TestUtils;
import com.hannesdorfmann.mosby3.sample.mvi.view.detail.ProductDetailsAbilityNew;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.aafwk.content.IntentParams;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class AbilitySliceMviDelegateImplTest {

    MviDelegateCallback mviDelegateCallback;
    AbilitySliceMviDelegateImpl abilitySliceMviDelegate;
    IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    ProductDetailsAbilityNew ability;

    boolean activeFlag,detachFlag;
    MviPresenter presenter;

    @Before
    public void setUp() throws Exception {

        IntentParams params = new IntentParams();
        params.setParam("productId", 8);
        ability = EventHelper.startAbilityWithParams(ProductDetailsAbilityNew.class, params);
        TestUtils.sleep(2000);
        AbilitySlice slice = sAbilityDelegator.getCurrentAbilitySlice(ability);

        presenter = new MviPresenter(){
            @Override
            public void attachView(MvpView view) {
                activeFlag = true;
            }

            @Override
            public void detachView(boolean retainInstance) {

            }

            @Override
            public void detachView() {
                detachFlag = true;
            }

            @Override
            public void destroy() {

            }
        };

        mviDelegateCallback = new MviDelegateCallback() {
            @Override
            public MviPresenter createPresenter() {
                return presenter;
            }

            @Override
            public MvpView getMvpView() {
                return new MvpView() {
                    @Override
                    public int hashCode() {
                        return super.hashCode();
                    }
                };
            }

            @Override
            public void setRestoringViewState(boolean restoringViewState) {

            }
        };
        abilitySliceMviDelegate = new AbilitySliceMviDelegateImpl(mviDelegateCallback, slice);
    }

    @After
    public void tearDown() throws Exception {
        sAbilityDelegator.stopAbility(ability);
        EventHelper.clearAbilities();
        TestUtils.sleep(2000);
    }

    @Test
    public void onStart() {
        abilitySliceMviDelegate.onStart(null);
        assertNotNull(abilitySliceMviDelegate.getPresenter());
    }

    @Test
    public void onStop() {
        abilitySliceMviDelegate.onStop();
        assertNull(abilitySliceMviDelegate.getPresenter());
        TestUtils.sleep(2000);
    }

    @Test
    public void onActive() {
        activeFlag = false;
        abilitySliceMviDelegate.onStart(null);
        abilitySliceMviDelegate.onActive();
        assertTrue(activeFlag);
        assertNotNull(mviDelegateCallback.getMvpView());
        TestUtils.sleep(2000);
    }

    @Test
    public void onBackground() {
        detachFlag = false;
        abilitySliceMviDelegate.onStart(null);
        abilitySliceMviDelegate.onBackground();
        assertTrue(detachFlag);
        TestUtils.sleep(2000);
    }
}