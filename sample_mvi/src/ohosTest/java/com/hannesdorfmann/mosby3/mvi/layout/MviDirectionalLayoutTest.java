package com.hannesdorfmann.mosby3.mvi.layout;

import com.hannesdorfmann.mosby3.ComponentContainerMviDelegateImpl;
import com.hannesdorfmann.mosby3.mvi.MviPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.hannesdorfmann.mosby3.sample.mvi.EventHelper;
import com.hannesdorfmann.mosby3.sample.mvi.TestAbility;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Component;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MviDirectionalLayoutTest {

    Context context;
    MviDirectionalLayout mviDirectionalLayout;
    Ability ability;

    @Before
    public void setUp() throws Exception {

        ability = EventHelper.startAbility(TestAbility.class);
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();

        mviDirectionalLayout = new MviDirectionalLayout(ability) {
            @Override
            protected void onAttachedToWindowDo(Component component) {
            }

            @Override
            protected void onDetachedFromWindowDo(Component component) {
            }

            @Override
            public MviPresenter createPresenter() {
                return new MviPresenter(){

                    @Override
                    public void attachView(MvpView view) {
                    }

                    @Override
                    public void detachView(boolean retainInstance) {
                    }

                    @Override
                    public void detachView() {
                    }

                    @Override
                    public void destroy() {
                    }
                };
            }

            @Override
            public Context getmComponetContext() {
                return ability;
            }

            @Override
            public void setmComponetContext(Context context) {
            }

            @Override
            public void setRestoringViewState(boolean restoringViewState) {

            }
        };
        mviDirectionalLayout.setmComponetContext(ability);
        mviDirectionalLayout.getMviDelegate().onAttachedToWindow();

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getMviDelegate() {
        assertNotNull(mviDirectionalLayout.getMviDelegate());
        assertTrue(mviDirectionalLayout.getMviDelegate() instanceof ComponentContainerMviDelegateImpl);
    }

    @Test
    public void getMvpView() {
        assertNotNull(mviDirectionalLayout.getMvpView());
        assertTrue(mviDirectionalLayout.getMvpView() instanceof MvpView);
    }
}