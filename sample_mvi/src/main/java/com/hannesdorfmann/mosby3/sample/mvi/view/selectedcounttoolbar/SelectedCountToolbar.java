/*
 * Copyright 2017 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.selectedcounttoolbar;

import com.hannesdorfmann.mosby3.ComponentContainerMviDelegate;
import com.hannesdorfmann.mosby3.ComponentContainerMviDelegateCallback;
import com.hannesdorfmann.mosby3.ComponentContainerMviDelegateImpl;
import com.hannesdorfmann.mosby3.sample.mvi.SampleApplication;
import com.hannesdorfmann.mosby3.sample.mvi.ResourceTable;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.global.resource.Element;

/**
 * @author Hannes Dorfmann
 */
public class SelectedCountToolbar extends DirectionalLayout implements SelectedCountToolbarView,
        ComponentContainerMviDelegateCallback<SelectedCountToolbarView, SelectedCountToolbarPresenter> {

  Context context;
  Context mComponetContext;

  Image mIback,mIdel;
  Text mTtitle;

  private ComponentContainerMviDelegate<SelectedCountToolbarView, SelectedCountToolbarPresenter>
      mviDelegate = null;

  private final PublishSubject<Boolean> clearSelectionIntent = PublishSubject.create();
  private final PublishSubject<Boolean> deleteSelectedItemsIntent = PublishSubject.create();

  public SelectedCountToolbar(Context context){
    this(context, null);
  }

  public SelectedCountToolbar(Context context, AttrSet attrs) {
    this(context, attrs,"");
  }

  public SelectedCountToolbar(Context context, AttrSet attrs, String styleName) {
    super(context, attrs, styleName);
    this.context = context;
    initBindStateChangedListener();
    Component mComponent = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_shopping_cart_toolbar, null, false);
    addComponent(mComponent);
    initComponent();
  }

  private void initComponent(){
    mIback = (Image) findComponentById(ResourceTable.Id_mIback);
    mIdel = (Image) findComponentById(ResourceTable.Id_mIdel);
    mTtitle = (Text) findComponentById(ResourceTable.Id_mTtitle);

    mIdel.setClickedListener(new ClickedListener() {
      @Override
      public void onClick(Component component) {
        deleteSelectedItemsIntent.onNext(true);
      }
    });
    mIback.setClickedListener(new ClickedListener() {
      @Override
      public void onClick(Component component) {
        clearSelectionIntent.onNext(true);
      }
    });
    mTtitle.setText("");
  }

  @Override public Observable<Boolean> clearSelectionIntent() {
    return clearSelectionIntent;
  }

  @Override public Observable<Boolean> deleteSelectedItemsIntent() {
    return deleteSelectedItemsIntent;
  }

  @Override public void render(int selectedCount) {
    if (selectedCount == 0) {
      if (getVisibility() == Component.VISIBLE) {
        AnimatorProperty contentTranslateIn = createAnimatorProperty();
        contentTranslateIn.alpha(0f).setStateChangedListener(new Animator.StateChangedListener(){
          @Override
          public void onStart(Animator animator) {
          }

          @Override
          public void onStop(Animator animator) {
            setVisibility(Component.HIDE);
          }

          @Override
          public void onCancel(Animator animator) {
            setVisibility(Component.HIDE);
          }

          @Override
          public void onEnd(Animator animator) {
            setVisibility(Component.HIDE);
          }

          @Override
          public void onPause(Animator animator) {
          }

          @Override
          public void onResume(Animator animator) {
          }
        }).start();
      } else {
        setVisibility(Component.HIDE);
      }
    } else {
      Element pluralElement = null;
      try {
        pluralElement = getResourceManager().getElement(ResourceTable.Plural_items);
      } catch (Exception e) {
        e.printStackTrace();
      }

      String resTxt = "";
      if(pluralElement != null){
        try {
          resTxt = pluralElement.getPluralString(selectedCount, selectedCount);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }

      mTtitle.setText(resTxt);
      if (getVisibility() != Component.VISIBLE) {
        AnimatorProperty contentTranslateIn = createAnimatorProperty();
        contentTranslateIn.alpha(1f).setStateChangedListener(new Animator.StateChangedListener(){
          @Override
          public void onStart(Animator animator) {
          }

          @Override
          public void onStop(Animator animator) {
            setVisibility(Component.VISIBLE);
          }

          @Override
          public void onCancel(Animator animator) {
          }

          @Override
          public void onEnd(Animator animator) {
            setVisibility(Component.VISIBLE);
          }

          @Override
          public void onPause(Animator animator) {

          }

          @Override
          public void onResume(Animator animator) {
          }
        }).start();
      } else {
        setVisibility(Component.VISIBLE);
      }
    }
  }

  @Override public SelectedCountToolbarView getMvpView() {
    return this;
  }

  @Override public SelectedCountToolbarPresenter createPresenter() {
    return SampleApplication.getDependencyInjection(getmComponetContext())
            .newSelectedCountToolbarPresenter();
  }

  @Override public void setRestoringViewState(boolean restoringViewState) {
    // Don't needed for this view
  }

//  @Override
  public Context getmComponetContext() {
    return mComponetContext;
  }

//  @Override
  public void setmComponetContext(Context context) {
    mComponetContext = context;
  }


//  public abstract void onAttachedToWindowDo(Component component);
//  public abstract void onDetachedFromWindowDo(Component component);
//
  private void initBindStateChangedListener(){
    setBindStateChangedListener(new Component.BindStateChangedListener() {
      @Override
      public void onComponentBoundToWindow(Component component) {
        mviDelegate = new ComponentContainerMviDelegateImpl<SelectedCountToolbarView, SelectedCountToolbarPresenter>(SelectedCountToolbar.this, SelectedCountToolbar.this, true);
        mviDelegate.onAttachedToWindow();
//        onAttachedToWindowDo(component);
      }

      @Override
      public void onComponentUnboundFromWindow(Component component) {
        mviDelegate.onDetachedFromWindow();
//        onDetachedFromWindowDo(component);
      }
    });
  }

}
