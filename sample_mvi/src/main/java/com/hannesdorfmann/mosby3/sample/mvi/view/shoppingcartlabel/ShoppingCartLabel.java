/*
 *Copyright 2017 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.shoppingcartlabel;


import io.reactivex.Observable;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.global.resource.Element;

import com.hannesdorfmann.mosby3.ComponentContainerMviDelegate;
import com.hannesdorfmann.mosby3.ComponentContainerMviDelegateCallback;
import com.hannesdorfmann.mosby3.ComponentContainerMviDelegateImpl;
import com.hannesdorfmann.mosby3.sample.mvi.SampleApplication;
import com.hannesdorfmann.mosby3.sample.mvi.ResourceTable;

/**
 * A UI widget that displays how many items are in the shopping cart
 *
 * @author Hannes Dorfmann
 */
public class ShoppingCartLabel extends Button implements ShoppingCartLabelView,
        ComponentContainerMviDelegateCallback<ShoppingCartLabelView, ShoppingCartLabelPresenter> {

    Context mComponetContext;

    private ComponentContainerMviDelegate<ShoppingCartLabelView, ShoppingCartLabelPresenter>
            mviDelegate = null;//new ComponentContainerMviDelegateImpl<>(this, this, true);

    public ShoppingCartLabel(Context context, AttrSet attrs) {
        super(context, attrs);
        initBindStateChangedListener();
    }

    @Override
    public ShoppingCartLabelView getMvpView() {
        return this;
    }

    @Override
    public ShoppingCartLabelPresenter createPresenter() {
        return SampleApplication.getDependencyInjection(getContext()).newShoppingCartLabelPresenter();
    }

    @Override
    public Context getmComponetContext() {
        return mComponetContext;
    }

    @Override
    public void setmComponetContext(Context context) {
        mComponetContext = context;
    }


    @Override
    public Observable<Boolean> loadIntent() {
        return Observable.just(true);
    }

    @Override
    public void render(int itemsInShoppingCart) {
        Element pluralElement = null;
        try {
            pluralElement = getResourceManager().getElement(ResourceTable.Plural_items);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String resTxt = "";
        if (pluralElement != null) {
            try {
                resTxt = pluralElement.getPluralString(itemsInShoppingCart, itemsInShoppingCart);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        setText(resTxt);
    }


    @Override
    public void setRestoringViewState(boolean restoringViewState) {
        // Not needed for this view
    }

    private void initBindStateChangedListener() {
        setBindStateChangedListener(new BindStateChangedListener() {

            @Override
            public void onComponentBoundToWindow(Component component) {
                mviDelegate = mviDelegate = new ComponentContainerMviDelegateImpl<ShoppingCartLabelView, ShoppingCartLabelPresenter>(ShoppingCartLabel.this, ShoppingCartLabel.this, true);
                mviDelegate.onAttachedToWindow();
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {
                if (mviDelegate != null)
                    mviDelegate.onDetachedFromWindow();
            }
        });
    }


}
