/*
 * Copyright 2016 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.menu;

import com.hannesdorfmann.mosby3.sample.mvi.businesslogic.model.MainMenuItem;
import com.hannesdorfmann.mosby3.sample.mvi.view.ui.viewholder.MainMenuViewHolder;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import ohos.agp.components.*;

import java.util.List;

/**
 * @author Hannes Dorfmann
 */
public class MainMenuAdapter extends BaseItemProvider implements MainMenuViewHolder.MainMenuSelectionListener{
  private final LayoutScatter layoutInflater;
  private List<MainMenuItem> items;
  private final PublishSubject<String> selectedItem = PublishSubject.create();
  public MainMenuAdapter(LayoutScatter layoutInflater) {
    this.layoutInflater = layoutInflater;
  }

  @Override public void onItemSelected(String categoryName) {
    selectedItem.onNext(categoryName);
  }

  public Observable<String> getSelectedItemObservable() {
    return selectedItem;
  }

  public void setItems(List<MainMenuItem> items) {
    this.items = items;
  }

  @Override
  public int getCount() {
    return items == null ? 0 : items.size();
  }

  @Override
  public MainMenuItem getItem(int i) {
    return items == null ? null : items.get(i);
  }

  @Override
  public long getItemId(int i) {
    return i;
  }

  @Override
  public Component getComponent(int i, Component component, ComponentContainer componentContainer) {

    MainMenuViewHolder mMainMenuViewHolder = MainMenuViewHolder.create(layoutInflater, this);
    if(items != null && items.size()>0)
        mMainMenuViewHolder.bind(items.get(i));
    return mMainMenuViewHolder.getItemView();
  }
}
