/**
 * Copyright 2016 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.ui;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.app.Context;

/** An ImageView that keeps asp
 *
 * @author Hannes Dorfmann
 */
public class AspectRatioImageView extends Image {

  public AspectRatioImageView(Context context) {
    super(context);
  }

  public AspectRatioImageView(Context context, AttrSet attrs) {
    super(context, attrs);
  }

  public AspectRatioImageView(Context context, AttrSet attrs, String styleName) {
    super(context, attrs, styleName);
  }


//  @Override
  //TODO 待处理
  public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec){

    return false;
  }

//  @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//    int measuredHeight = ((int) (((double) getMeasuredWidth()) / 0.68636363));
//    setMeasuredDimension(getMeasuredWidth(), measuredHeight);
//  }
}
