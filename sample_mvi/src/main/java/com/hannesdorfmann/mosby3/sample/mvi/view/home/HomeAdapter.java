/*
 * Copyright 2016 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.home;


import com.hannesdorfmann.mosby3.sample.mvi.businesslogic.model.AdditionalItemsLoadable;
import com.hannesdorfmann.mosby3.sample.mvi.businesslogic.model.FeedItem;
import com.hannesdorfmann.mosby3.sample.mvi.businesslogic.model.Product;
import com.hannesdorfmann.mosby3.sample.mvi.businesslogic.model.SectionHeader;
import com.hannesdorfmann.mosby3.sample.mvi.view.ui.viewholder.*;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import ohos.agp.components.*;

import java.util.List;

/**
 * @author Hannes Dorfmann
 */

public class HomeAdapter extends BaseItemProvider
    implements MoreItemsViewHolder.LoadItemsClickListener {

  static final int VIEW_TYPE_PRODUCT = 0;
  static final int VIEW_TYPE_LOADING_MORE_NEXT_PAGE = 1;
  static final int VIEW_TYPE_SECTION_HEADER = 2;
  static final int VIEW_TYPE_MORE_ITEMS_AVAILABLE = 3;

  private boolean isLoadingNextPage = false;
  private List<FeedItem> items;
  private final LayoutScatter layoutInflater;
  private final ProductViewHolder.ProductClickedListener productClickedListener;

  private PublishSubject<String> loadMoreItemsOfCategoryObservable = PublishSubject.create();

  public HomeAdapter(LayoutScatter layoutInflater,
      ProductViewHolder.ProductClickedListener productClickedListener) {
    this.layoutInflater = layoutInflater;
    this.productClickedListener = productClickedListener;
  }

  public List<FeedItem> getItems() {
    return items;
  }

  /**
   * @return true if value has changed since last invocation
   */
  public boolean setLoadingNextPage(boolean loadingNextPage) {
    boolean hasLoadingMoreChanged = loadingNextPage != isLoadingNextPage;
    boolean notifyInserted = loadingNextPage && hasLoadingMoreChanged;
    boolean notifyRemoved = !loadingNextPage && hasLoadingMoreChanged;
    isLoadingNextPage = loadingNextPage;
    if (notifyInserted) {
      notifyDataSetItemInserted(items.size());

    } else if (notifyRemoved) {
      notifyDataSetItemRemoved(items.size());
    }

    return hasLoadingMoreChanged;
  }

  public boolean isLoadingNextPage() {
    return isLoadingNextPage;
  }

  public void setItems(List<FeedItem> newItems) {
    List<FeedItem> oldItems = this.items;
    this.items = newItems;

    if (oldItems == null) {
      notifyDataChanged();
    } else {
      notifyDataChanged();
//      // Use Diff utils

    }
  }

  @Override public void loadItemsForCategory(String category) {
    loadMoreItemsOfCategoryObservable.onNext(category);
  }

  public Observable<String> loadMoreItemsOfCategoryObservable() {
    return loadMoreItemsOfCategoryObservable;
  }

  @Override
  public int getCount() {
    return items == null ? 0 : (items.size() + (isLoadingNextPage ? 1 : 0));
  }

  @Override
  public FeedItem getItem(int i) {
    return items == null ? null: items.get(i);
  }

  @Override
  public long getItemId(int i) {
    return i;
  }

  @Override
  public Component getComponent(int position, Component convertComponent, ComponentContainer parent) {

    return onCreateViewHolderAndGetComponent(convertComponent, parent, getItemComponentType(position), position);
  }

  @Override
  public int getItemComponentType(int position) {
    super.getItemComponentType(position);
    if (isLoadingNextPage && position == items.size()) {
      return VIEW_TYPE_LOADING_MORE_NEXT_PAGE;
    }
    FeedItem item = items.get(position);

    if (item instanceof Product) {
      return VIEW_TYPE_PRODUCT;
    } else if (item instanceof SectionHeader) {
      return VIEW_TYPE_SECTION_HEADER;
    } else if (item instanceof AdditionalItemsLoadable) {
      return VIEW_TYPE_MORE_ITEMS_AVAILABLE;
    }

    throw new IllegalArgumentException("Not able to dertermine the view type for item at position "
            + position
            + ". Item is: "
            + item);

  }

  public Component onCreateViewHolderAndGetComponent(Component convertComponent, ComponentContainer parent, int viewType, int position) {

    Component component = convertComponent;
    switch (viewType) {
      case VIEW_TYPE_PRODUCT:
        ProductViewHolder mProductViewHolder = null;
        if(component == null){
          mProductViewHolder = ProductViewHolder.create(layoutInflater, productClickedListener);
          component = mProductViewHolder.getItemView();
        }else{
          Object cls = component.getTag();
          if(cls instanceof ProductViewHolder){
            mProductViewHolder = (ProductViewHolder) cls;
          }else{
            mProductViewHolder = ProductViewHolder.create(layoutInflater, productClickedListener);
            component = mProductViewHolder.getItemView();
          }
        }

        onBindViewHolder(mProductViewHolder, position);
        return component;
      case VIEW_TYPE_LOADING_MORE_NEXT_PAGE:
        LoadingViewHolder mLoadingViewHolder = null;
        if(component == null){
          mLoadingViewHolder = LoadingViewHolder.create(layoutInflater);
          component = mLoadingViewHolder.getItemView();
        }else{
          Object cls = component.getTag();
          if(cls instanceof LoadingViewHolder){
            mLoadingViewHolder = (LoadingViewHolder) cls;
          }else{
            mLoadingViewHolder = LoadingViewHolder.create(layoutInflater);
            component = mLoadingViewHolder.getItemView();
          }
        }

        onBindViewHolder(mLoadingViewHolder, position);
        return component;
      case VIEW_TYPE_MORE_ITEMS_AVAILABLE:
        MoreItemsViewHolder mMoreItemsViewHolder = null;
        if(component == null) {
          mMoreItemsViewHolder = MoreItemsViewHolder.create(layoutInflater, this);
          component =  mMoreItemsViewHolder.getItemView();
        }else{
          Object cls = component.getTag();
          if(cls instanceof MoreItemsViewHolder){
            mMoreItemsViewHolder = (MoreItemsViewHolder) cls;
          }else{
            mMoreItemsViewHolder = MoreItemsViewHolder.create(layoutInflater, this);
            component =  mMoreItemsViewHolder.getItemView();
          }
        }
        onBindViewHolder(mMoreItemsViewHolder, position);
        return component;
      case VIEW_TYPE_SECTION_HEADER:
        SectionHederViewHolder mSectionHederViewHolder = null;
        if(component == null) {
          mSectionHederViewHolder = SectionHederViewHolder.create(layoutInflater);
          component = mSectionHederViewHolder.getItemView();
        }else{
          Object cls = component.getTag();
          if(cls instanceof SectionHederViewHolder){
            mSectionHederViewHolder = (SectionHederViewHolder) cls;
          }else{
            mSectionHederViewHolder = SectionHederViewHolder.create(layoutInflater);
            component = mSectionHederViewHolder.getItemView();
          }
        }
        onBindViewHolder(mSectionHederViewHolder, position);
        return component;
    }

    throw new IllegalArgumentException("Couldn't create a ViewHolder for viewType  = " + viewType);
  }

  public ViewHolder onCreateViewHolder(ComponentContainer parent, int viewType) {
    switch (viewType) {
      case VIEW_TYPE_PRODUCT:
        return ProductViewHolder.create(layoutInflater, productClickedListener);
      case VIEW_TYPE_LOADING_MORE_NEXT_PAGE:
        return LoadingViewHolder.create(layoutInflater);
      case VIEW_TYPE_MORE_ITEMS_AVAILABLE:
        return MoreItemsViewHolder.create(layoutInflater, this);
      case VIEW_TYPE_SECTION_HEADER:
        return SectionHederViewHolder.create(layoutInflater);
    }

    throw new IllegalArgumentException("Couldn't create a ViewHolder for viewType  = " + viewType);
  }

  public void onBindViewHolder(ViewHolder holder, int position) {
    if (holder instanceof LoadingViewHolder) {
      return;
    }
    FeedItem item = items.get(position);
    if (holder instanceof ProductViewHolder) {
      ((ProductViewHolder) holder).bind((Product) item);
    } else if (holder instanceof SectionHederViewHolder) {
      ((SectionHederViewHolder) holder).onBind((SectionHeader) item);
    } else if (holder instanceof MoreItemsViewHolder) {
      ((MoreItemsViewHolder) holder).bind((AdditionalItemsLoadable) item);
    } else {
      throw new IllegalArgumentException("couldn't accept  ViewHolder " + holder);
    }
  }
}
