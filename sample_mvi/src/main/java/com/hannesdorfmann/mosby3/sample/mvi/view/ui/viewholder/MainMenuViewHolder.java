/**
 * Copyright 2016 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.ui.viewholder;


import com.hannesdorfmann.mosby3.sample.mvi.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvi.businesslogic.model.MainMenuItem;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

/**
 * Simple ViewHolder to display a list of main menu items
 *
 * @author Hannes Dorfmann
 */
public class MainMenuViewHolder implements ViewHolder {

  Text name;
  public Component itemView;

  public interface MainMenuSelectionListener {
    public void onItemSelected(String categoryName);
  }

  public static MainMenuViewHolder create(LayoutScatter inflater,
                                          MainMenuSelectionListener listener) {
    return new MainMenuViewHolder(inflater.parse(ResourceTable.Layout_item_main_menu, null, false), listener);
  }

  public Component getItemView() {
    return itemView;
  }

  private MainMenuViewHolder(Component itemView, MainMenuSelectionListener listener) {

    this.itemView = itemView;
    name = (Text) itemView.findComponentById(ResourceTable.Id_name);
    itemView.setClickedListener(new Component.ClickedListener() {
      @Override
      public void onClick(Component component) {
        listener.onItemSelected(name.getText().toString());
      }
    });
  }

  public void bind(MainMenuItem item) {
    name.setText(item.getName());
  }
}
