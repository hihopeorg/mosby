/**
 * Copyright 2016 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.ui.viewholder;


import com.hannesdorfmann.mosby3.sample.mvi.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvi.businesslogic.model.AdditionalItemsLoadable;
import com.hannesdorfmann.mosby3.sample.mvi.view.widge.CirleProgressImg;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

/**
 * @author Hannes Dorfmann
 */
public class MoreItemsViewHolder implements ViewHolder {

    Text moreItemsCount;
    CirleProgressImg loadingView;
    Button loadMoreButton;
    Button errorRetry;
    public Component itemView;

    public interface LoadItemsClickListener {
        public void loadItemsForCategory(String category);
    }

    public Component getItemView() {
        return itemView;
    }

    public static MoreItemsViewHolder create(LayoutScatter layoutInflater,
                                             LoadItemsClickListener clickListener) {
        return new MoreItemsViewHolder(
                layoutInflater.parse(ResourceTable.Layout_item_more_available, null, false), clickListener);
    }


    private AdditionalItemsLoadable currentItem;

    private MoreItemsViewHolder(Component itemView, LoadItemsClickListener listener) {

        this.itemView = itemView;
        this.itemView.setTag(this);
        moreItemsCount = (Text) itemView.findComponentById(ResourceTable.Id_moreItemsCount);
        loadingView = (CirleProgressImg) itemView.findComponentById(ResourceTable.Id_loadingView);
        loadMoreButton = (Button) itemView.findComponentById(ResourceTable.Id_loadMoreButtton);
        errorRetry = (Button) itemView.findComponentById(ResourceTable.Id_errorRetryButton);
        itemView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                listener.loadItemsForCategory(currentItem.getCategoryName());
            }
        });
        errorRetry.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                listener.loadItemsForCategory(currentItem.getCategoryName());
            }
        });
        loadMoreButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                listener.loadItemsForCategory(currentItem.getCategoryName());
            }
        });

    }

    public void bind(AdditionalItemsLoadable item) {
        this.currentItem = item;
        if (item.isLoading()) {
            // TransitionManager.beginDelayedTransition((ViewGroup) itemView);
            moreItemsCount.setVisibility(Component.HIDE);
            loadMoreButton.setVisibility(Component.HIDE);
            loadingView.setVisibility(Component.VISIBLE);
            errorRetry.setVisibility(Component.HIDE);
            itemView.setClickable(false);
        } else if (item.getLoadingError() != null) {
            //TransitionManager.beginDelayedTransition((ViewGroup) itemView);
            moreItemsCount.setVisibility(Component.HIDE);
            loadMoreButton.setVisibility(Component.HIDE);
            loadingView.setVisibility(Component.HIDE);
            errorRetry.setVisibility(Component.VISIBLE);
            itemView.setClickable(true);
        } else {
            moreItemsCount.setText("+" + item.getMoreItemsCount());
            moreItemsCount.setVisibility(Component.VISIBLE);
            loadMoreButton.setVisibility(Component.VISIBLE);
            loadingView.setVisibility(Component.HIDE);
            errorRetry.setVisibility(Component.HIDE);
            itemView.setClickable(true);
        }
    }
}
