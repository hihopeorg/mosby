/*
 * Copyright 2017 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.checkoutbutton;

import com.hannesdorfmann.mosby3.ComponentContainerMviDelegate;
import com.hannesdorfmann.mosby3.ComponentContainerMviDelegateCallback;
import com.hannesdorfmann.mosby3.ComponentContainerMviDelegateImpl;
import com.hannesdorfmann.mosby3.sample.mvi.SampleApplication;
import io.reactivex.Observable;
import java.util.Locale;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * @author Hannes Dorfmann
 */
public class CheckoutButton extends Button implements CheckoutButtonView,
        ComponentContainerMviDelegateCallback<CheckoutButtonView, CheckoutButtonPresenter> {

  protected Context mComponentContext;

  private ComponentContainerMviDelegate<CheckoutButtonView, CheckoutButtonPresenter> mviDelegate = null;

  public CheckoutButton(Context context, AttrSet attrs) {
    super(context, attrs);
    initBindStateChangedListener();
    setClickedListener(new ClickedListener() {
      @Override
      public void onClick(Component component) {
        ToastDialog mToastDialog = new ToastDialog(context);
        mToastDialog.setText("This is just a demo app. You can't purchase any items.");
        mToastDialog.setDuration(1000);
        mToastDialog.show();
      }
    });

  }


  @Override public CheckoutButtonView getMvpView() {
    return this;
  }

  @Override public CheckoutButtonPresenter createPresenter() {
    return SampleApplication.getDependencyInjection(getContext()).newCheckoutButtonPresenter();
  }

  @Override
  public Context getmComponetContext() {
    return mComponentContext;
  }

  @Override
  public void setmComponetContext(Context context) {
    mComponentContext = context;
  }

  @Override public Observable<Boolean> loadIntent() {
    return Observable.just(true);
  }

  @Override public void render(double priceSum) {
    // TODO move to strings.xml / internationalization

    String priceString = String.format(Locale.US, "%.2f", priceSum);
    if (priceSum == 0) {
      setVisibility(Component.INVISIBLE);
    } else {
      setText("Checkout $ " + priceString);
      setVisibility(Component.VISIBLE);
    }
  }

  private void initBindStateChangedListener(){
    setBindStateChangedListener(new BindStateChangedListener() {
      Ability ability;
      AbilitySlice slice;
      @Override
      public void onComponentBoundToWindow(Component component) {
        mviDelegate = new ComponentContainerMviDelegateImpl<CheckoutButtonView, CheckoutButtonPresenter>(CheckoutButton.this, CheckoutButton.this, true);
        mviDelegate.onAttachedToWindow();
      }

      @Override
      public void onComponentUnboundFromWindow(Component component) {
        if(mviDelegate != null)
        mviDelegate.onDetachedFromWindow();
      }
    });
  }

  @Override public void setRestoringViewState(boolean restoringViewState) {
    // Not needed for this view
  }
}
