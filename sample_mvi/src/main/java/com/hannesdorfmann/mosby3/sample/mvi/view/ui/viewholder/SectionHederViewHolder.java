/**
 * Copyright 2016 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.ui.viewholder;


import com.hannesdorfmann.mosby3.sample.mvi.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvi.businesslogic.model.SectionHeader;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

/**
 * @author Hannes Dorfmann
 */
public class SectionHederViewHolder implements ViewHolder{

  Text sectionName;
  public Component itemView;

  public Component getItemView(){
    return itemView;
  }

  public static SectionHederViewHolder create(LayoutScatter layoutInflater) {
    return new SectionHederViewHolder(
        layoutInflater.parse(ResourceTable.Layout_item_section_header, null, false));
  }



  private SectionHederViewHolder(Component itemView) {
    this.itemView = itemView;
    this.itemView.setTag(this);
    sectionName = (Text) itemView.findComponentById(ResourceTable.Id_sectionName);
  }

  public void onBind(SectionHeader item) {
    sectionName.setText(item.getName());
  }
}
