/**
 * Copyright 2016 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.ui.viewholder;

import com.hannesdorfmann.mosby3.sample.mvi.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvi.businesslogic.model.Product;
import com.hannesdorfmann.mosby3.sample.mvi.dependencyinjection.DependencyInjection;
import com.hannesdorfmann.mosby3.sample.mvi.util.ImageUtils;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

/**
 * View Holder just to display
 *
 * @author Hannes Dorfmann
 */
public class ProductViewHolder implements ViewHolder{

  public Image image;
  public Text name;
  public Component itemView;

  private Product product;

  public Component getItemView(){
    return itemView;
  }

  public interface ProductClickedListener {
    void onProductClicked(Product product);
  }

  public static ProductViewHolder create(LayoutScatter inflater, ProductClickedListener listener) {
    return new ProductViewHolder(inflater.parse(ResourceTable.Layout_item_product, null, false), listener);
  }

  private ProductViewHolder(Component itemView, ProductClickedListener clickedListener) {
    this.itemView = itemView;
    this.itemView.setTag(this);
    name = (Text) itemView.findComponentById(ResourceTable.Id_productName);
    image = (Image) itemView.findComponentById(ResourceTable.Id_productImage);
    itemView.setClickedListener(new Component.ClickedListener() {
      @Override
      public void onClick(Component component) {
        clickedListener.onProductClicked(product);
      }
    });
  }

  public void bind(Product product) {

    this.product = product;
      ImageUtils.load(itemView.getContext(), image, DependencyInjection.BASE_IMAGE_URL + product.getImage());

    name.setText(product.getName());
  }
}
