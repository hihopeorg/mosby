/*
 * Copyright 2016 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.menu;

import com.hannesdorfmann.mosby3.mvi.layout.MviStackLayout;
import com.hannesdorfmann.mosby3.sample.mvi.SampleApplication;
import com.hannesdorfmann.mosby3.sample.mvi.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvi.view.widge.CirleProgressImg;
import io.reactivex.Observable;
import ohos.agp.components.*;
import ohos.app.Context;

/**
 * @author Hannes Dorfmann
 */
public class MainMenuLayout extends MviStackLayout<MainMenuView, MainMenuPresenter>
    implements MainMenuView {

  private final MainMenuAdapter adapter;
  CirleProgressImg loadingView;
  ListContainer listContainer;
  Text errorView;

  public MainMenuLayout(Context context, AttrSet attrs) {
    super(context, attrs);
    LayoutScatter factory = LayoutScatter.getInstance(context);
    StackLayout root = (StackLayout) factory.parse(ResourceTable.Layout_view_mainmenu, this, true);
    root.setClickedListener(new ClickedListener() {
      @Override
      public void onClick(Component component) {
        setVisibility(HIDE);
      }
    });

    loadingView = (CirleProgressImg) root.findComponentById(ResourceTable.Id_loadingView);
    listContainer = (ListContainer) root.findComponentById(ResourceTable.Id_listContainer);
    errorView = (Text) root.findComponentById(ResourceTable.Id_errorView);

    adapter = new MainMenuAdapter(LayoutScatter.getInstance(context));
    listContainer.setItemProvider(adapter);
  }

  @Override
  protected void onAttachedToWindowDo(Component component) {

  }

  @Override
  protected void onDetachedFromWindowDo(Component component) {

  }

  @Override public MainMenuPresenter createPresenter() {
    return SampleApplication.getDependencyInjection(getContext()).getMainMenuPresenter();
  }

  @Override public Observable<Boolean> loadCategoriesIntent() {
    return Observable.just(true);
  }

  @Override public Observable<String> selectCategoryIntent() {
    return adapter.getSelectedItemObservable();
  }

  @Override public void render(MenuViewState menuViewState) {
    if (menuViewState instanceof MenuViewState.LoadingState) {
      loadingView.setVisibility(Component.VISIBLE);
      listContainer.setVisibility(Component.HIDE);
      errorView.setVisibility(Component.HIDE);
    } else if (menuViewState instanceof MenuViewState.DataState) {
      adapter.setItems(((MenuViewState.DataState) menuViewState).getCategories());
      adapter.notifyDataChanged();
      loadingView.setVisibility(Component.HIDE);
      listContainer.setVisibility(Component.VISIBLE);
      errorView.setVisibility(Component.HIDE);
    } else if (menuViewState instanceof MenuViewState.ErrorState) {
      loadingView.setVisibility(Component.HIDE);
      listContainer.setVisibility(Component.HIDE);
      errorView.setVisibility(Component.VISIBLE);
    } else {
      throw new IllegalStateException("Unknown state " + menuViewState);
    }
  }
}
