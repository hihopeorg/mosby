/**
 * Copyright 2017 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.ui.viewholder;


import com.hannesdorfmann.mosby3.sample.mvi.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvi.businesslogic.model.Product;
import com.hannesdorfmann.mosby3.sample.mvi.dependencyinjection.DependencyInjection;
import com.hannesdorfmann.mosby3.sample.mvi.util.ImageUtils;
import com.hannesdorfmann.mosby3.sample.mvi.view.shoppingcartoverview.ShoppingCartOverviewItem;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.global.resource.ResourceManager;

import java.io.IOException;
import java.util.Locale;

/**
 * @author Hannes Dorfmann
 */

public class ShoppingCartItemViewHolder implements ViewHolder{

  Image image;
  Text name;
  Text price;
  public Component itemView;

  private final ItemSelectedListener selectedListener;
  private ShoppingCartOverviewItem item;
  private Element selectedDrawable;

  ResourceManager mResourceManager;


  public Component getItemView(){
    return itemView;
  }

  public interface ItemSelectedListener {
    public void onItemClicked(ShoppingCartOverviewItem product);

    public boolean onItemLongPressed(ShoppingCartOverviewItem product);
  }

  public static ShoppingCartItemViewHolder create(LayoutScatter inflater,
                                                  ItemSelectedListener selectedListener) {
    return new ShoppingCartItemViewHolder(
        inflater.parse(ResourceTable.Layout_item_shopping_cart, null, false), selectedListener);
  }


  private ShoppingCartItemViewHolder(Component itemView, ItemSelectedListener itemSelectedListener) {

    this.itemView = itemView;
    image = (Image) itemView.findComponentById(ResourceTable.Id_image);
    name = (Text) itemView.findComponentById(ResourceTable.Id_name);
    price = (Text) itemView.findComponentById(ResourceTable.Id_price);

    this.selectedListener = itemSelectedListener;
    itemView.setClickedListener(new Component.ClickedListener() {
      @Override
      public void onClick(Component component) {
        selectedListener.onItemClicked(item);
      }
    });
    itemView.setLongClickedListener(new Component.LongClickedListener() {
      @Override
      public void onLongClicked(Component component) {
        selectedListener.onItemLongPressed(item);
      }
    });

//    if(mResourceManager == null){
//      mResourceManager = itemView.getContext().getResourceManager();
//    }

//    selectedDrawable = new VectorElement(itemView.getContext(),  ResourceTable.Color_selected_shopping_cart_item);
//    itemView.setBackground(selectedDrawable);
    /*if (item.isSelected()) {
      Resource bgResource = null;
      try {
        //获取Media文件夹中的图片资源
        bgResource = itemView.getContext().getResourceManager().getResource(ResourceTable.Color_selected_shopping_cart_item);
      } catch (Exception e) {
        e.printStackTrace();
      }
      //根据资源生成PixelMapElement实例
      PixelMapElement pixBg=new PixelMapElement(bgResource);
      itemView.setBackground(selectedDrawable);
    } else {
      itemView.setBackground(null);
    }*/

//    image.setBackground(new VectorElement(context, ResourceTable.Graphic_ic_icon_moon));
//    Resource bgResource = null;
//    try {
//      //获取Media文件夹中的图片资源
//      bgResource = mResourceManager.getResource(ResourceTable.Color_selected_shopping_cart_item);
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//    //根据资源生成PixelMapElement实例
//    PixelMapElement pixBg=new PixelMapElement(bgResource);
//    if(pixBg != null){
//      itemView.setBackground(pixBg/*selectedDrawable*/);
//    }

  }

  public void bind(ShoppingCartOverviewItem item) {
    this.item = item;
    Product product = item.getProduct();

    ImageUtils.load(itemView.getContext(), image, DependencyInjection.BASE_IMAGE_URL + product.getImage());
//    Glide.with(itemView.getContext())
//        .load(DependencyInjection.BASE_IMAGE_URL + product.getImage())
//        .centerCrop()
//        .into(image);

    name.setText(product.getName());
    price.setText(String.format(Locale.US, "$ %.2f", product.getPrice()));

    /*if(mResourceManager == null){
      mResourceManager = itemView.getContext().getResourceManager();
    }*/

    /*if (item.isSelected()) {
      Resource bgResource = null;
      try {
        //获取Media文件夹中的图片资源
        bgResource = mResourceManager.getResource(ResourceTable.Color_selected_shopping_cart_item);
      } catch (Exception e) {
        e.printStackTrace();
      }
      //根据资源生成PixelMapElement实例
      PixelMapElement pixBg=new PixelMapElement(bgResource);
        itemView.setBackground(selectedDrawable);
    } else {
        itemView.setBackground(null);
    }*/

//    if (item.isSelected()) {
//      if (Build.VERSION.SDK_INT >= 23) {
//        itemView.setForeground(selectedDrawable);
//      } else {
//        itemView.setBackground(selectedDrawable);
//      }
//    } else {
//      if (Build.VERSION.SDK_INT >= 23) {
//        itemView.setForeground(null);
//      } else {
//        itemView.setBackground(null);
//      }
//    }
  }
}
