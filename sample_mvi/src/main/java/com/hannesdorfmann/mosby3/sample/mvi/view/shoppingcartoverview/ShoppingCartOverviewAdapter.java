/*
 * Copyright 2017 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.shoppingcartoverview;

import com.hannesdorfmann.mosby3.sample.mvi.businesslogic.model.Product;
import com.hannesdorfmann.mosby3.sample.mvi.view.detail.ProductDetailsAbility;
import com.hannesdorfmann.mosby3.sample.mvi.view.ui.viewholder.ShoppingCartItemViewHolder;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import java.util.ArrayList;
import java.util.List;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;

/**
 * @author Hannes Dorfmann
 */

public class ShoppingCartOverviewAdapter extends BaseItemProvider
    implements ShoppingCartItemViewHolder.ItemSelectedListener {

  private final LayoutScatter layoutInflater;
  private final Ability activity;
  private List<ShoppingCartOverviewItem> items;
  private PublishSubject<List<Product>> selectedProducts = PublishSubject.create();

  public ShoppingCartOverviewAdapter(Ability activity) {
    this.activity = activity;
    this.layoutInflater = LayoutScatter.getInstance(activity);
  }


  public boolean isInSelectionMode() {
    for (ShoppingCartOverviewItem item : items) {
      if (item.isSelected()) return true;
    }

    return false;
  }

  @Override public void onItemClicked(ShoppingCartOverviewItem product) {
    if (isInSelectionMode()) {
      toggleSelection(product);
    } else {
      ProductDetailsAbility.start(activity, product.getProduct());
    }
  }

  @Override public boolean onItemLongPressed(ShoppingCartOverviewItem product) {
    toggleSelection(product);
    return true;
  }

  public void setItems(List<ShoppingCartOverviewItem> items) {
    List<ShoppingCartOverviewItem> beforeItems = this.items;
    this.items = items;
    notifyDataChanged();
  }

  private void toggleSelection(ShoppingCartOverviewItem toToggle) {
    List<Product> selectedItems = new ArrayList<>();
    for (ShoppingCartOverviewItem item : items) {

      if (item.equals(toToggle)) {
        if (!toToggle.isSelected()) {
          selectedItems.add(item.getProduct());
        }
      } else if (item.isSelected()) {
        selectedItems.add(item.getProduct());
      }
    }

    selectedProducts.onNext(selectedItems);
  }

  public Observable<List<Product>> selectedItemsObservable() {
    return selectedProducts.doOnNext(selected -> System.err.println("ShoppingCartOverviewAdapter selected %s "+ selected));
  }

  public Product getProductAt(int position) {
    return items.get(position).getProduct();
  }

  @Override
  public int getCount() {
    return items == null ? 0 : items.size();
  }

  @Override
  public ShoppingCartOverviewItem getItem(int i) {
    return items == null ? null : items.get(i);
  }

  @Override
  public long getItemId(int i) {
    return i;
  }

  @Override
  public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
    ShoppingCartItemViewHolder mShoppingCartItemViewHolder = ShoppingCartItemViewHolder.create(layoutInflater, this);
    if(items != null && items.size()>0)
      mShoppingCartItemViewHolder.bind(items.get(i));
    return mShoppingCartItemViewHolder.getItemView();
  }

  public ShoppingCartItemViewHolder onCreateViewHolder(ComponentContainer parent, int viewType) {
    return ShoppingCartItemViewHolder.create(layoutInflater, this);
  }

  public void onBindViewHolder(ShoppingCartItemViewHolder holder, int position) {
    holder.bind(items.get(position));
  }

}
