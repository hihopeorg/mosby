/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.util;

import ohos.agp.components.Image;
import ohos.app.Context;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class ImageUtils {
    public static void load(Context context, Image image, String urlImage){
        if(context == null){
            return;
        }
        if(image == null){
            return;
        }
        if(urlImage == null || urlImage.length() == 0){
            return;
        }

        TaskDispatcher refreshUITask = context.createParallelTaskDispatcher("", TaskPriority.DEFAULT);
        refreshUITask.asyncDispatch(new Runnable() {
            @Override
            public void run() {
                HttpURLConnection connection = null;
                try {
                    URL url = new URL(urlImage);
                    URLConnection urlConnection =   url.openConnection();
                    if (urlConnection instanceof   HttpURLConnection) {
                        connection =   (HttpURLConnection) urlConnection;
                    }

                    if (connection != null) {
                        connection.connect();
                        // 之后可进行url的其他操作
                        // 得到服务器返回过来的流对象

                        InputStream inputStream =   urlConnection.getInputStream();
                        ImageSource imageSource = ImageSource.create(inputStream,   new ImageSource.SourceOptions());
                        ImageSource.DecodingOptions   decodingOptions = new ImageSource.DecodingOptions();
                        decodingOptions.desiredPixelFormat   = PixelFormat.ARGB_8888;

                        // 普通解码叠加旋转、缩放、裁剪
                        PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
                        // 普通解码

                        context.getUITaskDispatcher().syncDispatch(()   -> {
                            image.setPixelMap(pixelMap);
                            pixelMap.release();
                        });

                    }
                }   catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

//        refreshUITask.asyncDispatch(()->{
//            PixelMap pixelMap = ImageReadUtil.createPixelMap(inputUrl);
//            //Image组件填充位图数据，UI界面更新
//            Image image = (Image) findComponentById(ResourceTable.Id_net_image_example);
//            image.setPixelMap(pixelMap);
//        });

//        HttpURLConnection connection = null;
//        try {
//            URL url = new URL(urlImage);
//            URLConnection urlConnection =   url.openConnection();
//            if (urlConnection instanceof   HttpURLConnection) {
//                connection =   (HttpURLConnection) urlConnection;
//            }
//
//            if (connection != null) {
//                connection.connect();
//                // 之后可进行url的其他操作
//                // 得到服务器返回过来的流对象
//
//                InputStream inputStream =   urlConnection.getInputStream();
//                ImageSource imageSource = ImageSource.create(inputStream,   new ImageSource.SourceOptions());
//                ImageSource.DecodingOptions   decodingOptions = new ImageSource.DecodingOptions();
//                decodingOptions.desiredPixelFormat   = PixelFormat.ARGB_8888;
//
//                // 普通解码叠加旋转、缩放、裁剪
//                PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
//                // 普通解码
//
//                context.getUITaskDispatcher().syncDispatch(()   -> {
//
//                    image.setPixelMap(pixelMap);
//                    pixelMap.release();
////                    Image image = new   Image(HttpImageSlice.this);
////                    DirectionalLayout.LayoutConfig   config = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT,   DirectionalLayout.LayoutConfig.MATCH_CONTENT);
////                    config.setMargins(10, 10,   10, 10);
////                    image.setLayoutConfig(config);
////                    image.setPixelMap(pixelMap);
////                    myLayout.addComponent(image);
////                    pixelMap.release();
//                });
//
//            }
//        }   catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}
