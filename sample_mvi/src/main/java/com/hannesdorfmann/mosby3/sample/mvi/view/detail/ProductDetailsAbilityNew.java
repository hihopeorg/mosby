/*
 * Copyright 2016 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.detail;

import com.hannesdorfmann.mosby3.sample.mvi.businesslogic.model.Product;
import com.hannesdorfmann.mosby3.sample.mvi.slice.ProductDetailsAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

/**
 * @author Hannes Dorfmann
 */
public class ProductDetailsAbilityNew extends Ability{

    public static final String KEY_PRODUCT_ID = "productId";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ProductDetailsAbilitySlice.class.getName());
    }

    public static void start(Ability activity, Product product) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.hannesdorfmann.mosby3.sample.mvi")
                .withAbilityName(ProductDetailsAbilityNew.class.getName()/*"com.hannesdorfmann.mosby3.sample.mvi.view.detail.ProductDetailsAbility"*/)
                .build();
        intent.setOperation(operation);
        intent.setParam(KEY_PRODUCT_ID, product.getId());
        activity.startAbility(intent);
    }

}
