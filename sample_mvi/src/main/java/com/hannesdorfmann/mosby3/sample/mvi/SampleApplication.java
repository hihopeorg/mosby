/*
 * Copyright 2016 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi;

import com.hannesdorfmann.mosby3.sample.mvi.dependencyinjection.DependencyInjection;
import ohos.aafwk.ability.AbilityPackage;
import ohos.app.Context;

/**
 * @author Hannes Dorfmann
 */

public class SampleApplication extends AbilityPackage {
    private static volatile SampleApplication instance;
    protected DependencyInjection dependencyInjection = new DependencyInjection();

    public static DependencyInjection getDependencyInjection(Context context) {
        return instance.dependencyInjection;
    }


    @Override
    public void onInitialize() {
        super.onInitialize();
        instance = getInstance();
    }

    public static SampleApplication getInstance(){
        if(SampleApplication.instance == null){
            SampleApplication.instance = new SampleApplication();
        }
        return  SampleApplication.instance;
    }

}
