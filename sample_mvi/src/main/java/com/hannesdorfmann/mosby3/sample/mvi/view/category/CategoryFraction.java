/*
 * Copyright 2016 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.sample.mvi.view.category;


import com.hannesdorfmann.mosby3.mvi.MviFraction;
import com.hannesdorfmann.mosby3.sample.mvi.SampleApplication;
import com.hannesdorfmann.mosby3.sample.mvi.ResourceTable;
import com.hannesdorfmann.mosby3.sample.mvi.businesslogic.model.Product;
import com.hannesdorfmann.mosby3.sample.mvi.view.detail.ProductDetailsAbility;
import com.hannesdorfmann.mosby3.sample.mvi.view.ui.viewholder.ProductViewHolder;
import com.hannesdorfmann.mosby3.sample.mvi.view.widge.CirleProgressImg;
import io.reactivex.Observable;

import java.util.List;

import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;

/**
 * Displays all products of a certain category on the screen
 *
 * @author Hannes Dorfmann
 */
public class CategoryFraction extends MviFraction<CategoryView, CategoryPresenter>
        implements CategoryView, ProductViewHolder.ProductClickedListener {

    private final static String CATEGORY_NAME = "categoryName";
    private String categoryName = "";

    ListContainer listContainer;
    CirleProgressImg loadingView;
    Text errorView;
    int spanCount;
    Context context;

    private CategoryAdapter adapter;

    @Override
    public void onProductClicked(Product product) {
        ProductDetailsAbility.start(getFractionAbility(), product);
    }

    public CategoryFraction() {

    }

    public CategoryFraction(String categoryName, Context context) {
        this.categoryName = categoryName;
        this.context = context;
    }


    public static CategoryFraction newInstance(String categoryName, Context context) {
        if (categoryName == null) {
            throw new NullPointerException("category name == null");
        }
        CategoryFraction fragment = new CategoryFraction(categoryName, context);
        return fragment;
    }

    @Override
    protected int getUIContent() {
        return ResourceTable.Layout_stacklayout_category;
    }

    @Override
    protected void initCompoment() {
        listContainer = (ListContainer) mComponent.findComponentById(ResourceTable.Id_listContainer);
        loadingView = (CirleProgressImg) mComponent.findComponentById(ResourceTable.Id_loadingView);
        errorView = (Text) mComponent.findComponentById(ResourceTable.Id_errorView);

        if(context != null){
            adapter = new CategoryAdapter(LayoutScatter.getInstance(context), this);
            listContainer.setItemProvider(adapter);
        }

    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    public CategoryPresenter createPresenter() {//todo ssssss
        if(context != null){
            return SampleApplication.getDependencyInjection(context).newCategoryPresenter();
        }else{
            return null;
        }

    }

    @Override
    public Observable<String> loadIntents() {
        return Observable.just(categoryName);
    }

    @Override
    public void render(CategoryViewState state) {
        if (state instanceof CategoryViewState.LoadingState) {
            renderLoading();
        } else if (state instanceof CategoryViewState.DataState) {
            renderData(((CategoryViewState.DataState) state).getProducts());
        }
        if (state instanceof CategoryViewState.ErrorState) {
            renderError();
        }
    }

    private void renderError() {
        loadingView.setVisibility(Component.HIDE);
        errorView.setVisibility(Component.VISIBLE);
        listContainer.setVisibility(Component.HIDE);
    }

    private void renderLoading() {
        loadingView.setVisibility(Component.VISIBLE);
        errorView.setVisibility(Component.HIDE);
        listContainer.setVisibility(Component.HIDE);
    }

    private void renderData(List<Product> products) {
        adapter.setProducts(products);
        adapter.notifyDataChanged();
        loadingView.setVisibility(Component.HIDE);
        errorView.setVisibility(Component.HIDE);
        listContainer.setVisibility(Component.VISIBLE);
    }
}
