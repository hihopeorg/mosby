/*
 * Copyright 2016 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hannesdorfmann.mosby3.mvi.layout;


import com.hannesdorfmann.mosby3.ComponentContainerMviDelegate;
import com.hannesdorfmann.mosby3.ComponentContainerMviDelegateCallback;
import com.hannesdorfmann.mosby3.ComponentContainerMviDelegateImpl;
import com.hannesdorfmann.mosby3.mvi.MviPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.app.Context;

/**
 * A RelativeLayout that can be used as view with an presenter.
 *
 * @author Hannes Dorfmann
 */
public abstract class MviDependentLayout<V extends MvpView, P extends MviPresenter<V, ?>>
    extends DependentLayout implements MvpView, ComponentContainerMviDelegateCallback<V, P> {

  private boolean isRestoringViewState = false;
  protected ComponentContainerMviDelegate<V, P> mvpDelegate;
  private boolean retainInstance = false;

  public MviDependentLayout(Context context) {
    super(context);
    initBindStateChangedListener();
  }

  public MviDependentLayout(Context context, AttrSet attrs) {
    super(context, attrs);
    initBindStateChangedListener();
  }

  public MviDependentLayout(Context context, AttrSet attrs, String styleName) {
    super(context, attrs, styleName);
    initBindStateChangedListener();
  }


  /**
   * Get the mvp delegate. This is internally used for creating presenter, attaching and detaching
   * view from presenter etc.
   *
   * <p><b>Please note that only one instance of mvp delegate should be used per ohos.agp.components.Component
   * instance</b>.
   * </p>
   *
   * <p>
   * Only override this method if you really know what you are doing.
   * </p>
   *
   * @return {@link ComponentContainerMviDelegate}
   */
  protected ComponentContainerMviDelegate<V, P> getMviDelegate() {
    if (mvpDelegate == null) {
      mvpDelegate = new ComponentContainerMviDelegateImpl<>(this, this, true);
    }

    return mvpDelegate;
  }

  private void initBindStateChangedListener(){
    setBindStateChangedListener(new BindStateChangedListener() {
      Ability ability;
      AbilitySlice slice;
      @Override
      public void onComponentBoundToWindow(Component component) {
        getMviDelegate().onAttachedToWindow();
        onAttachedToWindowDo(component);
      }

      @Override
      public void onComponentUnboundFromWindow(Component component) {
        getMviDelegate().onDetachedFromWindow();
        onDetachedFromWindowDo(component);
      }
    });
  }

  protected abstract void onAttachedToWindowDo(Component component);
  protected abstract void onDetachedFromWindowDo(Component component);


  /**
   * Instantiate a presenter instance
   *
   * @return The {@link MvpPresenter} for this view
   */
  public abstract P createPresenter();


  @Override public V getMvpView() {
    return (V) this;
  }

}
