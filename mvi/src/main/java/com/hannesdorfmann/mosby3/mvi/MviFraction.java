/*
 * Copyright 2016 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.hannesdorfmann.mosby3.mvi;

import com.hannesdorfmann.mosby3.FractionMviDelegate;
import com.hannesdorfmann.mosby3.FractionMviDelegateImpl;
import com.hannesdorfmann.mosby3.MviDelegateCallback;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * <p>
 * This abstract class can be used to extend from to implement an Model-View-Intent pattern with
 * this Fraction as View and a {@link MviPresenter} to coordinate the View and the underlying
 * model (business logic)
 * </p>
 *
 * <p>
 * Per default {@link FractionMviDelegateImpl} is used with the following lifecycle:
 * The View is attached to the Presenter in {@link Fraction#onViewCreated(View, Bundle)}.
 * So you better instantiate all your UI widgets before that lifecycle callback (typically in
 * {@link
 * Fraction#onCreateView(LayoutInflater, ViewGroup, Bundle)}. The View is detached from Presenter in
 * {@link Fraction#onDestroyView()}
 * </p>
 *
 * @author Hannes Dorfmann
 */
public abstract class MviFraction<V extends MvpView, P extends MviPresenter<V, ?>> extends Fraction
    implements MvpView, MviDelegateCallback<V, P> {

  private boolean isRestoringViewState = false;
  protected FractionMviDelegate<V, P> mvpDelegate;
  protected Component mComponent;

  @Override
  public Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
    super.onComponentAttached(scatter, container, intent);
    getMvpDelegate().onComponentAttached(scatter, container, intent);

    mComponent = scatter.parse(getUIContent(), container, false);
    initCompoment();
    return mComponent;
  }

  protected abstract int getUIContent();

  protected abstract void initCompoment();

  protected void onStart(Intent intent) {
    super.onStart(intent);
    getMvpDelegate().onStart(intent);
  }

  @Override
  protected void onActive() {
    super.onActive();
    getMvpDelegate().onActive();
  }

  @Override
  protected void onInactive() {
    super.onInactive();
    getMvpDelegate().onInactive();
  }

  @Override
  protected void onForeground(Intent intent) {
    super.onForeground(intent);
    getMvpDelegate().onForeground(intent);
  }

  @Override
  protected void onBackground() {
    super.onBackground();
    getMvpDelegate().onBackground();
  }

  @Override
  protected void onStop() {
    super.onStop();
    getMvpDelegate().onStop();
  }

  @Override
  protected void onComponentDetach(){
    super.onComponentDetach();
    getMvpDelegate().onComponentDetach();
  }

  /**
   * Instantiate a presenter instance
   *
   * @return The {@link MvpPresenter} for this viewState
   */
  public abstract P createPresenter();

  /**
   * Get the mvp delegate. This is internally used for creating presenter, attaching and detaching
   * viewState from presenter.
   *
   * <p><b>Please note that only one instance of mvp delegate should be used per Fraction
   * instance</b>.
   * </p>
   *
   * <p>
   * Only override this method if you really know what you are doing.
   * </p>
   *
   * @return {@link FractionMviDelegate}
   */
  public FractionMviDelegate<V, P> getMvpDelegate() {
    if (mvpDelegate == null) {
      mvpDelegate = new FractionMviDelegateImpl<V, P>(this, this);
    }

    return mvpDelegate;
  }

  @Override
  public V getMvpView() {
    try {
      return (V) this;
    } catch (ClassCastException e) {
      String msg =
          "Couldn't cast the View to the corresponding View interface. Most likely you forgot to add \"Fraction implements YourMvpViewInterface\".\"";
      System.err.println(msg);
      throw new RuntimeException(msg, e);
    }
  }

  @Override
  public void setRestoringViewState(boolean restoringViewState) {
    this.isRestoringViewState = restoringViewState;
  }

  protected boolean isRestoringViewState() {
    return isRestoringViewState;
  }

}
